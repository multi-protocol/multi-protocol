# multi-protocol

Плагин мультиверсий для spigot, аналог ViaVersion.

Цель: сделать плагин, который будет меньше грузить процессор и не будет беспощадно потреблять оперативку, как это делает ViaVersion. 
Этот плагин будет нацелен на поддержку большого онлайна на одном bukkit-инстансе.

Так же он будет выступать в качестве альтернативы ProtocolLib, например:

```java
MultiProtocolApi.getHandlerManager().registerHandler(SetSlotPacket.class, (packet, connection) -> {
    Slot item = packet.getItem();
    String materialName = item.getItemType();
    if (materialName.equals("stone")) {
        item.setItemType("glass");
    }
});

MultiProtocolApi.getHandlerManager().registerHandler(EntityMetadataPacket.class, (packet, connection) -> {
    for (Metadata metadata : packet.getMetadata().getMetadata()) {
        if (metadata instanceof EntityCustomName) {
            EntityCustomName customName = (EntityCustomName) metadata;
            customName.setName("My Custom Name");
            break;
        }
    }
});
```

Выглядит лучше, чем через рефлекцию получать поля, не так ли?  
Более того, ProtocolLib нещадно генерирует RuntimeException, делает синхронизации в netty потоках и грузит во время компиляции структур.  

Если вы замечали на сервере переодические зависания на 0,5-2 секунды, и при этом TPS и оперативка в норме, значит скорее всего дело в ViaVersion и ProtocolLib.   
ViaVersion иногда вообще составляет около 70% от всей нагрузки сервера, пруф - https://imgur.com/ZLAmnuS

-----

Захотел сделать вклад? Запроси доступ. Инструкция по настройке локального сервера - [TestServer/как-сделать-локальный-сервер.md](TestServer/как-сделать-локальный-сервер.md)