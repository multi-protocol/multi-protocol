package ua.lokha.multiprotocol;

import io.netty.channel.Channel;
import io.netty.util.Attribute;
import io.netty.util.AttributeKey;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.bukkit.entity.Player;
import ua.lokha.multiprotocol.type.EntityType;

/**
 * Сюда вынесена вся информация о соединении
 */
@Data
@EqualsAndHashCode(of = "channel")
@ToString(of = {"name", "version"})
public class Connection {
    @Getter
    private static final AttributeKey<Connection> attributeKey = AttributeKey.newInstance("multi-protocol-user");

    private final Channel channel;
    private Player player;
    private String name;
    private Version version;
    private Protocol protocol;
    private Int2ObjectMap<EntityType> viewEntities = new Int2ObjectOpenHashMap<>();
    private Int2ObjectMap<IntList> entityPassengers = new Int2ObjectOpenHashMap<>();

    /**
     * Тип мира, в котором находится игрок
     */
    private int dimension = -1;

    public Connection(Channel channel) {
        this.channel = channel;
    }

    /**
     * Получить объект соединения по каналу
     */
    public static Connection get(Channel channel) {
        Attribute<Connection> attr = channel.attr(attributeKey);
        Connection connection = attr.get();
        if (connection == null) {
            throw new IllegalStateException("connection object not created for this channel " + channel);
        }
        return connection;
    }

    /**
     * Отправить пакет клиенту
     */
    public void sendPacket(Packet packet) {
        channel.eventLoop().execute(() -> channel.write(packet, channel.voidPromise()));
    }

    /**
     * Вызвать обработчики чтения пакета, сымитировать входящий пакета от клиента
     */
    public void receivePacket(Packet packet) {
        channel.eventLoop().execute(() -> channel.pipeline().fireChannelRead(packet));
    }

    public IntList getEntityPassengers(int entityId) {
        IntList entityIds = entityPassengers.get(entityId);

        if (entityIds == null) {
            entityPassengers.put(entityId, (entityIds = new IntArrayList(2)));
        }

        return entityIds;
    }

    public void addEntityPassenger(int entityId, int passengerId) {
        getEntityPassengers(entityId).add(passengerId);
    }

    public void clearEntityPassengers(int entityId) {
        entityPassengers.remove(entityId);
    }
}
