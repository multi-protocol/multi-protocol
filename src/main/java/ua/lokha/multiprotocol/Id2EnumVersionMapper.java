package ua.lokha.multiprotocol;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.java.Log;
import lombok.var;
import ua.lokha.multiprotocol.util.ResourceUtils;

import java.util.*;
import java.util.stream.Stream;

/**
 * Карта, позволяющая связывать enum с id в зависимости от версии.
 *
 *  <pre>
 *         Id2EnumVersionMapper<TestEnum> mapper = new Id2EnumVersionMapper<>(TestEnum.class);
 *
 *         mapper.register(TestEnum.VALUE1, // значение enum
 *                 map(Version.MINECRAFT_1_9, 20), // связывает значение enum с id 20 начиная с версии 1.9
 *                 map(Version.MINECRAFT_1_13, 30), // связывает значение enum с id 30 начиная с версии 1.13
 *                 map(Version.MINECRAFT_1_14, -1) // начиная с версии 1.14 значение enum не будет иметь id
 *                 );
 *
 *         mapper.getId(TestEnum.VALUE1, Version.MINECRAFT_1_8); // вернет -1
 *         mapper.getId(TestEnum.VALUE1, Version.MINECRAFT_1_9); // вернет 20
 *         mapper.getId(TestEnum.VALUE1, Version.MINECRAFT_1_10); // вернет 20
 *         mapper.getId(TestEnum.VALUE1, Version.MINECRAFT_1_12); // вернет 20
 *         mapper.getId(TestEnum.VALUE1, Version.MINECRAFT_1_13); // вернет 30
 *         mapper.getId(TestEnum.VALUE1, Version.MINECRAFT_1_13_1); // вернет 20
 *         mapper.getId(TestEnum.VALUE1, Version.MINECRAFT_1_14); // вернет -1
 *
 *         mapper.getEnum(20, Version.MINECRAFT_1_11); // вернет значение enum, соотвествующий id 20 в версии 1.11 (TestEnum.VALUE1)
 *  </pre>
 */
@Log
public class Id2EnumVersionMapper<T extends Enum<T>> {

    @Getter
    private Map<Version, VersionMapper<T>> versionMapper = new EnumMap<>(Version.class);

    public Id2EnumVersionMapper(Class<T> enumClass) {
        for (Version version : Version.getVersions()) {
            versionMapper.put(version, new VersionMapper<>(version, enumClass));
        }
    }

    /**
     * Получить id по указанному значению enum в указанной версии
     * @return -1, если в указанной версии нет зарегистрированного значения enum
     */
    public int getId(T enumValue, Version version) {
        return versionMapper.get(version).getId(enumValue);
    }

    /**
     * Получить значения enum по указанному id в указанной версии
     * @return null, если в указанной версии нет зарегистрированного значения enum с указанным id
     */
    public T getEnum(int id, Version version) {
        return versionMapper.get(version).getEnum(id);
    }

    /**
     * Зарегистрировать значение enum.
     * @param idMappers список id связанных с версиями (см. javadoc этого класса для примера)
     * @return this, для цепочки вызовов.
     */
    public Id2EnumVersionMapper<T> register(T enumValue, IdMapper... idMappers) {
        if (idMappers.length == 0) {
            log.warning("idMappers пустой при регистрации " + enumValue);
            return this;
        }

        // регистрирует с конца списка версий таким образом, чтобы в промежуточных версиях
        // тоже был зарегистрировано значение enum
        Iterator<IdMapper> iterator = Stream.of(idMappers)
                .sorted(Comparator.comparingInt(id -> -id.getVersion().ordinal()))
                .iterator();

        List<Version> versions = Version.getVersions();
        IdMapper mapper = iterator.next();
        for (int i = versions.size() - 1; i >= 0; i--) {
            Version version = versions.get(i);
            if (mapper.getId() != -1) {
                versionMapper.get(version).register(enumValue, mapper.getId());
            }
            if (mapper.getVersion().equals(version)) {
                if (iterator.hasNext()) {
                    IdMapper next = iterator.next();
                    if (mapper.getVersion().equals(next.getVersion())) {
                        log.warning("При регистрации значения enum " + enumValue + " были указаны одинаковые версии " + next.getVersion().name());
                    }
                    if (mapper.getId() == next.getId()) {
                        log.warning("При регистрации значения enum " + enumValue + " были указаны одинаковые подряд id " + next.getId());
                    }
                    mapper = next;
                } else {
                    break;
                }
            }
        }
        return this;
    }

    @Data
    @ToString(of = "version")
    public static class VersionMapper<T extends Enum<T>> {
        private Int2ObjectMap<T> enumById = new Int2ObjectOpenHashMap<>();
        private EnumMap<T, Integer> idByEnum;
        private Version version;

        public VersionMapper(Version version, Class<T> enumClass) {
            this.version = version;
            idByEnum = new EnumMap<>(enumClass);
        }

        private static Integer DEFAULT_VALUE = -1;

        public int getId(T enumValue)  {
            return idByEnum.getOrDefault(enumValue, DEFAULT_VALUE);
        }

        public T getEnum(int id)  {
            return enumById.get(id);
        }

        public void register(T enumValue, int id) {
            if (enumById.containsKey(id) || idByEnum.containsKey(enumValue)) {
                log.warning("enum " + id + " " + enumValue + " already registered by version " + version +
                        ": " + idByEnum.get(enumValue) + " " + enumById.get(id));
            }
            enumById.put(id, enumValue);
            idByEnum.put(enumValue, id);
        }
    }

    @Data
    public static class IdMapper {
        private final Version version;
        private final int id;
    }

    /**
     * Связать id с версией в виде пары {@link IdMapper}
     */
    public static IdMapper map(Version version, int id) {
        return new IdMapper(version, id);
    }

    /**
     * Загрузить Id2EnumVersionMapper из файла ресурсов json.
     * <p>Пример файла:
     *     <pre>
     *  [
     *   {
     *     "enum": "STRUCTURE_BLOCK",
     *     "map": [
     *       {
     *         "id": "4080",
     *         "version": "MINECRAFT_1_8"
     *       },
     *       {
     *         "id": "8578",
     *         "version": "MINECRAFT_1_13"
     *       }
     *     ]
     *   }
     * ]
     *     </pre>
     * </p>
     * @param resourceName имя файла ресурсов
     * @param enumClass enum класс, который будет выступать в качестве ключа
     */
    public static <T extends Enum<T>> Id2EnumVersionMapper<T> loadFromResource(String resourceName, Class<T> enumClass) {
        Id2EnumVersionMapper<T> mapper = new Id2EnumVersionMapper<>(enumClass);

        JsonArray blocks = ResourceUtils.GSON.fromJson(Objects.requireNonNull(ResourceUtils.getAsBufferedReader(resourceName)), JsonArray.class);
        for (JsonElement element : blocks) {
            try {
                JsonObject block = element.getAsJsonObject();
                T enumValue = Enum.valueOf(enumClass, block.get("enum").getAsString());

                JsonArray mapList = block.get("map").getAsJsonArray();
                var idMappers = new Id2EnumVersionMapper.IdMapper[mapList.size()];
                for (int i = 0; i < mapList.size(); i++) {
                    JsonObject mapItem = mapList.get(i).getAsJsonObject();
                    Version version = Version.valueOf(mapItem.get("version").getAsString());
                    int id = mapItem.get("id").getAsInt();
                    idMappers[i] = map(version, id);
                }

                mapper.register(enumValue, idMappers);
            } catch (Exception e) {
                log.severe("Ошибка загрузки " + resourceName + ": " + element);
                e.printStackTrace();
            }
        }
        return mapper;
    }
}
