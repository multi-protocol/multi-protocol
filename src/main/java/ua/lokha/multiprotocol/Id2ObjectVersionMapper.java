package ua.lokha.multiprotocol;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.java.Log;
import lombok.var;
import ua.lokha.multiprotocol.util.ResourceUtils;

import java.util.*;
import java.util.stream.Stream;


/**
 * Карта, позволяющая связывать объекты с id в зависимости от версии.
 *
 *  <pre>
 *         Id2ObjectVersionMapper<String> mapper = new Id2ObjectVersionMapper<>();
 *
 *         mapper.register("stone", // объект типа строки
 *                 map(Version.MINECRAFT_1_9, 20), // связывает строку с id 20 начиная с версии 1.9
 *                 map(Version.MINECRAFT_1_13, 30), // связывает строку с id 30 начиная с версии 1.13
 *                 map(Version.MINECRAFT_1_14, -1) // начиная с версии 1.14 строка "stone" не будет иметь id
 *         );
 *
 *         mapper.getId("stone", Version.MINECRAFT_1_8); // вернет -1
 *         mapper.getId("stone", Version.MINECRAFT_1_9); // вернет 20
 *         mapper.getId("stone", Version.MINECRAFT_1_10); // вернет 20
 *         mapper.getId("stone", Version.MINECRAFT_1_12); // вернет 20
 *         mapper.getId("stone", Version.MINECRAFT_1_13); // вернет 30
 *         mapper.getId("stone", Version.MINECRAFT_1_13_1); // вернет 20
 *         mapper.getId("stone", Version.MINECRAFT_1_14); // вернет -1
 *
 *         mapper.getValue(20, Version.MINECRAFT_1_11); // вернет объект, соотвествующий id 20 в версии 1.11 ("stone")
 *  </pre>
 */
@Log
public class Id2ObjectVersionMapper<T> {

    @Getter
    private Map<Version, VersionMapper<T>> versionMapper = new EnumMap<>(Version.class);

    public Id2ObjectVersionMapper() {
        for (Version version : Version.getVersions()) {
            versionMapper.put(version, new VersionMapper<>(version));
        }
    }

    /**
     * Получить id по указанному значению в указанной версии
     *
     * @return -1, если в указанной версии нет зарегистрированного значения
     */
    public int getId(T value, Version version) {
        return versionMapper.get(version).getId(value);
    }

    /**
     * Получить значения по указанному id в указанной версии
     *
     * @return null, если в указанной версии нет зарегистрированного значения с указанным id
     */
    public T getValue(int id, Version version) {
        return versionMapper.get(version).getValue(id);
    }

    /**
     * Зарегистрировать значение.
     *
     * @param idMappers список id связанных с версиями (см. javadoc этого класса для примера)
     * @return this, для цепочки вызовов.
     */
    public Id2ObjectVersionMapper<T> register(T value, IdMapper... idMappers) {
        if (idMappers.length == 0) {
            log.warning("idMappers пустой при регистрации " + value);
            return this;
        }

        // регистрирует с конца списка версий таким образом, чтобы в промежуточных версиях
        // тоже был зарегистрировано значение
        Iterator<IdMapper> iterator = Stream.of(idMappers)
                .sorted(Comparator.comparingInt(id -> -id.getVersion().ordinal()))
                .iterator();

        List<Version> versions = Version.getVersions();
        IdMapper mapper = iterator.next();
        for (int i = versions.size() - 1; i >= 0; i--) {
            Version version = versions.get(i);
            if (mapper.getId() != -1) {
                versionMapper.get(version).register(value, mapper.getId());
            }
            if (mapper.getVersion().equals(version)) {
                if (iterator.hasNext()) {
                    IdMapper next = iterator.next();
                    if (mapper.getVersion().equals(next.getVersion())) {
                        log.warning("При регистрации значения " + value + " были указаны одинаковые версии " + next.getVersion().name());
                    }
                    if (mapper.getId() == next.getId()) {
                        log.warning("При регистрации значения " + value + " были указаны одинаковые подряд id " + next.getId());
                    }
                    mapper = next;
                } else {
                    break;
                }
            }
        }
        return this;
    }

    @Data
    @ToString(of = "version")
    public static class VersionMapper<T> {
        private Int2ObjectMap<T> valueById = new Int2ObjectOpenHashMap<>();
        private Object2IntMap<T> idByValue = new Object2IntOpenHashMap<>();
        private Version version;

        public VersionMapper(Version version) {
            this.version = version;
            idByValue.defaultReturnValue(-1);
        }

        public int getId(T value) {
            return idByValue.getInt(value);
        }

        public T getValue(int id) {
            return valueById.get(id);
        }

        public void register(T value, int id) {
            if (valueById.containsKey(id) || idByValue.containsKey(value)) {
                log.warning("value " + id + " " + value + " already registered by version " + version +
                        ": " + idByValue.getInt(value) + " " + valueById.get(id));
            }
            valueById.put(id, value);
            idByValue.put(value, id);
        }
    }

    @Data
    public static class IdMapper {
        private final Version version;
        private final int id;
    }

    /**
     * Связать id с версией в виде пары {@link IdMapper}
     */
    public static IdMapper map(Version version, int id) {
        return new IdMapper(version, id);
    }

    /**
     * Загрузить Id2ObjectVersionMapper из файла ресурсов json.
     * <p>Пример файла:
     *     <pre>
     * [
     *   {
     *     "value": "diorite",
     *     "map": [
     *       {
     *         "id": "19",
     *         "version": "MINECRAFT_1_8"
     *       },
     *       {
     *         "id": "4",
     *         "version": "MINECRAFT_1_13"
     *       }
     *     ]
     *   }
     * ]
     *     </pre>
     * </p>
     * @param resourceName имя файла ресурсов
     */
    public static Id2ObjectVersionMapper<String> loadFromResourceStringValue(String resourceName) {
        Id2ObjectVersionMapper<String> mapper = new Id2ObjectVersionMapper<>();

        JsonArray blocks = ResourceUtils.GSON.fromJson(Objects.requireNonNull(ResourceUtils.getAsBufferedReader(resourceName)), JsonArray.class);
        for (JsonElement element : blocks) {
            try {
                JsonObject block = element.getAsJsonObject();
                String value = block.get("value").getAsString();

                JsonArray mapList = block.get("map").getAsJsonArray();
                var idMappers = new Id2ObjectVersionMapper.IdMapper[mapList.size()];
                for (int i = 0; i < mapList.size(); i++) {
                    JsonObject mapItem = mapList.get(i).getAsJsonObject();
                    Version version = Version.valueOf(mapItem.get("version").getAsString());
                    int id = mapItem.get("id").getAsInt();
                    idMappers[i] = map(version, id);
                }

                mapper.register(value, idMappers);
            } catch (Exception e) {
                log.severe("Ошибка загрузки " + resourceName + ": " + element);
                e.printStackTrace();
            }
        }
        return mapper;
    }
}
