package ua.lokha.multiprotocol;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import lombok.Data;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.java.Log;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Карта, позволяющая связывать типы данных с id в зависимости от версии.
 *
 *  <pre>
 *         Id2TypeVersionMapper mapper = new Id2TypeVersionMapper();
 *
 *         mapper.register(SetSlotPacket.class, SetSlotPacket::new, // тип данных (класс типа и его creator)
 *                 map(Version.MINECRAFT_1_9, 20), // связывает тип данных с id 20 начиная с версии 1.9
 *                 map(Version.MINECRAFT_1_13, 30), // связывает тип данных с id 30 начиная с версии 1.13
 *                 map(Version.MINECRAFT_1_14, -1) // начиная с версии 1.14 тип данных не будет иметь id
 *                 );
 *
 *         mapper.getId(SetSlotPacket.class, Version.MINECRAFT_1_8); // вернет -1
 *         mapper.getId(SetSlotPacket.class, Version.MINECRAFT_1_9); // вернет 20
 *         mapper.getId(SetSlotPacket.class, Version.MINECRAFT_1_10); // вернет 20
 *         mapper.getId(SetSlotPacket.class, Version.MINECRAFT_1_12); // вернет 20
 *         mapper.getId(SetSlotPacket.class, Version.MINECRAFT_1_13); // вернет 30
 *         mapper.getId(SetSlotPacket.class, Version.MINECRAFT_1_13_1); // вернет 20
 *         mapper.getId(SetSlotPacket.class, Version.MINECRAFT_1_14); // вернет -1
 *
 *         mapper.create(20, Version.MINECRAFT_1_11); // создаст объект, соотвествующий id 20 в версии 1.11 (SetSlotPacket)
 *  </pre>
 */
@Log
public class Id2TypeVersionMapper<T> {

    @Getter
    private Map<Version, VersionMapper<T>> versionMapper = new EnumMap<>(Version.class);

    public Id2TypeVersionMapper() {
        for (Version version : Version.getVersions()) {
            versionMapper.put(version, new VersionMapper<>(version));
        }
    }

    /**
     * Создать типа данных, соответствующий указанному id в указанной версии
     * @return null, если в указанной версии нет зарегистрированного типа данных с таким id
     */
    public T create(int id, Version version) {
        return versionMapper.get(version).create(id);
    }

    /**
     * Получить id по указанному типу данных в указанной версии
     * @return -1, если в указанной версии нет зарегистрированного типа данных с таким классом
     */
    public int getId(Class<? extends T> typeClass, Version version) {
        return versionMapper.get(version).getId(typeClass);
    }

    /**
     * Получить тип данных по указанному id в указанной версии
     * @return null, если в указанной версии нет зарегистрированного типа данных с id
     */
    public Class<? extends T> getType(int id, Version version) {
        return versionMapper.get(version).getType(id);
    }
    
    /**
     * Зарегистрировать тип данных.
     * @param idMappers список id связанных с версиями (см. javadoc этого класса для примера)
     * @return this, для цепочки вызовов.
     */
    public <R extends T> Id2TypeVersionMapper<T> register(Class<R> typeClass, Supplier<R> creator, IdMapper... idMappers) {
        if (idMappers.length == 0) {
            log.warning("idMappers пустой при регистрации " + typeClass);
            return this;
        }

        // регистрирует с конца списка версий таким образом, чтобы в промежуточных версиях
        // тоже был зарегистрирован тип данных
        Iterator<IdMapper> iterator = Stream.of(idMappers)
                .sorted(Comparator.comparingInt(id -> -id.getVersion().ordinal()))
                .iterator();

        List<Version> versions = Version.getVersions();
        IdMapper mapper = iterator.next();
        for (int i = versions.size() - 1; i >= 0; i--) {
            Version version = versions.get(i);
            if (mapper.getId() != -1) {
                versionMapper.get(version).register(typeClass, creator, mapper.getId());
            }
            if (mapper.getVersion().equals(version)) {
                if (iterator.hasNext()) {
                    IdMapper next = iterator.next();
                    if (mapper.getVersion().equals(next.getVersion())) {
                        log.warning("При регистрации типа данных " + typeClass + " были указаны одинаковые версии " + next.getVersion().name());
                    }
                    if (mapper.getId() == next.getId()) {
                        log.warning("При регистрации типа данных " + typeClass + " были указаны одинаковые подряд id " + next.getId());
                    }
                    mapper = next;
                } else {
                    break;
                }
            }
        }
        return this;
    }

    @Data
    @ToString(of = "version")
    public static class VersionMapper<T> {
        private Int2ObjectMap<Info<T>> typeById = new Int2ObjectOpenHashMap<>();
        private Object2IntMap<Class<? extends T>> idByClass = new Object2IntOpenHashMap<>();
        private Version version;

        public VersionMapper(Version version) {
            this.version = version;
            idByClass.defaultReturnValue(-1);
        }

        public int getId(Class<? extends T> typeClass)  {
            return idByClass.getInt(typeClass);
        }

        public Class<? extends T> getType(int id)  {
            Info<T> typeInfo = typeById.get(id);
            if (typeInfo == null) {
                return null;
            }
            return typeInfo.getTypeClass();
        }

        public T create(int id) {
            Info<T> typeInfo = typeById.get(id);
            if (typeInfo == null) {
                return null;
            }
            return typeInfo.getCreator().get();
        }

        public void register(Class<? extends T> typeClass, Supplier<? extends T> creator, int id) {
            Info<T> typeInfo = new Info<>(typeClass, creator);
            if (typeById.containsKey(id) || idByClass.containsKey(typeClass)) {
                log.warning("type " + id + " " + typeClass + " already registered by version " + version +
                        ": " + idByClass.getInt(typeClass) + " " + typeById.get(id));
            }
            typeById.put(id, typeInfo);
            idByClass.put(typeClass, id);
        }
    }

    @Data
    @ToString(of = "typeClass")
    public static class Info<T> {
        private final Class<? extends T> typeClass;
        private final Supplier<? extends T> creator;
    }

    @Data
    public static class IdMapper {
        private final Version version;
        private final int id;
    }

    /**
     * Связать id с версией в виде пары {@link IdMapper}
     */
    public static IdMapper map(Version version, int id) {
        return new IdMapper(version, id);
    }
}
