package ua.lokha.multiprotocol;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import lombok.var;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import ua.lokha.multiprotocol.handler.*;
import ua.lokha.multiprotocol.packet.handshake.HandshakePacket;
import ua.lokha.multiprotocol.packet.login.LoginSuccessPacket;
import ua.lokha.multiprotocol.packet.play.*;
import ua.lokha.multiprotocol.type.BlockProtocol;
import ua.lokha.multiprotocol.type.EntityProtocol;
import ua.lokha.multiprotocol.type.ItemProtocol;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataProtocol;
import ua.lokha.multiprotocol.util.MyObject;

import java.util.List;

/**
 * Главный класс плагина
 */
public class Main extends JavaPlugin {

    private static Main instance;

    public Main() {
        instance = this;
    }

    @Override
    public void onEnable() {
        try {
            this.injectChannelInitializer();

            // init protocols
            Protocol.getProtocols();
            var byMob = MetadataProtocol.BY_ENTITY;
            var blockProtocol = BlockProtocol.BLOCK_PROTOCOL;
            var itemProtocol = ItemProtocol.ITEM_PROTOCOL;
            var mobProtocol = EntityProtocol.MOB_PROTOCOL;
            var objectProtocol = EntityProtocol.OBJECT_PROTOCOL;

            // register packet handlers
            HandlerManager manager = HandlerManager.getInstance();
            manager.registerHandler(HandshakePacket.class, new HandshakePacketHandler());
            manager.registerHandler(LoginSuccessPacket.class, new LoginSuccessPacketHandler());
            manager.registerHandler(LoginPacket.class, new LoginPacketHandler());
            manager.registerHandler(RespawnPacket.class, new RespawnPacketHandler());
            manager.registerHandler(OutPlayerPositionAndLookPacket.class, new TeleportConfirmBefore1_9PacketHandler());
            manager.registerHandler(SetPassengersPacket.class, new SetPassengersPacketHandler());

            manager.registerHandler(SpawnExperienceOrbPacket.class, new SpawnExperienceOrbPacketHandler());
            manager.registerHandler(SpawnMobPacket.class, new SpawnMobPacketHandler());
            manager.registerHandler(SpawnObjectPacket.class, new SpawnObjectPacketHandler());
            manager.registerHandler(SpawnPaintingPacket.class, new SpawnPaintingPacketHandler());
            manager.registerHandler(SpawnPlayerPacket.class, new SpawnPlayerPacketHandler());
            manager.registerHandler(DestroyEntitiesPacket.class, new DestroyEntitiesPacketHandler());

            this.getCommand("testprotocol").setExecutor(new TestProtocolCommand());
        } catch (Exception e) {
            this.getLogger().severe("Ошибка инициализации плагина, выключаем сервер...");
            e.printStackTrace();
            Bukkit.shutdown();
        }
    }

    /**
     * Заменить обработчик входящих соединений, чтобы добавлять свои handler'ы
     */
    private void injectChannelInitializer() {
        List<ChannelFuture> futures = MyObject.wrap(Bukkit.getServer())
                .invokeMethod("getHandle")
                .invokeMethod("getServer")
                .invokeMethod("getServerConnection")
                .getField("g").getObject();

        ChannelHandler channelHandler = futures.get(0).channel().pipeline().get("ServerBootstrap$ServerBootstrapAcceptor#0");

        MyObject wrapChannelHandler = MyObject.wrap(channelHandler);
        ChannelInitializer channelInitializer = wrapChannelHandler.getField("childHandler").getObject();
        if (channelHandler instanceof InjectChannelInitializer) {
            this.getLogger().warning("Обработчик входящих соединений уже был внедрен, не делаем это повторно.");
        } else {
            wrapChannelHandler.setField("childHandler", new InjectChannelInitializer(channelInitializer));
        }
    }

    public static Main getInstance() {
        return instance;
    }
}
