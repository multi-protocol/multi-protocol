package ua.lokha.multiprotocol;

/**
 * Создание пакета
 */
public interface PacketCreator<T extends Packet> {
    T create();
}
