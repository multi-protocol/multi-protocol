package ua.lokha.multiprotocol;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import lombok.extern.java.Log;
import lombok.var;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import ua.lokha.multiprotocol.packet.login.LoginSuccessPacket;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Система, которая отключается пакеты шаг за шагом, чтобы определить проблемные пакеты, что кикают игрока.
 * Используется в команде /testprotocol whokick
 */
@Log
public class TestProtocolWhoKick {

    private PacketHandler<LoginSuccessPacket> packetHandler;

    private List<Class<? extends Packet>> good = new ArrayList<>();
    private List<Class<? extends Packet>> bad = new ArrayList<>();
    private List<Class<? extends Packet>> check;
    private List<List<Class<? extends Packet>>> suspects = new ArrayList<>();

    // протокол со всеми пакетами
    private Id2TypeVersionMapper<Packet> playToClient;
    private Id2TypeVersionMapper<Packet> playToServer;

    public void start() {
        log.info("Запустили систему whokick, теперь нужно заходить с клиента на сервер, пока не будут выявлены все проблемные пакеты.");

        playToClient = Protocol.PLAY.TO_CLIENT;
        playToServer = Protocol.PLAY.TO_SERVER;

        check = collectAllPacketClasses();

        this.updateProtocol();

        packetHandler = (packet, connection) -> {
            log.info("Игрок входит на сервер, ждем кикнет ли его...");

            new BukkitRunnable() {
                private int tick = 0;

                @Override
                public void run() {
                    Player player = Bukkit.getPlayer(connection.getName());
                    if (player != null) {
                        if (tick++ >= 5 * 20) {
                            this.cancel();
                            good();
                            player.kickPlayer("Вас не кикнуло, см. консоль. \nДля следующего шага нужно перезайти.");
                        }
                    } else {
                        this.cancel();
                        fail();
                    }
                }
            }.runTaskTimer(Main.getInstance(), 0, 1);
        };

        MultiProtocolApi.getHandlerManager().registerHandler(LoginSuccessPacket.class, packetHandler);
    }

    public void stop() {
        log.info("Останавливаем систему whokick.");

        Protocol.PLAY.TO_CLIENT = playToClient;
        Protocol.PLAY.TO_SERVER = playToServer;

        MultiProtocolApi.getHandlerManager().unregisterHandler(packetHandler);
    }

    private int step = 1;

    /**
     * Включить только нужные пакеты, которые соотвествуют текущему шагу проверки
     */
    private void updateProtocol() {
        log.info("Шаг " + step++ + ":");
        log.info("  Хороших пакетов " + good.size() +  ": " + good);
        log.info("  Плохих пакетов " + bad.size() +  ": " + bad);
        log.info("  Проверяемых пакетов " + check.size() +  ": " + check);

        List<Class<? extends Packet>> allSuspects = suspects.stream().flatMap(Collection::stream)
                .collect(Collectors.toList());
        log.info("  Ожидают проверки " + allSuspects.size() +  ": " + allSuspects);

        Protocol.PLAY.TO_CLIENT = cloneWithOnlyEnabledPackets(playToClient);
        Protocol.PLAY.TO_SERVER = cloneWithOnlyEnabledPackets(playToServer);
    }

    private void fail() {
        log.info("Игрока кикнуло, анализируем...");

        if (check.size() == 1) {
            Class<? extends Packet> badPacket = check.get(0);
            bad.add(badPacket);
            log.info("Определили плохой пакет " + badPacket);
            
            check.clear();
        }

        if (check.isEmpty()) {
            if (suspects.isEmpty()) {
                log.info("Проверка /testprotocol whokick завершена.");
                log.info("  Проблемные пакеты выявлены: " + bad);
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "testprotocol whokick");
                return;
            }
            
            check = suspects.remove(0);
            log.info("Следующий список на проверку " + check.size() + ": " + check);
        } else {
            int hf = check.size() / 2;
            List<Class<? extends Packet>> addSuspend = new ArrayList<>(hf);
            for (int i = 0; i < hf; i++) {
                addSuspend.add(check.remove(0));
            }
            suspects.add(addSuspend);

            log.info("Оставили половину выборки " + check.size() + ": " + check);
        }

        this.updateProtocol();
    }

    private void good() {
        log.info("Игрока не кикнуло, анализируем...");

        good.addAll(check);

        if (suspects.isEmpty()) {
            log.info("Проверка /testprotocol whokick завершена.");
            log.info("  Проблемные пакеты выявлены: " + bad);
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "testprotocol whokick");
            return;
        }

        check = suspects.remove(0);
        log.info("Следующий список на проверку " + check.size() + ": " + check);

        this.updateProtocol();
    }

    /**
     * Скопировать мапу протокола, при этом оставить в ней только нужные пакеты, которые соотвествуют текущему шагу проверки
     */
    private Id2TypeVersionMapper<Packet> cloneWithOnlyEnabledPackets(Id2TypeVersionMapper<Packet> mapper) {
        Id2TypeVersionMapper<Packet> clone = new Id2TypeVersionMapper<>();
        mapper.getVersionMapper().forEach((version, versionMapper) -> {
            var cloneVersionMapper = new Id2TypeVersionMapper.VersionMapper<Packet>(version);
            cloneVersionMapper.setIdByClass(new Object2IntOpenHashMap<>(versionMapper.getIdByClass()));
            cloneVersionMapper.setTypeById(new Int2ObjectOpenHashMap<>(versionMapper.getTypeById()));

            cloneVersionMapper.getIdByClass().keySet().removeIf(clazz -> !this.isEnablePacket(clazz));
            cloneVersionMapper.getTypeById().values().removeIf(info -> !this.isEnablePacket(info.getTypeClass()));

            clone.getVersionMapper().put(version, cloneVersionMapper);
        });

        return clone;
    }

    public boolean isEnablePacket(Class<? extends Packet> clazz) {
        if (bad.contains(clazz)) {
            return false;
        }

        if (good.contains(clazz)) {
            return true;
        }

        if (check.contains(clazz)) {
            return true;
        }

        return false;
    }

    /**
     * Получить список всех известных классов зареганных пакетов
     */
    private List<Class<? extends Packet>> collectAllPacketClasses() {
        return Stream.concat(
                Protocol.PLAY.TO_CLIENT.getVersionMapper().values().stream()
                        .flatMap(mapper -> mapper.getIdByClass().keySet().stream()),
                Protocol.PLAY.TO_SERVER.getVersionMapper().values().stream()
                        .flatMap(mapper -> mapper.getIdByClass().keySet().stream()))
                .distinct()
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
