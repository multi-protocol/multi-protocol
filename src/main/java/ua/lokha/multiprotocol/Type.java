package ua.lokha.multiprotocol;

import io.netty.buffer.ByteBuf;

/**
 * Означает любой тип данных, который может конвертировать в байты и обратно в зависимости от версии протокола.
 */
public interface Type {
    void read(ByteBuf buf, Version version);

    void write(ByteBuf buf, Version version);
}
