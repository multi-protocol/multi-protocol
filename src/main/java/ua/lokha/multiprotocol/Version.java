package ua.lokha.multiprotocol;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import ua.lokha.multiprotocol.util.MapUtils;

import java.util.Arrays;
import java.util.List;

public enum Version {
    MINECRAFT_1_8(47),
    MINECRAFT_1_9(107),
    MINECRAFT_1_9_1(108),
    MINECRAFT_1_9_2(109),
    MINECRAFT_1_9_4(110),
    MINECRAFT_1_10(210),
    MINECRAFT_1_11(315),
    MINECRAFT_1_11_1(316),
    MINECRAFT_1_12(335),
    MINECRAFT_1_12_1(338),
    MINECRAFT_1_12_2(340),
    MINECRAFT_1_13(393),
    MINECRAFT_1_13_1(401),
    MINECRAFT_1_13_2(404),
    MINECRAFT_1_14(477),
    MINECRAFT_1_14_1(480),
    MINECRAFT_1_14_2(485),
    MINECRAFT_1_14_3(490),
    MINECRAFT_1_14_4(498),
    MINECRAFT_1_15(573),
    MINECRAFT_1_15_1(575),
    MINECRAFT_1_15_2(578);

    private static List<Version> versions = Arrays.asList(values());
    private static Int2ObjectMap<Version> versionByProtocolVersion = new Int2ObjectOpenHashMap<>(MapUtils.calculateExpectedSize(versions.size()));
    private static Version serverVersion = Version.MINECRAFT_1_12_2;

    static {
        for (Version version : versions) {
            versionByProtocolVersion.put(version.getProtocolVersion(), version);
        }
    }

    private final int protocolVersion;

    Version(int protocolVersion) {
        this.protocolVersion = protocolVersion;
    }

    public int getProtocolVersion() {
        return protocolVersion;
    }

    /**
     * Если текущая версия выше переданной.
     * <ul>
     *     <li>{@code Version.MINECRAFT_1_10.isAfter(Version.MINECRAFT_1_9) == true}</li>
     * </ul>
     */
    public boolean isAfter(Version version) {
        return this.protocolVersion > version.getProtocolVersion();
    }

    /**
     * Если текущая версия ниже переданной.
     * <ul>
     *     <li>{@code Version.MINECRAFT_1_10.isBefore(Version.MINECRAFT_1_12) == true}</li>
     * </ul>
     */
    public boolean isBefore(Version version) {
        return this.protocolVersion < version.getProtocolVersion();
    }

    /**
     * Если текущая версия выше или равна переданной.
     * <ul>
     *     <li>{@code Version.MINECRAFT_1_10.isAfterOrEq(Version.MINECRAFT_1_9) == true}</li>
     *     <li>{@code Version.MINECRAFT_1_10.isAfterOrEq(Version.MINECRAFT_1_10) == true}</li>
     * </ul>
     */
    public boolean isAfterOrEq(Version version) {
        return this.protocolVersion >= version.getProtocolVersion();
    }

    /**
     * Если текущая версия ниже или равна переданной.
     * <ul>
     *     <li>{@code Version.MINECRAFT_1_10.isBeforeOrEq(Version.MINECRAFT_1_12) == true}</li>
     *     <li>{@code Version.MINECRAFT_1_10.isBeforeOrEq(Version.MINECRAFT_1_10) == true}</li>
     * </ul>
     */
    public boolean isBeforeOrEq(Version version) {
        return this.protocolVersion <= version.getProtocolVersion();
    }

    public static Version getByProtocolVersion(int protocolVersion) {
        return versionByProtocolVersion.get(protocolVersion);
    }

    /**
     * Получить список всех версий
     */
    public static List<Version> getVersions() {
        return versions;
    }

    /**
     * Версия сервера
     */
    public static Version getServerVersion() {
        return serverVersion;
    }

    @Override
    public String toString() {
        return "Version{" +
                "name=" + this.name() +
                ", protocolVersion=" + protocolVersion +
                '}';
    }
}
