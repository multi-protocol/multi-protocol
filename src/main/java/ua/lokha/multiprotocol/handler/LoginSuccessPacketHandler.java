package ua.lokha.multiprotocol.handler;

import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.PacketHandler;
import ua.lokha.multiprotocol.Protocol;
import ua.lokha.multiprotocol.packet.login.LoginSuccessPacket;

/**
 * Тут мы просто меняем протокол в Connection, если игрок успешно прошел первую стадию логина
 */
public class LoginSuccessPacketHandler implements PacketHandler<LoginSuccessPacket> {
    @Override
    public void handle(LoginSuccessPacket packet, Connection connection) {
        connection.setName(packet.getUsername());
        connection.setProtocol(Protocol.PLAY);
    }
}
