package ua.lokha.multiprotocol.handler;

import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.PacketHandler;
import ua.lokha.multiprotocol.packet.play.RespawnPacket;

/**
 * Получаем информацию о dimension
 */
public class RespawnPacketHandler implements PacketHandler<RespawnPacket> {
    @Override
    public void handle(RespawnPacket packet, Connection connection) {
        connection.setDimension(packet.getDimension());
    }
}
