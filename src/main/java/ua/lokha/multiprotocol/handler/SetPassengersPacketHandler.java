package ua.lokha.multiprotocol.handler;

import it.unimi.dsi.fastutil.ints.IntList;
import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.PacketHandler;
import ua.lokha.multiprotocol.packet.play.SetPassengersPacket;

/**
 * Ловим пакет изменения пассажиров моба и обновляем эти данные в {@link Connection#getEntityPassengers(int)}.
 * Это необходимо, чтобы при отправке пакета в 1.8 иметь данные о текущих пассажирах.
 */
public class SetPassengersPacketHandler implements PacketHandler<SetPassengersPacket> {

    @Override
    public void handle(SetPassengersPacket packet, Connection connection) {
        IntList passengers = connection.getEntityPassengers(packet.getEntityId());

        passengers.clear();

        for (int passenger : packet.getPassengers()) {
            passengers.add(passenger);
        }
    }
}
