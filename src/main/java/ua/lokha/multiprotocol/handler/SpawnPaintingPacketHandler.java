package ua.lokha.multiprotocol.handler;

import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.PacketHandler;
import ua.lokha.multiprotocol.packet.play.SpawnPaintingPacket;
import ua.lokha.multiprotocol.type.EntityType;

public class SpawnPaintingPacketHandler implements PacketHandler<SpawnPaintingPacket> {
    @Override
    public void handle(SpawnPaintingPacket packet, Connection connection) {
        connection.getViewEntities().put(packet.getEntityId(), EntityType.PAINTING);
    }
}
