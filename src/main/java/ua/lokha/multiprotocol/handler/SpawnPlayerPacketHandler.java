package ua.lokha.multiprotocol.handler;

import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.PacketHandler;
import ua.lokha.multiprotocol.packet.play.SpawnPlayerPacket;
import ua.lokha.multiprotocol.type.EntityType;

public class SpawnPlayerPacketHandler implements PacketHandler<SpawnPlayerPacket> {
    @Override
    public void handle(SpawnPlayerPacket packet, Connection connection) {
        connection.getViewEntities().put(packet.getEntityId(), EntityType.PLAYER);
    }
}
