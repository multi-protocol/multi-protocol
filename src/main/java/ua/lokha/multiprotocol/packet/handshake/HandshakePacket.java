package ua.lokha.multiprotocol.packet.handshake;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.*;


@Data
public class HandshakePacket implements Packet {

    private int protocolVersion;
    private String host;
    private int port;
    private int requestedProtocol;

    @Override
    public void read(ByteBuf buf, Version version) {
        this.protocolVersion = readVarInt(buf);
        this.host = readString(buf);
        this.port = buf.readUnsignedShort();
        this.requestedProtocol = readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(Version.getServerVersion().getProtocolVersion(), buf);
        writeString(this.host, buf);
        buf.writeShort(this.port);
        writeVarInt(this.requestedProtocol, buf);
    }
}
