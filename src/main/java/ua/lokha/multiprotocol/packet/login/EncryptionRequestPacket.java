package ua.lokha.multiprotocol.packet.login;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

@Data
public class EncryptionRequestPacket implements Packet {
    private String serverId;
    private byte[] publicKey;
    private byte[] verifyToken;

    @Override
    public void read(ByteBuf buf, Version version) {
        this.serverId = readString(buf);
        this.publicKey = readArray(buf);
        this.verifyToken = readArray(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeString(this.serverId, buf);
        writeArray(this.publicKey, buf);
        writeArray(this.verifyToken, buf);
    }
}
