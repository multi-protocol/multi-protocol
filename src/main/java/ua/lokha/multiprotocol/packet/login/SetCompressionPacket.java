package ua.lokha.multiprotocol.packet.login;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
public class SetCompressionPacket implements Packet {
    private int threshold;

    @Override
    public void read(ByteBuf buf, Version version) {
        this.threshold = readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(this.threshold, buf);
    }
}
