package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AttachEntityPacket implements Packet {

    private int attachedId;
    private int holdingId;

    private boolean leash;

    public AttachEntityPacket(int attachedId, int holdingId) {
        this(attachedId, holdingId, false);
    }

    @Override
    public void read(ByteBuf buf, Version version) {
        attachedId = buf.readInt();
        holdingId = buf.readInt();

        if (version.isBeforeOrEq(Version.MINECRAFT_1_8)) {
            leash = buf.readBoolean();
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeInt(attachedId);
        buf.writeInt(holdingId);

        if (version.isBeforeOrEq(Version.MINECRAFT_1_8)) {
            buf.writeBoolean(leash);
        }
    }
}
