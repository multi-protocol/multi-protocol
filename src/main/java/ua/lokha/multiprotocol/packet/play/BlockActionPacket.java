package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Position;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class BlockActionPacket implements Packet {

    private int x;
    private int y;
    private int z;
    private short actionId;
    private short actionParam;
    private int blockId;

    @Override
    public void read(ByteBuf buf, Version version) {
        long position = buf.readLong();
        x = Position.getX(position, version);
        y = Position.getY(position, version);
        z = Position.getZ(position, version);

        actionId = buf.readUnsignedByte();
        actionParam = buf.readUnsignedByte();
        blockId = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeLong(Position.getPosition(x, y, z, version));
        buf.writeByte(actionId);
        buf.writeByte(actionParam);
        PacketUtils.writeVarInt(blockId, buf);
    }
}
