package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
public class ChangeGameStatePacket implements Packet {
    private int reason;
    private float gameMode;

    @Override
    public void read(ByteBuf buf, Version version) {
        reason = buf.readUnsignedByte();
        gameMode = buf.readFloat();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeByte(reason);
        buf.writeFloat(gameMode);
    }
}
