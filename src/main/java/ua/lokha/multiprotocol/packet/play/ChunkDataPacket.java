package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.Connection;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.type.BlockProtocol;
import ua.lokha.multiprotocol.type.chunk.ChunkData;
import ua.lokha.multiprotocol.type.chunk.ChunkSection;
import ua.lokha.multiprotocol.type.nbt.CompoundTag;
import ua.lokha.multiprotocol.type.nbt.LongArrayTag;
import ua.lokha.multiprotocol.type.nbt.Tag;
import ua.lokha.multiprotocol.util.NbtUtils;
import ua.lokha.multiprotocol.util.PacketUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
@Log
public class ChunkDataPacket implements Packet {

    /**
     * Формат чанков этой имплементации. Временная переменная
     */
    private static Version chunkFormatVersion = Version.MINECRAFT_1_8;
    private static int air = 0;
    private static int void_air = BlockProtocol.BLOCK_PROTOCOL.getId("void_air", chunkFormatVersion);
    private static int cave_air = BlockProtocol.BLOCK_PROTOCOL.getId("cave_air", chunkFormatVersion);

    /**
     * Тип мира, в котором находится игрок
     */
    private int dimension = -1;

    private int chunkX;
    private int chunkZ;
    private boolean groundUpContinuous;
    private int primaryBitMask;
    private Tag heightMaps;
    private ChunkData chunkData;
    private Tag[] tileEntities;

    public ChunkDataPacket() {
    }

    public ChunkDataPacket(int dimension) {
        this.dimension = dimension;
    }

    @Override
    public void read(ByteBuf buf, Version version, Connection connection) {
        this.dimension = connection.getDimension();
        this.read(buf, version);
    }

    @Override
    public void read(ByteBuf buf, Version version) {
        chunkX = buf.readInt();
        chunkZ = buf.readInt();
        groundUpContinuous = buf.readBoolean();
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            primaryBitMask = readVarInt(buf);
        } else {
            primaryBitMask = buf.readUnsignedShort();
        }

        if (version.isBefore(Version.MINECRAFT_1_9) || version.isAfter(Version.MINECRAFT_1_12_2)) {
            throw new UnsupportedVersionException(version); // считывать умеем пока только чанки версии 1.9-1.12.2
        }

        boolean hasSkyLight;
        if (dimension == -1) {
            hasSkyLight = true; // наиболее вероятно
            log.warning("reading a ChunkDataPacket without specifying the dimension of world, " +
                    "please use the constructor ChunkDataPacket(int dimension)");
        } else {
            hasSkyLight = dimension == 0;
        }

        if (version.isAfterOrEq(Version.MINECRAFT_1_14)) {
            heightMaps = Tag.readNamedTag(buf);
        }

        int dataLength = readVarInt(buf);
        chunkData = new ChunkData();
        chunkData.read(buf, version, dataLength, primaryBitMask, hasSkyLight);

        if (version.isAfterOrEq(Version.MINECRAFT_1_9_4)) {
            tileEntities = new Tag[readVarInt(buf)];
            for (int i = 0; i < tileEntities.length; i++) {
                tileEntities[i] = Tag.readNamedTag(buf);
            }
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeInt(chunkX);
        buf.writeInt(chunkZ);
        buf.writeBoolean(groundUpContinuous);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            writeVarInt(primaryBitMask, buf);
        } else {
            buf.writeShort(primaryBitMask);
        }

        if (version.isAfterOrEq(Version.MINECRAFT_1_14)) {
            if (heightMaps != null) {
                Tag.writeNamedTag(heightMaps, buf);
            } else {
                CompoundTag tag = new CompoundTag("");
                int[] motionBlocking = new int[16 * 16];
                int[] worldSurface = new int[16 * 16];

                boolean[] motionBlockingContainsIds = BlockProtocol.MOTION_BLOCKING.get(chunkFormatVersion);

                for (int sectionY = 0; sectionY < 16; sectionY++) {
                    ChunkSection section = chunkData.getSections()[sectionY];
                    if (section == null) {
                        continue;
                    }
                    for (int x = 0; x < 16; x++) {
                        for (int y = 0; y < 16; y++) {
                            for (int z = 0; z < 16; z++) {
                                int id = section.getBlockState(x, y, z);
                                if (id != air && id != void_air && id != cave_air) {
                                    worldSurface[x + z * 16] = y + sectionY * 16 + 1; // +1 (top of the block)
                                }
                                if (id > 0 && id < motionBlockingContainsIds.length && motionBlockingContainsIds[id]) {
                                    motionBlocking[x + z * 16] = y + sectionY * 16 + 1; // +1 (top of the block)
                                }
                            }
                        }
                    }
                }

                tag.put("MOTION_BLOCKING", new LongArrayTag("MOTION_BLOCKING", encodeHeightMap(motionBlocking)));
                tag.put("WORLD_SURFACE", new LongArrayTag("WORLD_SURFACE", encodeHeightMap(worldSurface)));
                Tag.writeNamedTag(tag, buf);
            }
        }

        if (version.isAfterOrEq(Version.MINECRAFT_1_15)) { // remap chunk 1.15
            byte[] biomeData = chunkData.getBiomeData();
            if (biomeData != null) {
                int[] newBiomeData = new int[1024];
                // Now in 4x4x4 areas - take the biome of each "middle"
                for (int i = 0; i < 4; ++i) {
                    for (int j = 0; j < 4; ++j) {
                        int x = (j << 2) + 2;
                        int z = (i << 2) + 2;
                        int oldIndex = (z << 4 | x);
                        newBiomeData[i << 2 | j] = biomeData[oldIndex] & 0xFF;
                    }
                }
                // ... and copy it to the new y layers
                for (int i = 1; i < 64; ++i) {
                    System.arraycopy(newBiomeData, 0, newBiomeData, i * 16, 16);
                }
                for (int data : newBiomeData) {
                    buf.writeInt(data);
                }
            }
        }

        if (chunkData != null) {
            ByteBuf chunkBuf = buf.alloc().buffer();
            chunkData.write(chunkBuf, version);
            writeVarInt(chunkBuf.readableBytes(), buf);
            buf.writeBytes(chunkBuf);
            chunkBuf.release();
        } else {
            PacketUtils.writeVarInt(0, buf);
        }

        if (version.isAfterOrEq(Version.MINECRAFT_1_9_4)) {
            writeVarInt(tileEntities.length, buf);
            for(Tag tileEntity : tileEntities) {
                Tag.writeNamedTag(tileEntity, buf);
            }
        }
    }

    private static long[] encodeHeightMap(int[] heightMap) {
        final int bitsPerBlock = 9;
        long maxEntryValue = (1L << bitsPerBlock) - 1;

        int length = (int) Math.ceil(heightMap.length * bitsPerBlock / 64.0);
        long[] data = new long[length];

        for (int index = 0; index < heightMap.length; index++) {
            int value = heightMap[index];
            int bitIndex = index * 9;
            int startIndex = bitIndex / 64;
            int endIndex = ((index + 1) * bitsPerBlock - 1) / 64;
            int startBitSubIndex = bitIndex % 64;
            data[startIndex] = data[startIndex] & ~(maxEntryValue << startBitSubIndex) | ((long) value & maxEntryValue) << startBitSubIndex;
            if (startIndex != endIndex) {
                int endBitSubIndex = 64 - startBitSubIndex;
                data[endIndex] = data[endIndex] >>> endBitSubIndex << endBitSubIndex | ((long) value & maxEntryValue) >> endBitSubIndex;
            }
        }

        return data;
    }

    @Override
    public List<Packet> map(Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_14)) {
            List<Packet> packets = new ArrayList<>(3);
            packets.add(new UpdateViewPositionPacket(chunkX, chunkZ));
            UpdateLightPacket packet = new UpdateLightPacket();
            packet.setChunkX(chunkX);
            packet.setChunkX(chunkZ);
            // todo сделать remap света секций через пакет UpdateLightPacket
            packets.add(packet);
            packets.add(this);
            return packets;
        }

        if(version.isBefore(Version.MINECRAFT_1_9_4)) {
            ArrayList<Packet> remap = new ArrayList<>(tileEntities.length + 1);
            remap.add(this);

            for(Tag baseTag: tileEntities) {
                CompoundTag entityTag = (CompoundTag) baseTag;
                String nbtId = entityTag.getString("id");
                int tileId = NbtUtils.getTileEntityId(nbtId);
                if(tileId != -1) {
                    if(tileId == 9) {
                        remap.add(new OutUpdateSignPacket(entityTag.getInt("x"), entityTag.getInt("y"), entityTag.getInt("z"), NbtUtils.readSignLines(entityTag)));
                        continue;
                    }

                    remap.add(new UpdateBlockEntityPacket(entityTag.getInt("x"), entityTag.getInt("y"), entityTag.getInt("z"), (short) tileId, baseTag));
            } else
                    log.warning("Не удалось обнаружить ID для TileEntity " + nbtId);
            }

            return remap;
        }
        return Collections.singletonList(this);
    }
}
