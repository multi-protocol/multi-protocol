package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Slot;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class ClickWindowPacket implements Packet {

    private int windowId;
    private short slot;
    private byte button;
    private short action;
    private int mode;
    private Slot clickedItem;

    @Override
    public void read(ByteBuf buf, Version version) {
        windowId = buf.readUnsignedByte();
        slot = buf.readShort();
        button = buf.readByte();
        action = buf.readShort();

        if(version.isBefore(Version.MINECRAFT_1_9))
            mode = buf.readByte();
        else
            mode = PacketUtils.readVarInt(buf);

        clickedItem = new Slot();
        clickedItem.read(buf, version);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeByte(windowId);
        buf.writeShort(slot);
        buf.writeByte(button);
        buf.writeShort(action);

        if(version.isBefore(Version.MINECRAFT_1_9))
            buf.writeByte(mode);
        else
            PacketUtils.writeVarInt(mode, buf);

        clickedItem.write(buf, version);
    }
}
