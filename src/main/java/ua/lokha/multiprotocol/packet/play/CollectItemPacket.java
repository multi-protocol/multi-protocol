package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class CollectItemPacket implements Packet {

    private int collectedItemId;
    private int collectorId;

    private int itemCount;

    @Override
    public void read(ByteBuf buf, Version version) {
        collectedItemId = PacketUtils.readVarInt(buf);
        collectorId = PacketUtils.readVarInt(buf);

        if(version.isAfter(Version.MINECRAFT_1_11))
            itemCount = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(collectedItemId, buf);
        PacketUtils.writeVarInt(collectorId, buf);

        if(version.isAfter(Version.MINECRAFT_1_11))
            PacketUtils.writeVarInt(itemCount, buf);
    }
}
