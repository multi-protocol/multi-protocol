package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class CombatEventPacket implements Packet {
    private int event;

    private int entityId;
    private int duration;
    private int playerId;
    private String messageJson;

    @Override
    public void read(ByteBuf buf, Version version) {
        event = PacketUtils.readVarInt(buf);
        switch(event) {
            case 1: { //ENTER_COMBAT
                duration = PacketUtils.readVarInt(buf);
                entityId = buf.readInt();
                break;
            }
            case 2: { //ENTITY_DEAD
                playerId = PacketUtils.readVarInt(buf);
                entityId = buf.readInt();
                messageJson = PacketUtils.readString(buf);
            }
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(event, buf);
        switch(event) {
            case 1: { //ENTER_COMBAT
                PacketUtils.writeVarInt(duration, buf);
                buf.writeInt(entityId);
                break;
            }
            case 2: { //ENTITY_DEAD
                PacketUtils.writeVarInt(playerId, buf);
                buf.writeInt(entityId);
                PacketUtils.writeString(messageJson, buf);
            }
        }
    }
}
