package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class CraftRecipeResponsePacket implements Packet {

    private byte windowID;
    private int recipeID;

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_12_1))
            throw new UnsupportedVersionException(version);
        windowID = buf.readByte();
        recipeID = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_12_1))
            throw new UnsupportedVersionException(version);
        buf.writeByte(windowID);
        PacketUtils.writeVarInt(recipeID, buf);
    }
}
