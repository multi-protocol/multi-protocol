package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Slot;

@Data
public class CreativeInventoryActionPacket implements Packet {

    private short inventorySlot;
    private Slot clickedItem;

    @Override
    public void read(ByteBuf buf, Version version) {
        inventorySlot = buf.readShort();
        clickedItem = new Slot();
        clickedItem.read(buf, version);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeShort(inventorySlot);
        clickedItem.write(buf, version);
    }
}
