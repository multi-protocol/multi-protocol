package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
public class DestroyEntitiesPacket implements Packet {
    private int[] entityIds;

    @Override
    public void read(ByteBuf buf, Version version) {
        int entityIds$count = 0;
        entityIds$count = readVarInt(buf);
        int max_i_entityIds = entityIds$count;
        entityIds = new int[max_i_entityIds];
        for (int i_entityIds = 0; i_entityIds < max_i_entityIds; i_entityIds++) {
            entityIds[i_entityIds] = readVarInt(buf);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        int entityIds$count = entityIds.length;
        writeVarInt(entityIds$count, buf);
        for (int entityIds : this.entityIds) {
            writeVarInt(entityIds, buf);
        }
    }
}
