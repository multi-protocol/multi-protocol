package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
//В 1.14 ClickWindowButtonPacket, поля те же
public class EnchantItemPacket implements Packet {

    private byte windowId;
    private byte buttonId;

    @Override
    public void read(ByteBuf buf, Version version) {
        windowId = buf.readByte();
        buttonId = buf.readByte();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeByte(windowId);
        buf.writeByte(buttonId);
    }
}
