package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.ActionProtocol;
import ua.lokha.multiprotocol.type.ActionType;

import java.util.Collections;
import java.util.List;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
public class EntityActionPacket implements Packet {

    private int entityId;
    private ActionType actionType;
    private int jumpBoost;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = readVarInt(buf);
        actionType = ActionProtocol.ACTION_PROTOCOL.getEnum(readVarInt(buf), version);
        jumpBoost = readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(entityId, buf);
        writeVarInt(ActionProtocol.ACTION_PROTOCOL.getId(actionType, version), buf);
        writeVarInt(jumpBoost, buf);
    }

    @Override
    public List<Packet> map(Version version) {
        int id = ActionProtocol.ACTION_PROTOCOL.getId(actionType, version);
        if (id == -1) {
            return Collections.emptyList();
        }

        return Collections.singletonList(this);
    }
}
