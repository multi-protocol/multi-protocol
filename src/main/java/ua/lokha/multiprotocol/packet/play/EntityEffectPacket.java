package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.EffectProtocol;
import ua.lokha.multiprotocol.type.EffectType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class EntityEffectPacket implements Packet {

    private int entityId;
    private EffectType effectType;
    private byte amplifier;
    private int duration;
    private byte flags;


    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = PacketUtils.readVarInt(buf);
        int effectId = buf.readByte();
        effectType = EffectProtocol.EFFECT_PROTOCOL.getEnum(effectId, version);
        amplifier = buf.readByte();
        duration = PacketUtils.readVarInt(buf);
        flags = buf.readByte();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(entityId, buf);
        int effectId = EffectProtocol.EFFECT_PROTOCOL.getId(effectType, version);
        buf.writeByte(effectId);
        buf.writeByte(amplifier);
        PacketUtils.writeVarInt(duration, buf);
        buf.writeByte(flags);
    }
}