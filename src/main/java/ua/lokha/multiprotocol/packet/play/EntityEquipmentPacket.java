package ua.lokha.multiprotocol.packet.play;


import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.type.Slot;

import java.util.Collections;
import java.util.List;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
public class EntityEquipmentPacket implements Packet {
    private int entityId;
    private SlotType slot;
    private Slot item;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = readVarInt(buf);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            slot = SlotType.getType(readVarInt(buf), version);
        } else {
            slot = SlotType.getType(buf.readShort(), version);
        }
        item = new Slot();
        item.read(buf, version);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(entityId, buf);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            writeVarInt(SlotType.getId(slot, version), buf);
        } else {
            if (slot.equals(SlotType.OFF_HAND)) {
                throw new UnsupportedVersionException(version, "slot type OFF_HAND on version" + version);
            }
            buf.writeShort(SlotType.getId(slot, version));
        }
        item.write(buf, version);
    }

    @Override
    public List<Packet> map(Version version) {
        if (version.isBefore(Version.MINECRAFT_1_9) && slot.equals(SlotType.OFF_HAND)) {
            return Collections.emptyList();
        }
        return Collections.singletonList(this);
    }

    public enum SlotType {
        MAIN_HAND,
        OFF_HAND,
        BOOTS,
        LEGGINGS,
        CHESTPLATE,
        HELMET,
        ;

        private static SlotType[] byOrdinal = values();

        public static int getId(SlotType type, Version version) {
            if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
                return type.ordinal();
            } else {
                if (type.equals(MAIN_HAND)) {
                    return 0;
                } else if (type.equals(OFF_HAND)) {
                    return -1;
                } else {
                    return type.ordinal() - 1;
                }
            }
        }

        public static SlotType getType(int id, Version version) {
            if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
                return byOrdinal[id];
            } else {
                if (id == 0) {
                    return MAIN_HAND;
                } else if (id < 5) {
                    return byOrdinal[id + 1];
                } else {
                    return null;
                }
            }
        }
    }
}
