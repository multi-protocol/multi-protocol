package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class EntityLookAndRelativeMovePacket implements Packet {
    private int entityId;
    private short dX;
    private short dY;
    private short dZ;
    private byte yaw;
    private byte pitch;
    private boolean onGround;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = PacketUtils.readVarInt(buf);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            dX = buf.readShort();
            dY = buf.readShort();
            dZ = buf.readShort();
        } else {
            dX = (short) (buf.readByte() << 7);
            dY = (short) (buf.readByte() << 7);
            dZ = (short) (buf.readByte() << 7);
        }
        yaw = buf.readByte();
        pitch = buf.readByte();
        onGround = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(entityId, buf);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            buf.writeShort(dX);
            buf.writeShort(dY);
            buf.writeShort(dZ);
        } else {
            buf.writeByte(dX >> 7);
            buf.writeByte(dY >> 7);
            buf.writeByte(dZ >> 7);
        }
        buf.writeByte(yaw);
        buf.writeByte(pitch);
        buf.writeBoolean(onGround);
    }
}
