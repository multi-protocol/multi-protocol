package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import java.util.UUID;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

// todo сделать remap
// в разных версиях разное чисто Properties
@Data
public class EntityPropertiesPacket implements Packet {
    public int entityId;
    public Properties[] properties;

    @Override
    public void read(ByteBuf buf, Version version) {
        int properties$count = 0;
        entityId = readVarInt(buf);
        properties$count = buf.readInt();
        int max_i_properties = properties$count;
        properties = new Properties[max_i_properties];
        for (int i_properties = 0; i_properties < max_i_properties; i_properties++) {
            properties[i_properties] = new Properties();
            properties[i_properties].read(buf);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(entityId, buf);
        if (version.equals(Version.MINECRAFT_1_9_4)) {
            buf.writeInt(0); // todo кикает в 1.9.4, сделать remap
        } else {
            int properties$count = properties.length;
            buf.writeInt(properties$count);
            for (Properties properties : this.properties) {
                properties.write(buf);
            }
        }
    }

    @Data
    public static class Properties {
        public String key;
        public double value;
        public Modifiers[] modifiers;

        public void read(ByteBuf in) {
            int modifiers$count = 0;
            key = readString(in);
            value = in.readDouble();
            modifiers$count = readVarInt(in);
            int max_i_modifiers = modifiers$count;
            modifiers = new Modifiers[max_i_modifiers];
            for (int i_modifiers = 0; i_modifiers < max_i_modifiers; i_modifiers++) {
                modifiers[i_modifiers] = new Modifiers();
                modifiers[i_modifiers].read(in);
            }
        }

        public void write(ByteBuf out) {
            writeString(key, out);
            out.writeDouble(value);
            int modifiers$count = modifiers.length;
            writeVarInt(modifiers$count, out);
            for (Modifiers modifiers : this.modifiers) {
                modifiers.write(out);
            }
        }

        public static class Modifiers {
            public UUID uuid;
            public double amount;
            public byte operation;

            private void read(ByteBuf in) {
                uuid = readUUID(in);
                amount = in.readDouble();
                operation = in.readByte();
            }

            private void write(ByteBuf out) {
                writeUUID(uuid, out);
                out.writeDouble(amount);
                out.writeByte(operation);
            }

        }
    }
}
