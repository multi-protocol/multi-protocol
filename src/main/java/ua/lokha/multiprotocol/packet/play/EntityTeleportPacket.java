package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class EntityTeleportPacket implements Packet {

    private int entityId;
    private double x;
    private double y;
    private double z;
    private byte yaw;
    private byte pitch;
    private boolean onGround;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = PacketUtils.readVarInt(buf);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            x = buf.readDouble();
            y = buf.readDouble();
            z = buf.readDouble();
        } else {
            x = buf.readInt() / 32.0D;
            y = buf.readInt() / 32.0D;
            z = buf.readInt() / 32.0D;
        }
        yaw = buf.readByte();
        pitch = buf.readByte();
        onGround = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(entityId, buf);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            buf.writeDouble(x);
            buf.writeDouble(y);
            buf.writeDouble(z);
        } else {
            buf.writeInt((int) (x * 32.0D));
            buf.writeInt((int) (y * 32.0D));
            buf.writeInt((int) (z * 32.0D));
        }
        buf.writeByte(yaw);
        buf.writeByte(pitch);
        buf.writeBoolean(onGround);
    }
}
