package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

//В документации не изменяется от 1.8 к 1.14, в клиенте тоже
@Data
public class ExplosionPacket implements Packet {

    private float x;
    private float y;
    private float z;
    private float radius;
    private Record[] records;
    private float motionX;
    private float motionY;
    private float motionZ;

    @Override
    public void read(ByteBuf buf, Version version) {
        x = buf.readFloat();
        y = buf.readFloat();
        z = buf.readFloat();
        radius = buf.readFloat();
        int recordCount = buf.readInt();
        records = new Record[recordCount];
        for (int i = 0; i < recordCount; i++) {
            Record record = new Record();
            record.setX(buf.readByte());
            record.setY(buf.readByte());
            record.setZ(buf.readByte());
            records[i] = record;
        }
        motionX = buf.readFloat();
        motionY = buf.readFloat();
        motionZ = buf.readFloat();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeFloat(x);
        buf.writeFloat(y);
        buf.writeFloat(z);
        buf.writeFloat(radius);
        buf.writeInt(records.length);
        for (Record record : records) {
            buf.writeByte(record.getX());
            buf.writeByte(record.getY());
            buf.writeByte(record.getZ());
        }
        buf.writeFloat(motionX);
        buf.writeFloat(motionY);
        buf.writeFloat(motionZ);
    }

    @Data
    public static class Record {
        private byte x;
        private byte y;
        private byte z;
    }
}
