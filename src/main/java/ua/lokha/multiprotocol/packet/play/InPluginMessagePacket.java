package ua.lokha.multiprotocol.packet.play;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import java.util.Locale;
import java.util.function.Function;

import static ua.lokha.multiprotocol.util.PacketUtils.readString;
import static ua.lokha.multiprotocol.util.PacketUtils.writeString;

@Data
public class InPluginMessagePacket implements Packet {
    public static final Function<String, String> MODERNISE = tag -> {
        // Transform as per Bukkit
        if ( tag.equals( "BungeeCord" ) )
        {
            return "bungeecord:main";
        }
        if ( tag.equals( "bungeecord:main" ) )
        {
            return "BungeeCord";
        }

        // Code that gets to here is UNLIKELY to be viable on the Bukkit side of side things,
        // but we keep it anyway. It will eventually be enforced API side.
        if ( tag.indexOf( ':' ) != -1 )
        {
            return tag;
        }

        return "legacy:" + tag.toLowerCase( Locale.ROOT );
    };
    public static final Predicate<InPluginMessagePacket> SHOULD_RELAY = input -> ( input.getTag().equals("REGISTER" ) || input.getTag().equals("minecraft:register" ) || input.getTag().equals("MC|Brand" ) || input.getTag().equals("minecraft:brand" ) ) && input.getData().length < Byte.MAX_VALUE;

    private String tag;
    private byte[] data;

    /**
     * Allow this packet to be sent as an "extended" packet.
     */
    private boolean allowExtendedPacket = false;

    @Override
    public void read(ByteBuf buf, Version version) {
        tag = ( version.isAfterOrEq(Version.MINECRAFT_1_13)) ? MODERNISE.apply( readString( buf ) ) : readString( buf );
        int maxSize = 0x100000;
        Preconditions.checkArgument( buf.readableBytes() < maxSize );
        data = new byte[ buf.readableBytes() ];
        buf.readBytes( data );
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeString( ( version.isAfterOrEq(Version.MINECRAFT_1_13) ) ? MODERNISE.apply( tag ) : tag, buf );
        buf.writeBytes( data );
    }
}
