package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Position;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class InUpdateSignPacket implements Packet {

    private int x;
    private int y;
    private int z;
    private String[] contents;

    public InUpdateSignPacket() {}

    public InUpdateSignPacket(int x, int y, int z, String... contents) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.contents = contents;
    }

    @Override
    public void read(ByteBuf buf, Version version) {
        contents = new String[4];

        long position = buf.readLong();
        x = Position.getX(position, version);
        y = Position.getY(position, version);
        z = Position.getZ(position, version);

        for(int i = 0; i < 4; i++) {
            String line = PacketUtils.readString(buf);
            contents[i] = line;
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeLong(Position.getPosition(x, y, z, version));

        for(String line: contents) {
            PacketUtils.writeString(line, buf);
        }
    }
}
