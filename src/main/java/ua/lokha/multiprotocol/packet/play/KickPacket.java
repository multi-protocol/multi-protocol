package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readString;
import static ua.lokha.multiprotocol.util.PacketUtils.writeString;

@Data
public class KickPacket implements Packet {

    private String message;

    @Override
    public void read(ByteBuf buf, Version version) {
        message = readString( buf );
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeString( message, buf );
    }
}
