package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import net.minecraft.server.v1_12_R1.MapIcon;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
//TODO: сделать ремап для 1.13+
public class MapPacket implements Packet {

    private int mapId;
    private byte scale;
    private boolean trackingPosition; //1.9+
    private boolean locked; //1.13+
    private int iconCount;
    private MapIcon[] mapIcons;
    private short columns;
    private short rows;
    private short minX;
    private short minY;
    private byte[] mapData;

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isAfterOrEq(Version.MINECRAFT_1_13))
            throw new UnsupportedVersionException(version);

        mapId = PacketUtils.readVarInt(buf);
        scale = buf.readByte();

        if(version.isAfterOrEq(Version.MINECRAFT_1_9))
            trackingPosition = buf.readBoolean();

        iconCount = PacketUtils.readVarInt(buf);
        mapIcons = new MapIcon[iconCount];

        for(int i = 0; i < iconCount; i++) {
            short code = (short) buf.readByte();
            mapIcons[i] = new MapIcon(MapIcon.Type.a((byte) (code >> 4 & 15)), buf.readByte(), buf.readByte(), (byte) (code & 15));
        }

        columns = buf.readUnsignedByte();

        if(columns > 0) {
            rows = buf.readUnsignedByte();
            minX = buf.readUnsignedByte();
            minY = buf.readUnsignedByte();
            mapData = PacketUtils.readArray(buf);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isAfterOrEq(Version.MINECRAFT_1_13))
            throw new UnsupportedVersionException(version);

        PacketUtils.writeVarInt(mapId, buf);
        buf.writeByte(scale);

        if(version.isAfterOrEq(Version.MINECRAFT_1_9))
            buf.writeBoolean(trackingPosition);

        PacketUtils.writeVarInt(iconCount, buf);

        for(MapIcon icon: mapIcons) {
            buf.writeByte((icon.getType() & 15) << 4 | icon.getRotation() & 15);
            buf.writeByte(icon.getX());
            buf.writeByte(icon.getY());
        }

        buf.writeByte(columns);

        if(columns > 0) {
            buf.writeByte(rows);
            buf.writeByte(minX);
            buf.writeByte(minY);
            PacketUtils.writeArray(mapData, buf);
        }
    }
}
