package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.BlockProtocol;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
@Log
public class MultiBlockChangePacket implements Packet {

    public int chunkX;
    public int chunkZ;
    public Records[] records;

    @Override
    public void read(ByteBuf buf, Version version) {
        int records$count = 0;
        chunkX = buf.readInt();
        chunkZ = buf.readInt();
        records$count = readVarInt(buf);
        int max_i_records = records$count;
        records = new Records[max_i_records];
        for (int i_records = 0; i_records < max_i_records; i_records++) {
            records[i_records] = new Records();
            records[i_records].read(buf, version);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeInt(chunkX);
        buf.writeInt(chunkZ);
        int records$count = records.length;
        writeVarInt(records$count, buf);
        for (Records records : this.records) {
            records.write(buf, version);
        }
    }

    @Data
    public static class Records {
        private int horizontalPos;
        private int y;
        private String blockType;

        public void read(ByteBuf in, Version version) {
            horizontalPos = in.readUnsignedByte();
            y = in.readUnsignedByte();
            int id = readVarInt(in);
            blockType = BlockProtocol.BLOCK_PROTOCOL.getValue(id, version);
            if (blockType == null) {
                log.warning("blockType not found by id " + id + " and version " + version);
                blockType = BlockProtocol.AIR;
            }
        }

        public void write(ByteBuf out, Version version) {
            out.writeByte(horizontalPos);
            out.writeByte(y);
            int id = BlockProtocol.BLOCK_PROTOCOL.getId(blockType, version);
            if (id == -1) {
                log.warning("id not found by blockType " + blockType + " and version " + version);
                id = 0;
            }
            writeVarInt(id, out);
        }
    }
}
