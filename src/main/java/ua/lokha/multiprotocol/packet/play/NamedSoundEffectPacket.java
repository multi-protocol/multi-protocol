package ua.lokha.multiprotocol.packet.play;


import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.sound.SoundRemapper;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

/**
 * В 1.8 этого пакета не было, он был объединен с SoundEffectPacket
 *
 * @see SoundEffectPacket
 */
@Data
public class NamedSoundEffectPacket implements Packet {

    private int id;
    private int category;
    private int x;
    private int y;
    private int z;
    private float volume;
    private float pitch;

    private String soundName;

    public NamedSoundEffectPacket(SoundEffectPacket packet, Version version) {
        this.id = packet.getId();
        this.category = packet.getCategory();
        this.x = packet.getX();
        this.y = packet.getY();
        this.z = packet.getZ();
        this.volume = packet.getVolume();
        this.pitch = packet.getPitch();
        this.soundName = SoundRemapper.getSoundName(version, id);
    }

    public NamedSoundEffectPacket() {

    }

    @Override
    public void read(ByteBuf buf, Version version) {
        soundName = readString(buf);
        category = readVarInt(buf);
        x = buf.readInt();
        y = buf.readInt();
        z = buf.readInt();
        volume = buf.readFloat();
        pitch = buf.readFloat();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeString(soundName, buf);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            writeVarInt(category, buf);
        }
        buf.writeInt(x);
        buf.writeInt(y);
        buf.writeInt(z);
        buf.writeFloat(volume);
        if (version.isAfterOrEq(Version.MINECRAFT_1_10)) {
            buf.writeFloat(pitch);
        } else {
            buf.writeByte((int) (pitch * 63.5));
        }
    }
}
