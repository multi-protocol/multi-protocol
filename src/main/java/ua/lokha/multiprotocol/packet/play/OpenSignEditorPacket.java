package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Position;

@Data
public class OpenSignEditorPacket implements Packet {

    private int x;
    private int y;
    private int z;

    @Override
    public void read(ByteBuf buf, Version version) {
        long position = buf.readLong();
        x = Position.getX(position, version);
        y = Position.getY(position, version);
        z = Position.getZ(position, version);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeLong(Position.getPosition(x, y, z, version));
    }
}
