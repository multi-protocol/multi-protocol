package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
public class OutKeepAlivePacket implements Packet {
    private long randomId;

    @Override
    public void read(ByteBuf buf, Version version) {
        this.randomId = version.isAfterOrEq(Version.MINECRAFT_1_12_2) ? buf.readLong() : (long)readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_12_2)) {
            buf.writeLong(this.randomId);
        } else {
            writeVarInt((int)this.randomId, buf);
        }
    }
}
