package ua.lokha.multiprotocol.packet.play;

import com.google.gson.JsonObject;
import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Position;
import ua.lokha.multiprotocol.util.NbtUtils;
import ua.lokha.multiprotocol.util.PacketUtils;

@Log
@Data
public class OutUpdateSignPacket implements Packet {

    private int x;
    private int y;
    private int z;
    private String[] contents;

    public OutUpdateSignPacket() {}

    public OutUpdateSignPacket(int x, int y, int z, String... contents) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.contents = contents;
    }

    @Override
    public void read(ByteBuf buf, Version version) {
        long position = buf.readLong();
        x = Position.getX(position, version);
        y = Position.getY(position, version);
        z = Position.getZ(position, version);

        contents = new String[4];

        for(int i = 0; i < 4; i++) {
            contents[i] = PacketUtils.readString(buf);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeLong(Position.getPosition(x, y, z, version));

        for(String line: contents) {
            if(version.isBefore(Version.MINECRAFT_1_9)) {
                JsonObject object = NbtUtils.GSON.fromJson(line, JsonObject.class);
                if(object.has("extra")) {
                    String text = object.get("extra").getAsJsonArray().get(0).getAsJsonObject().get("text").getAsString();
                    line = NbtUtils.fixJson(text);
                }
            }

            PacketUtils.writeString(line, buf);
        }
    }
}
