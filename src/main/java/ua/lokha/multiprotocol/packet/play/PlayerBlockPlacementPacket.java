package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.type.Position;
import ua.lokha.multiprotocol.type.Slot;

import java.util.Collections;
import java.util.List;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
public class PlayerBlockPlacementPacket implements Packet {

    private int x;
    private int y;
    private int z;
    private int face;

    private Slot slot; //для 1.8
    private int hand;

    private float cursorX, cursorY, cursorZ;

    private boolean insideBlock = false;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_14)) {
            hand = readVarInt(buf);

            long position = buf.readLong();
            x = Position.getX(position, version);
            y = Position.getY(position, version);
            z = Position.getZ(position, version);

            face = readVarInt(buf);
        } else {

            long position = buf.readLong();
            x = Position.getX(position, version);
            y = Position.getY(position, version);
            z = Position.getZ(position, version);

            if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
                face = readVarInt(buf);
                hand = readVarInt(buf);
            } else {
                face = buf.readByte();
                slot = new Slot();
                slot.read(buf, version);
            }
        }

        if (version.isAfterOrEq(Version.MINECRAFT_1_11)) {
            cursorX = buf.readFloat();
            cursorY = buf.readFloat();
            cursorZ = buf.readFloat();
        } else if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            cursorX = buf.readUnsignedByte() / 16.0F;
            cursorY = buf.readUnsignedByte() / 16.0F;
            cursorZ = buf.readUnsignedByte() / 16.0F;
        } else {
            cursorX = buf.readByte() / 16.0F;
            cursorY = buf.readByte() / 16.0F;
            cursorZ = buf.readByte() / 16.0F;
        }

        if (version.isAfterOrEq(Version.MINECRAFT_1_14)) {
            insideBlock = buf.readBoolean();
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_14)) {
            writeVarInt(hand, buf);
            buf.writeLong(Position.getPosition(x, y, z, version));
            writeVarInt(face, buf);
        } else {
            buf.writeLong(Position.getPosition(x, y, z, version));
            if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
                writeVarInt(face, buf);
                writeVarInt(hand, buf);
            } else {
                buf.writeByte(face);
                if (slot == null) {
                    // мы сервак не будем запускать на ядре 1.8, по этому не буду делать тут remap
                    throw new UnsupportedVersionException(version, "slot is null");
                } else {
                    slot.write(buf, version);
                }
            }
        }

        if (version.isAfterOrEq(Version.MINECRAFT_1_11)) {
            buf.writeFloat(cursorX);
            buf.writeFloat(cursorY);
            buf.writeFloat(cursorZ);
        } else if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            buf.writeByte((short) (cursorX * 16.0F));
            buf.writeByte((short) (cursorY * 16.0F));
            buf.writeByte((short) (cursorZ * 16.0F));
        } else {
            buf.writeByte((byte) (cursorX * 16.0F));
            buf.writeByte((byte) (cursorY * 16.0F));
            buf.writeByte((byte) (cursorZ * 16.0F));
        }

        if (version.isAfterOrEq(Version.MINECRAFT_1_14)) {
            buf.writeBoolean(insideBlock);
        }
    }

    @Override
    public List<Packet> map(Version version) {
        if (face == -1 && version.isAfterOrEq(Version.MINECRAFT_1_12_1)) {
            return Collections.singletonList(new UseItemPacket(hand));
        }
        return Collections.singletonList(this);
    }
}
