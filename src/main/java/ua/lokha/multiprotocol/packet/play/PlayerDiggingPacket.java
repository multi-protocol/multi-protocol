package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Position;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class PlayerDiggingPacket implements Packet {

    private int action;
    private int x;
    private int y;
    private int z;
    private short facing;

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            action = buf.readByte();
        else
            action = PacketUtils.readVarInt(buf);

        long position = buf.readLong();
        x = Position.getX(position, version);
        y = Position.getY(position, version);
        z = Position.getZ(position, version);

        facing = buf.readUnsignedByte();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            PacketUtils.writeVarInt(action, buf);
        else
            buf.writeByte(action);

        buf.writeLong(Position.getPosition(x, y, z, version));
        buf.writeByte(facing);
    }
}
