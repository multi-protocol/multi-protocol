package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class ResourcePackSendPacket implements Packet {

    private String url;
    private String hash;

    @Override
    public void read(ByteBuf buf, Version version) {
        url = PacketUtils.readString(buf);
        hash = PacketUtils.readString(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeString(url, buf);
        PacketUtils.writeString(hash, buf);
    }
}
