package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readString;
import static ua.lokha.multiprotocol.util.PacketUtils.writeString;

@Data
public class RespawnPacket implements Packet {
    private int dimension;
    private short difficulty;
    private short gameMode;
    private String levelType;

    @Override
    public void read(ByteBuf buf, Version version) {
        dimension = buf.readInt();
        if ( version.isBefore(Version.MINECRAFT_1_14) )
        {
            difficulty = buf.readUnsignedByte();
        }
        gameMode = buf.readUnsignedByte();
        levelType = readString( buf );
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeInt( dimension );
        if ( version.isBefore(Version.MINECRAFT_1_14) )
        {
            buf.writeByte( difficulty );
        }
        buf.writeByte( gameMode );
        writeString( levelType, buf );
    }
}
