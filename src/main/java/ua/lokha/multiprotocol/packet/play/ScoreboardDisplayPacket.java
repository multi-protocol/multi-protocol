package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readString;
import static ua.lokha.multiprotocol.util.PacketUtils.writeString;
@Data
public class ScoreboardDisplayPacket implements Packet {
    /**
     * 0 = list, 1 = side, 2 = below.
     */
    private byte position;
    private String name;

    @Override
    public void read(ByteBuf buf, Version version) {
        position = buf.readByte();
        name = readString( buf );
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeByte( position );
        writeString( name, buf );
    }
}
