package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import java.util.Locale;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

@Data
public class ScoreboardObjectivePacket implements Packet {

    private String name;
    private String value;
    private HealthDisplay type;
    /**
     * 0 to create, 1 to remove, 2 to update display text.
     */
    private byte action;

    @Override
    public void read(ByteBuf buf, Version version) {
        name = readString( buf );
        action = buf.readByte();
        if ( action == 0 || action == 2 )
        {
            value = readString( buf );
            if ( version.isAfterOrEq(Version.MINECRAFT_1_13) )
            {
                type = HealthDisplay.values()[readVarInt( buf )];
            } else
            {
                type = HealthDisplay.fromString( readString( buf ) );
            }
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeString( name, buf );
        buf.writeByte( action );
        if ( action == 0 || action == 2 )
        {
            writeString( value, buf );
            if ( version.isAfterOrEq(Version.MINECRAFT_1_13) )
            {
                writeVarInt( type.ordinal(), buf );
            } else
            {
                writeString( type.toString(), buf );
            }
        }
    }

    public enum HealthDisplay
    {

        INTEGER, HEARTS;

        @Override
        public String toString()
        {
            return super.toString().toLowerCase( Locale.ROOT );
        }

        public static HealthDisplay fromString(String s)
        {
            return valueOf( s.toUpperCase( Locale.ROOT ) );
        }
    }
}
