package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.*;
@Data
public class ScoreboardScorePacket implements Packet {
    private String itemName;
    /**
     * 0 = create / update, 1 = remove.
     */
    private byte action;
    private String scoreName;
    private int value;

    @Override
    public void read(ByteBuf buf, Version version) {
        itemName = readString( buf );
        action = buf.readByte();
        scoreName = readString( buf );
        if ( action != 1 )
        {
            value = readVarInt( buf );
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeString( itemName, buf );
        buf.writeByte( action );
        writeString( scoreName, buf );
        if ( action != 1 )
        {
            writeVarInt( value, buf );
        }
    }
}
