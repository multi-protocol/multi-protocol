package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class SelectAdvancementTabPacket implements Packet {

    private String identifier;

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_12))
            throw new UnsupportedVersionException(version);
        boolean hasId = buf.readBoolean();
        if(hasId) {
            identifier = PacketUtils.readString(buf);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_12))
            throw new UnsupportedVersionException(version);
        if(identifier != null) {
            buf.writeBoolean(true);
            PacketUtils.writeString(identifier, buf);
        } else {
            buf.writeBoolean(false);
        }
    }
}
