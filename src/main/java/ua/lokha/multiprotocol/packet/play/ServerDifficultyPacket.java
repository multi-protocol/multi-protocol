package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
public class ServerDifficultyPacket implements Packet {
    private short difficulty;
    private boolean difficultyLocked;

    @Override
    public void read(ByteBuf buf, Version version) {
        difficulty = buf.readUnsignedByte();
        if (version.isAfterOrEq(Version.MINECRAFT_1_14)) {
            difficultyLocked = buf.readBoolean();
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeByte(difficulty);
        if (version.isAfterOrEq(Version.MINECRAFT_1_14)) {
            buf.writeBoolean(difficultyLocked);
        }
    }
}
