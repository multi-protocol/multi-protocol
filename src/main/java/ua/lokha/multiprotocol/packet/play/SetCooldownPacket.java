package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class SetCooldownPacket implements Packet {

    private int itemId;
    private int cooldownTicks;

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            throw new UnsupportedVersionException(version);
        itemId = PacketUtils.readVarInt(buf);
        cooldownTicks = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            throw new UnsupportedVersionException(version);
        PacketUtils.writeVarInt(itemId, buf);
        PacketUtils.writeVarInt(cooldownTicks, buf);
    }


}
