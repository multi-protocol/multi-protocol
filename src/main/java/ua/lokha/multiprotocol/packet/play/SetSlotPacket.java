package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Slot;

@Data
public class SetSlotPacket implements Packet {

    private byte windowId;
    private short slotId;
    private Slot item;

    @Override
    public void read(ByteBuf buf, Version version) {
        windowId = buf.readByte();
        slotId = buf.readShort();
        item = new Slot();
        item.read(buf, version);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeByte(windowId);
        buf.writeShort(slotId);
        item.write(buf, version);
    }
}
