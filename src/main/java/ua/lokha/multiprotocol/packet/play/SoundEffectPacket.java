package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.sound.SoundRemapper;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

@Data
public class SoundEffectPacket implements Packet {

    private int id;
    private int category;
    private int x;
    private int y;
    private int z;
    private float volume;
    private float pitch;

    @Override
    public void read(ByteBuf buf, Version version) {
        id = readVarInt(buf);
        category = readVarInt(buf);
        x = buf.readInt();
        y = buf.readInt();
        z = buf.readInt();
        volume = buf.readFloat();
        pitch = buf.readFloat();
    }

    @Override
    public List<Packet> map(Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            return Collections.singletonList(new NamedSoundEffectPacket(this, version));
        }
        return Collections.singletonList(this);
    }

    @Override
    public void write(ByteBuf buf, Version version) { // for 1.8 clients
        String soundName = SoundRemapper.getSoundName(version, id);
        writeString(soundName, buf);
        buf.writeInt(x);
        buf.writeInt(y);
        buf.writeInt(z);
        buf.writeFloat(volume);
        buf.writeByte((int) (pitch * 63.5));
    }

}
