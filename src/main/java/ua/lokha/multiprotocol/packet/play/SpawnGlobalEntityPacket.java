package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
public class SpawnGlobalEntityPacket implements Packet {
    private int entityId;
    private byte type;
    private double x;
    private double y;
    private double z;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = readVarInt(buf);
        type = buf.readByte();
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            x = buf.readDouble();
            y = buf.readDouble();
            z = buf.readDouble();
        } else {
            x = buf.readInt() / 32.0D;
            y = buf.readInt() / 32.0D;
            z = buf.readInt() / 32.0D;
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(entityId, buf);
        buf.writeByte(type);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            buf.writeDouble(x);
            buf.writeDouble(y);
            buf.writeDouble(z);
        } else {
            buf.writeInt((int) (x * 32.0D));
            buf.writeInt((int) (y * 32.0D));
            buf.writeInt((int) (z * 32.0D));
        }
    }
}
