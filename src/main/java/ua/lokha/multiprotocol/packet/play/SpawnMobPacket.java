package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.RemapConventions;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.EntityProtocol;
import ua.lokha.multiprotocol.type.EntityType;
import ua.lokha.multiprotocol.type.entitymetadata.EntityMetadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataProtocol;
import ua.lokha.multiprotocol.util.PacketUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

@Data
@Log
public class SpawnMobPacket implements Packet {
    private int entityId;
    private UUID uuid;
    private EntityType entityType;
    private double x;
    private double y;
    private double z;
    private byte yaw;
    private byte pitch;
    private byte headPitch;
    private short velocityX;
    private short velocityY;
    private short velocityZ;
    private EntityMetadata metadata;

    @Override
    public void read(ByteBuf buf, Version version) {
        entityId = readVarInt(buf);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            uuid = readUUID(buf);
        }
        int typeId;
        if (version.isAfterOrEq(Version.MINECRAFT_1_11)) {
            typeId = PacketUtils.readVarInt(buf);
        } else {
            typeId = buf.readUnsignedByte();
        }
        entityType = EntityProtocol.MOB_PROTOCOL.getEnum(typeId, version);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            x = buf.readDouble();
            y = buf.readDouble();
            z = buf.readDouble();
        } else {
            x = buf.readInt() / 32.0D;
            y = buf.readInt() / 32.0D;
            z = buf.readInt() / 32.0D;
        }
        yaw = buf.readByte();
        pitch = buf.readByte();
        headPitch = buf.readByte();
        velocityX = buf.readShort();
        velocityY = buf.readShort();
        velocityZ = buf.readShort();

        if (version.isBefore(Version.MINECRAFT_1_15)) {
            try {
                MetadataProtocol entity = MetadataProtocol.getByEntity(entityType);
                metadata = new EntityMetadata(entity);
                metadata.read(buf, version);
            } catch (IllegalStateException e) {
                log.warning("Metadata protocol not found by entity type " + entityType);
            }
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(entityId, buf);
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            writeUUID(uuid != null ? uuid : RemapConventions.entityIdToUUID(entityId), buf);
        }
        int typeId = EntityProtocol.MOB_PROTOCOL.getId(entityType, version);
        if (version.isAfterOrEq(Version.MINECRAFT_1_11)) {
            PacketUtils.writeVarInt(typeId, buf);
        } else {
            buf.writeByte(typeId);
        }
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            buf.writeDouble(x);
            buf.writeDouble(y);
            buf.writeDouble(z);
        } else {
            buf.writeInt((int) (x * 32.0D));
            buf.writeInt((int) (y * 32.0D));
            buf.writeInt((int) (z * 32.0D));
        }
        buf.writeByte(yaw);
        buf.writeByte(pitch);
        buf.writeByte(headPitch);
        buf.writeShort(velocityX);
        buf.writeShort(velocityY);
        buf.writeShort(velocityZ);

        if (version.isBefore(Version.MINECRAFT_1_15)) {
            if (metadata != null) {
                metadata.write(buf, version);
            } else {
                buf.writeByte(EntityMetadata.getEndIndex(version));
            }
        }
    }

    @Override
    public List<Packet> map(Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_15) && metadata != null) {
            List<Packet> packets = new ArrayList<>(2);
            packets.add(this);
            packets.add(new EntityMetadataPacket(entityId, entityType, metadata));
            return packets;
        }

        return Collections.singletonList(this);
    }
}
