package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;

@Data
public class SteerBoatPacket implements Packet {

    private boolean leftPaddleTurning;
    private boolean rightPaddleTurning;

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            throw new UnsupportedVersionException(version);

        leftPaddleTurning = buf.readBoolean();
        rightPaddleTurning = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            throw new UnsupportedVersionException(version);

        buf.writeBoolean(leftPaddleTurning);
        buf.writeBoolean(rightPaddleTurning);
    }
}
