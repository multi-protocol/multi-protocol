package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
public class SteerVehiclePacket implements Packet {

    private float sideWays;
    private float forward;
    private short flags;

    @Override
    public void read(ByteBuf buf, Version version) {
        sideWays = buf.readFloat();
        forward = buf.readFloat();
        flags = buf.readUnsignedByte();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeFloat(sideWays);
        buf.writeFloat(forward);
        buf.writeByte(flags);
    }
}
