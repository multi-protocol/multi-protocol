package ua.lokha.multiprotocol.packet.play;

import com.mojang.brigadier.LiteralMessage;
import com.mojang.brigadier.context.StringRange;
import com.mojang.brigadier.suggestion.Suggestion;
import com.mojang.brigadier.suggestion.Suggestions;
import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import java.util.LinkedList;
import java.util.List;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

@Data
public class TabCompleteResponsePacket implements Packet {
    private int transactionId;
    private Suggestions suggestions;
    //
    private List<String> commands;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_13)) {
            transactionId = readVarInt(buf);
            int start = readVarInt(buf);
            int length = readVarInt(buf);
            StringRange range = StringRange.between(start, start + length);

            int cnt = readVarInt(buf);
            List<Suggestion> matches = new LinkedList<>();
            for (int i = 0; i < cnt; i++) {
                String match = readString(buf);
                String tooltip = buf.readBoolean() ? readString(buf) : null;

                matches.add(new Suggestion(range, match, new LiteralMessage(tooltip)));
            }

            suggestions = new Suggestions(range, matches);
        }

        if (version.isBefore(Version.MINECRAFT_1_13)) {
            commands = readStringArray(buf);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_13)) {
            writeVarInt(transactionId, buf);
            writeVarInt(suggestions.getRange().getStart(), buf);
            writeVarInt(suggestions.getRange().getLength(), buf);

            writeVarInt(suggestions.getList().size(), buf);
            for (Suggestion suggestion : suggestions.getList()) {
                writeString(suggestion.getText(), buf);
                buf.writeBoolean(suggestion.getTooltip() != null && suggestion.getTooltip().getString() != null);
                if (suggestion.getTooltip() != null && suggestion.getTooltip().getString() != null) {
                    writeString(suggestion.getTooltip().getString(), buf);
                }
            }
        }

        if (version.isBefore(Version.MINECRAFT_1_13)) {
            writeStringArray(commands, buf);
        }
    }
}
