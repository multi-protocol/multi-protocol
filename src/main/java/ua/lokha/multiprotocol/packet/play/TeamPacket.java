package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.*;

@Data
public class TeamPacket implements Packet {

    private String name;
    /**
     * 0 - create, 1 remove, 2 info update, 3 player add, 4 player remove.
     */
    private byte mode;
    private String displayName;
    private String prefix;
    private String suffix;
    private String nameTagVisibility;
    private String collisionRule;
    private int color;
    private byte friendlyFire;
    private String[] players;

    @Override
    public void read(ByteBuf buf, Version version) {
        name = readString( buf );
        mode = buf.readByte();
        if ( mode == 0 || mode == 2 )
        {
            displayName = readString( buf );
            if ( version.isBefore(Version.MINECRAFT_1_13) )
            {
                prefix = readString( buf );
                suffix = readString( buf );
            }
            friendlyFire = buf.readByte();
            nameTagVisibility = readString( buf );
            if ( version.isAfterOrEq(Version.MINECRAFT_1_9) )
            {
                collisionRule = readString( buf );
            }
            color = ( version.isAfterOrEq(Version.MINECRAFT_1_13) ) ? readVarInt( buf ) : buf.readByte();
            if ( version.isAfterOrEq(Version.MINECRAFT_1_13) )
            {
                prefix = readString( buf );
                suffix = readString( buf );
            }
        }
        if ( mode == 0 || mode == 3 || mode == 4 )
        {
            int len = readVarInt( buf );
            players = new String[ len ];
            for ( int i = 0; i < len; i++ )
            {
                players[i] = readString( buf );
            }
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeString( name, buf );
        buf.writeByte( mode );
        if ( mode == 0 || mode == 2 )
        {
            writeString( displayName, buf );
            if ( version.isBefore(Version.MINECRAFT_1_13) )
            {
                writeString( prefix, buf );
                writeString( suffix, buf );
            }
            buf.writeByte( friendlyFire );
            writeString( nameTagVisibility, buf );
            if ( version.isAfterOrEq(Version.MINECRAFT_1_9) )
            {
                writeString( collisionRule, buf );
            }

            if ( version.isAfterOrEq(Version.MINECRAFT_1_13) )
            {
                writeVarInt( color, buf );
                writeString( prefix, buf );
                writeString( suffix, buf );
            } else
            {
                buf.writeByte( color );
            }
        }
        if ( mode == 0 || mode == 3 || mode == 4 )
        {
            writeVarInt( players.length, buf );
            for ( String player : players )
            {
                writeString( player, buf );
            }
        }
    }
}
