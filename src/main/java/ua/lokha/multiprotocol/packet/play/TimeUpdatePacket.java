package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
public class TimeUpdatePacket implements Packet {

    public long age;
    public long time;

    @Override
    public void read(ByteBuf buf, Version version) {
        age = buf.readLong();
        time = buf.readLong();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeLong(age);
        buf.writeLong(time);
    }
}
