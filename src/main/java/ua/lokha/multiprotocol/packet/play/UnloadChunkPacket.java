package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;

import java.util.Collections;
import java.util.List;

@Data
public class UnloadChunkPacket implements Packet {
    private int chunkX;
    private int chunkZ;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version);
        }
        chunkX = buf.readInt();
        chunkZ = buf.readInt();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version);
        }
        buf.writeInt(chunkX);
        buf.writeInt(chunkZ);
    }

    @Override
    public List<Packet> map(Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_9)) {
            return Collections.singletonList(this);
        } else {
            ChunkDataPacket packet = new ChunkDataPacket();
            packet.setChunkX(chunkX);
            packet.setChunkZ(chunkZ);
            packet.setPrimaryBitMask(0);
            packet.setGroundUpContinuous(true);
            return Collections.singletonList(packet);
        }
    }
}
