package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class UnlockRecipesPacket implements Packet {
    private int action;
    private boolean craftingBookOpen;
    private boolean filteringCraftable;
    private int[] array1;
    private int[] array2;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_12)) {
            throw new UnsupportedVersionException(version);
        }
        action = PacketUtils.readVarInt(buf);
        craftingBookOpen = buf.readBoolean();
        filteringCraftable = buf.readBoolean();
        array1 = PacketUtils.readVarIntArray(buf);
        if (action == 0) {
            array2 = PacketUtils.readVarIntArray(buf);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_12)) {
            throw new UnsupportedVersionException(version);
        }
        PacketUtils.writeVarInt(action, buf);
        buf.writeBoolean(craftingBookOpen);
        buf.writeBoolean(filteringCraftable);
        PacketUtils.writeVarIntArray(array1, buf);
        if (action == 0) {
            PacketUtils.writeVarIntArray(array2, buf);
        }
    }
}
