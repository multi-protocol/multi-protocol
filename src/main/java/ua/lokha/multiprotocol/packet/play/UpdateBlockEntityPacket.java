package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.extern.java.Log;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Position;
import ua.lokha.multiprotocol.type.nbt.CompoundTag;
import ua.lokha.multiprotocol.type.nbt.Tag;
import ua.lokha.multiprotocol.util.NbtUtils;

import java.util.Collections;
import java.util.List;

//TODO: remap
@Data
@Log
public class UpdateBlockEntityPacket implements Packet {

    private int x;
    private int y;
    private int z;
    private short action;
    private Tag tileTag;

    public UpdateBlockEntityPacket() {}

    public UpdateBlockEntityPacket(int x, int y, int z, short action, Tag tileTag) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.action = action;
        this.tileTag = tileTag;
    }

    @Override
    public void read(ByteBuf buf, Version version) {
        long position = buf.readLong();
        x = Position.getX(position, version);
        y = Position.getY(position, version);
        z = Position.getZ(position, version);

        action = buf.readUnsignedByte();
        tileTag = Tag.readNamedTag(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeLong(Position.getPosition(x, y, z, version));
        buf.writeByte(action);
        Tag.writeNamedTag(tileTag, buf);
    }

    @Override
    public List<Packet> map(Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9_4) && action == 9)
            return Collections.singletonList(new OutUpdateSignPacket(x, y, z, NbtUtils.readSignLines((CompoundTag) tileTag)));

        if(version.isBefore(Version.MINECRAFT_1_9_2) && action > 6)
            return Collections.emptyList();
        else if(version.isBefore(Version.MINECRAFT_1_9_4) && action > 8)
            return Collections.emptyList();
        else if(version.isBefore(Version.MINECRAFT_1_11_1) && action > 9)
            return Collections.emptyList();
        else if(version.isBefore(Version.MINECRAFT_1_12) && action > 10)
            return Collections.emptyList();
        else if(version.isBefore(Version.MINECRAFT_1_14) && action > 11)
            return Collections.emptyList();

        return Collections.singletonList(this);
    }
}
