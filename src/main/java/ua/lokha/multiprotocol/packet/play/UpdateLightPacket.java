package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
public class UpdateLightPacket implements Packet {

    private int chunkX;
    private int chunkZ;
    private int skyLightMask;
    private int blockLightMask;
    private int emptySkyLightMask;
    private int emptyBlockLightMask;

    @Override
    public void read(ByteBuf buf, Version version) {
        chunkX = readVarInt(buf);
        chunkZ = readVarInt(buf);
        skyLightMask = readVarInt(buf);
        blockLightMask = readVarInt(buf);
        emptySkyLightMask = readVarInt(buf);
        emptyBlockLightMask = readVarInt(buf);

    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(chunkX, buf);
        writeVarInt(chunkZ, buf);
        writeVarInt(skyLightMask, buf);
        writeVarInt(blockLightMask, buf);
        writeVarInt(emptySkyLightMask, buf);
        writeVarInt(emptyBlockLightMask, buf);

    }
}
