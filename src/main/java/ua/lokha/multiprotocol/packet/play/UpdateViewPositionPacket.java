package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readVarInt;
import static ua.lokha.multiprotocol.util.PacketUtils.writeVarInt;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateViewPositionPacket implements Packet {
    private int chunkX;
    private int chunkZ;

    @Override
    public void read(ByteBuf buf, Version version) {
        chunkX = readVarInt(buf);
        chunkZ = readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        writeVarInt(chunkX, buf);
        writeVarInt(chunkZ, buf);
    }
}
