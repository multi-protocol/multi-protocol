package ua.lokha.multiprotocol.packet.play;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class WorldBorderPacket implements Packet {
    private int action;

    private double oldDiameter;
    private double newDiameter;
    private long speed;
    private double x;
    private double z;
    private int portalTeleportBoundary;
    private int warningTime;
    private int warningBlocks;

    @Override
    public void read(ByteBuf buf, Version version) {
        action = PacketUtils.readVarInt(buf);
        if (action == 0) {
            this.newDiameter = buf.readDouble();
        } else if (action == 1) {
            this.oldDiameter = buf.readDouble();
            this.newDiameter = buf.readDouble();
            this.speed = PacketUtils.readVarLong(buf);
        } else if (action == 2) {
            this.x = buf.readDouble();
            this.z = buf.readDouble();
        } else  if (action == 3) {
            this.x = buf.readDouble();
            this.z = buf.readDouble();
            this.oldDiameter = buf.readDouble();
            this.newDiameter = buf.readDouble();
            this.speed = PacketUtils.readVarLong(buf);
            this.portalTeleportBoundary = PacketUtils.readVarInt(buf);
            this.warningTime = PacketUtils.readVarInt(buf);
            this.warningBlocks = PacketUtils.readVarInt(buf);
        } else if (action == 4) {
            this.warningTime = PacketUtils.readVarInt(buf);
        } else if (action == 5) {
            this.warningBlocks = PacketUtils.readVarInt(buf);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(action, buf);
        if (action == 0) {
            buf.writeDouble(this.newDiameter);
        } else if (action == 1) {
            buf.writeDouble(this.oldDiameter);
            buf.writeDouble(this.newDiameter);
            PacketUtils.writeVarLong(this.speed, buf);
        } else if (action == 2) {
            buf.writeDouble(this.x);
            buf.writeDouble(this.z);
        } else  if (action == 3) {
            buf.writeDouble(this.x);
            buf.writeDouble(this.z);
            buf.writeDouble(this.oldDiameter);
            buf.writeDouble(this.newDiameter);
            PacketUtils.writeVarLong(this.speed, buf);
            PacketUtils.writeVarInt(this.portalTeleportBoundary, buf);
            PacketUtils.writeVarInt(this.warningTime, buf);
            PacketUtils.writeVarInt(this.warningBlocks, buf);
        } else if (action == 4) {
            PacketUtils.writeVarInt(this.warningTime, buf);
        } else if (action == 5) {
            PacketUtils.writeVarInt(this.warningBlocks, buf);
        }
    }
}
