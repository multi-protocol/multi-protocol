package ua.lokha.multiprotocol.packet.status;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

@Data
public class OutPingPacket implements Packet {
    private long time;

    @Override
    public void read(ByteBuf buf, Version version) {
        this.time = buf.readLong();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeLong(this.time);
    }
}
