package ua.lokha.multiprotocol.packet.status;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Packet;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.util.PacketUtils.readString;
import static ua.lokha.multiprotocol.util.PacketUtils.writeString;


@Data
public class StatusResponsePacket implements Packet {

    private String json;

    @Override
    public void read(ByteBuf buf, Version version) {
        json = readString(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        String remap = this.json.replaceFirst("\"protocol\":[0-9]+", "\"protocol\":" + version.getProtocolVersion());
        writeString(remap, buf);
    }
}
