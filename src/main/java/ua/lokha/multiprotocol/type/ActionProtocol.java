package ua.lokha.multiprotocol.type;

import ua.lokha.multiprotocol.Id2EnumVersionMapper;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.Id2EnumVersionMapper.map;

public class ActionProtocol {

    public static Id2EnumVersionMapper<ActionType> ACTION_PROTOCOL;

    static {
        ACTION_PROTOCOL = new Id2EnumVersionMapper<>(ActionType.class);

        ACTION_PROTOCOL.register(ActionType.START_SNEAK, map(Version.MINECRAFT_1_8, 0));
        ACTION_PROTOCOL.register(ActionType.STOP_SNEAK, map(Version.MINECRAFT_1_8, 1));
        ACTION_PROTOCOL.register(ActionType.LEAVE_BED, map(Version.MINECRAFT_1_8, 2));
        ACTION_PROTOCOL.register(ActionType.START_SPRINT, map(Version.MINECRAFT_1_8, 3));
        ACTION_PROTOCOL.register(ActionType.STOP_SPRINT, map(Version.MINECRAFT_1_8, 4));
        ACTION_PROTOCOL.register(ActionType.JUMP_HORSE, map(Version.MINECRAFT_1_8, 5));
        ACTION_PROTOCOL.register(ActionType.START_JUMP_HORSE, map(Version.MINECRAFT_1_9, 5));
        ACTION_PROTOCOL.register(ActionType.STOP_JUMP_HORSE, map(Version.MINECRAFT_1_9, 6));
        ACTION_PROTOCOL.register(ActionType.OPEN_HORSE, map(Version.MINECRAFT_1_8, 6), map(Version.MINECRAFT_1_9, 7));
        ACTION_PROTOCOL.register(ActionType.START_FLY_ELYTRA, map(Version.MINECRAFT_1_9, 8));
    }
}
