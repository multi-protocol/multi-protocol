package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class ByteType implements Type, MetadataType {
    private byte value;

    @Override
    public void read(ByteBuf buf, Version version) {
        value = buf.readByte();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeByte(value);
    }
}
