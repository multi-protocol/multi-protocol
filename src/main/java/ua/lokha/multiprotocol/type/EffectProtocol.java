package ua.lokha.multiprotocol.type;

import ua.lokha.multiprotocol.Id2EnumVersionMapper;
import ua.lokha.multiprotocol.Version;

import static ua.lokha.multiprotocol.Id2EnumVersionMapper.map;

public class EffectProtocol {

    public static Id2EnumVersionMapper<EffectType> EFFECT_PROTOCOL;

    static {
        EFFECT_PROTOCOL = new Id2EnumVersionMapper<>(EffectType.class);

        EFFECT_PROTOCOL.register(EffectType.SPEED, map(Version.MINECRAFT_1_8, 1));
        EFFECT_PROTOCOL.register(EffectType.SLOWNESS, map(Version.MINECRAFT_1_8, 2));
        EFFECT_PROTOCOL.register(EffectType.HASTE, map(Version.MINECRAFT_1_8, 3));
        EFFECT_PROTOCOL.register(EffectType.MINING_FATIGUE, map(Version.MINECRAFT_1_8, 4));
        EFFECT_PROTOCOL.register(EffectType.STRENGTH, map(Version.MINECRAFT_1_8, 5));
        EFFECT_PROTOCOL.register(EffectType.INSTANT_HEALTH, map(Version.MINECRAFT_1_8, 6));
        EFFECT_PROTOCOL.register(EffectType.INSTANT_DAMAGE, map(Version.MINECRAFT_1_8, 7));
        EFFECT_PROTOCOL.register(EffectType.JUMP_BOOST, map(Version.MINECRAFT_1_8, 8));
        EFFECT_PROTOCOL.register(EffectType.NAUSEA, map(Version.MINECRAFT_1_8, 9));
        EFFECT_PROTOCOL.register(EffectType.REGENERATION, map(Version.MINECRAFT_1_8, 10));
        EFFECT_PROTOCOL.register(EffectType.RESISTANCE, map(Version.MINECRAFT_1_8, 11));
        EFFECT_PROTOCOL.register(EffectType.FIRE_RESISTANCE, map(Version.MINECRAFT_1_8, 12));
        EFFECT_PROTOCOL.register(EffectType.WATER_BREATHING, map(Version.MINECRAFT_1_8, 13));
        EFFECT_PROTOCOL.register(EffectType.INVISIBILITY, map(Version.MINECRAFT_1_8, 14));
        EFFECT_PROTOCOL.register(EffectType.BLINDNESS, map(Version.MINECRAFT_1_8, 15));
        EFFECT_PROTOCOL.register(EffectType.NIGHT_VISION, map(Version.MINECRAFT_1_8, 16));
        EFFECT_PROTOCOL.register(EffectType.HUNGER, map(Version.MINECRAFT_1_8, 17));
        EFFECT_PROTOCOL.register(EffectType.WEAKNESS, map(Version.MINECRAFT_1_8, 18));
        EFFECT_PROTOCOL.register(EffectType.POISON, map(Version.MINECRAFT_1_8, 19));
        EFFECT_PROTOCOL.register(EffectType.WITHER, map(Version.MINECRAFT_1_8, 20));
        EFFECT_PROTOCOL.register(EffectType.HEALTH_BOOST, map(Version.MINECRAFT_1_8, 21));
        EFFECT_PROTOCOL.register(EffectType.ABSORPTION, map(Version.MINECRAFT_1_8, 22));
        EFFECT_PROTOCOL.register(EffectType.SATURATION, map(Version.MINECRAFT_1_8, 23));
        EFFECT_PROTOCOL.register(EffectType.GLOWING, map(Version.MINECRAFT_1_9, 24));
        EFFECT_PROTOCOL.register(EffectType.LEVITATION, map(Version.MINECRAFT_1_9, 25));
        EFFECT_PROTOCOL.register(EffectType.LUCK, map(Version.MINECRAFT_1_9, 26));
        EFFECT_PROTOCOL.register(EffectType.UNLUCK, map(Version.MINECRAFT_1_9, 27));
        EFFECT_PROTOCOL.register(EffectType.SLOW_FALLING, map(Version.MINECRAFT_1_13, 28));
        EFFECT_PROTOCOL.register(EffectType.CONDUIT_POWER, map(Version.MINECRAFT_1_13, 29));
        EFFECT_PROTOCOL.register(EffectType.DOLPHINS_GRACE, map(Version.MINECRAFT_1_13, 30));
        EFFECT_PROTOCOL.register(EffectType.BAD_OMEN, map(Version.MINECRAFT_1_14, 31));
        EFFECT_PROTOCOL.register(EffectType.VILLAGE_HERO, map(Version.MINECRAFT_1_14, 32));
    }
}
