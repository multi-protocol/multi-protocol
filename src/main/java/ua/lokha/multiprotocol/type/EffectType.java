package ua.lokha.multiprotocol.type;


import java.util.Arrays;
import java.util.List;

public enum EffectType {
    SPEED,
    SLOWNESS,
    HASTE,
    MINING_FATIGUE,
    STRENGTH,
    INSTANT_HEALTH,
    INSTANT_DAMAGE,
    RESISTANCE,
    JUMP_BOOST,
    NAUSEA,
    REGENERATION,
    FIRE_RESISTANCE,
    BLINDNESS,
    INVISIBILITY,
    WATER_BREATHING,
    NIGHT_VISION,
    HUNGER,
    WEAKNESS,
    POISON,
    WITHER,
    HEALTH_BOOST,
    ABSORPTION,
    SATURATION,
    GLOWING,
    LEVITATION,
    LUCK,
    UNLUCK,
    DOLPHINS_GRACE,
    CONDUIT_POWER,
    SLOW_FALLING,
    BAD_OMEN,
    VILLAGE_HERO;

    private static List<EffectType> values = Arrays.asList(values());
    public static List<EffectType> getTypes() {
        return values;
    }
}