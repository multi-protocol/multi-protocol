package ua.lokha.multiprotocol.type;

import ua.lokha.multiprotocol.Id2EnumVersionMapper;
import ua.lokha.multiprotocol.Version;

public class EntityProtocol {

    public static Id2EnumVersionMapper<EntityType> MOB_PROTOCOL;
    public static Id2EnumVersionMapper<EntityType> OBJECT_PROTOCOL;

    static {
        MOB_PROTOCOL = Id2EnumVersionMapper.loadFromResource("mob-protocol.json", EntityType.class);
        OBJECT_PROTOCOL = Id2EnumVersionMapper.loadFromResource("object-protocol.json", EntityType.class);
    }

    public static EntityType getByObjectId(int id, int data, Version version) {
        // вагонетки - единственный тип мобов, у которых id одинаковый, а различаются они по data
        if (version.isBeforeOrEq(Version.MINECRAFT_1_12_2)) {
            if (id == 10) {
                switch (data) {
                    case 1:
                        return EntityType.MINECART_CHEST;
                    case 2:
                        return EntityType.MINECART_FURNACE;
                    case 3:
                        return EntityType.MINECART_TNT;
                    case 4:
                        return EntityType.MINECART_SPAWNER;
                    case 5:
                        return EntityType.MINECART_HOPPER;
                    case 6:
                        return EntityType.MINECART_COMMAND_BLOCK;
                    case 0:
                    default:
                        return EntityType.MINECART_RIDEABLE;
                }
            }
        }

        return OBJECT_PROTOCOL.getEnum(id, version);
    }
}
