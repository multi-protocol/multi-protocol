package ua.lokha.multiprotocol.type;

import java.util.Arrays;
import java.util.List;

public enum EntityType {
    ITEM,
    EXPERIENCE_ORB,
    AREA_EFFECT_CLOUD,
    ELDER_GUARDIAN,
    WITHER_SKELETON,
    STRAY,
    THROWN_EGG,
    LEASH_KNOT,
    PAINTING,
    ARROW,
//    TIPPED_ARROW,
    SNOWBALL,
    FIREBALL,
    SMALL_FIREBALL,
    THROWN_ENDERPEARL,
    EYE_OF_ENDER,
    THROWN_POTION,
    THROWN_EXP_BOTTLE,
    ITEM_FRAME,
    WITHER_SKULL,
    PRIMED_TNT,
    FALLING_BLOCK,
    FIREWORK_ROCKET,
    HUSK,
    SPECTRAL_ARROW,
    SHULKER_BULLET,
    DRAGON_FIREBALL,
    ZOMBIE_VILLAGER,
    SKELETON_HORSE,
    ZOMBIE_HORSE,
    ARMOR_STAND,
    DONKEY,
    MULE,
    EVOCATION_FANGS,
    EVOCATION_ILLAGER,
    VEX,
    VINDICATION_ILLAGER,
    ILLUSION_ILLAGER,
    MINECART_COMMAND_BLOCK,
    BOAT,
    MINECART_RIDEABLE,
    MINECART_CHEST,
    MINECART_FURNACE,
    MINECART_TNT,
    MINECART_HOPPER,
    MINECART_SPAWNER,
    CREEPER,
    SKELETON,
    SPIDER,
    GIANT,
    ZOMBIE,
    SLIME,
    GHAST,
    PIG_ZOMBIE,
    ENDERMAN,
    CAVE_SPIDER,
    SILVERFISH,
    BLAZE,
    LAVA_SLIME,
    ENDER_DRAGON,
    WITHER,
    BAT,
    WITCH,
    ENDERMITE,
    GUARDIAN,
    SHULKER,
    PIG,
    SHEEP,
    COW,
    CHICKEN,
    SQUID,
    WOLF,
    MUSHROOM_COW,
    SNOWMAN,
    OCELOT,
    IRON_GOLEM,
    HORSE,
    RABBIT,
    POLAR_BEAR,
    LLAMA,
    LLAMA_SPIT,
    PARROT,
    VILLAGER,
    ENDER_CRYSTAL,
    FISHING_HOOK, // todo сделать remap 1.13 object-protocol.json
    PLAYER, // todo сделать remap 1.13 object-protocol.json
    TRIDENT, // todo сделать remap 1.13 object-protocol.json
    COD_MOB, // todo сделать remap 1.13 mob-protocol.json
    DOLPHIN, // todo сделать remap 1.13 mob-protocol.json
    DROWNED, // todo сделать remap 1.13 mob-protocol.json
    LIGHTNING_BOLT, // todo сделать remap 1.13 mob-protocol.json
    PHANTOM, // todo сделать remap 1.13 mob-protocol.json
    PUFFER_FISH, // todo сделать remap 1.13 mob-protocol.json
    SALMON_MOB, // todo сделать remap 1.13 mob-protocol.json
    TROPICAL_FISH, // todo сделать remap 1.13 mob-protocol.json
    ;

    private static List<EntityType> values = Arrays.asList(values());

    public static List<EntityType> getTypes() {
        return values;
    }
}
