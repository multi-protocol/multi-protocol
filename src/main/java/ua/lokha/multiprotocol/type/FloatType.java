package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class FloatType implements Type, MetadataType {
    private float value;

    @Override
    public void read(ByteBuf buf, Version version) {
        value = buf.readFloat();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeFloat(value);
    }
}
