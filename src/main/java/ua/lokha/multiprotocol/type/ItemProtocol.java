package ua.lokha.multiprotocol.type;

import lombok.extern.java.Log;
import ua.lokha.multiprotocol.Id2ObjectVersionMapper;

@Log
public class ItemProtocol {

    public static String AIR = "air";
    public static String STONE = "stone";
    public static Id2ObjectVersionMapper<String> ITEM_PROTOCOL;

    static {
        ITEM_PROTOCOL = Id2ObjectVersionMapper.loadFromResourceStringValue("item-protocol.json");
    }
}
