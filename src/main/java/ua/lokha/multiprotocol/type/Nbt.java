package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.type.nbt.Tag;

@Data
public class Nbt implements Type, MetadataType {
    private Tag tag;

    @Override
    public void read(ByteBuf buf, Version version) {
        tag = Tag.readNamedTag(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        Tag.writeNamedTag(tag, buf);
    }
}
