package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class OptBlockId implements Type, MetadataType {
    // todo сделать parse id << 4 | data
    private int data;

    @Override
    public void read(ByteBuf buf, Version version) {
        data = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(data, buf);
    }
}
