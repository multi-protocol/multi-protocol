package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Data
public class OptPosition extends Position implements Type, MetadataType {
    private boolean present;

    @Override
    public void read(ByteBuf buf, Version version) {
        present = buf.readBoolean();
        if (present) {
            super.read(buf, version);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeBoolean(present);
        if (present) {
            super.write(buf, version);
        }
    }
}
