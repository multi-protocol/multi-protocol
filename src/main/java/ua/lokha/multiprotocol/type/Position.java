package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class Position implements Type, MetadataType {
    private int x;
    private int y;
    private int z;

    public Position() {}

    public Position(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void read(ByteBuf buf, Version version) {
        long position = buf.readLong();
        x = getX(position, version);
        y = getY(position, version);
        z = getZ(position, version);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeLong(getPosition(x, y, z, version));
    }

    public static int getX(long position, Version version) {
        return (int) (position >> 38);
    }

    public static int getY(long position, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_14)) {
            return (int) (position << 52 >> 52);
        } else {
            return (int) ((position >> 26) & 0xfff);
        }
    }

    public static int getZ(long position, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_14)) {
            return (int) (position << 26 >> 38);
        } else {
            return (int) (position << 38 >> 38);
        }
    }

    public static long getPosition(int x, int y, int z, Version version) {
        if (version.isAfterOrEq(Version.MINECRAFT_1_14)) {
            return (((long)x & 0x3ffffff) << 38) | ((long)y & 0xfff) | (((long)z & 0x3ffffff) << 12);
        } else {
            return (((long)x & 0x3FFFFFF) << 38) | (((long)y & 0xFFF) << 26) | (z & 0x3FFFFFF);
        }
    }
}
