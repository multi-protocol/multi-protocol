package ua.lokha.multiprotocol.type;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class ShortType implements Type, MetadataType {
    private short value;

    @Override
    public void read(ByteBuf buf, Version version) {
        value = buf.readShort();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeShort(value);
    }
}
