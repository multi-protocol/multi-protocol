package ua.lokha.multiprotocol.type.chunk;

import lombok.Data;

import java.util.Arrays;

@Data
public class ChunkSection {
    private short bitsPerBlock;
    private int singleValMask;
    private int[] palette;
    private long[] blockData;
    private byte[] blockLight;
    private byte[] skyLight;

    public void setBitsPerBlock(short bitsPerBlock) {
        this.bitsPerBlock = bitsPerBlock;
        this.singleValMask = (1 << bitsPerBlock) - 1;
    }

    public int getBlockState(int x, int y, int z) {
        return this.getBlockState(index(x, y, z));
    }

    public int getBlockState(int blockIndex) {
        int index = getPaletteIndex(blockIndex);
        return palette == null ? index : palette[index];
    }

    public int getPaletteIndex(int blockIndex) {
        int bitStartIndex = blockIndex * bitsPerBlock;
        int arrStartIndex = bitStartIndex >> 6;
        int arrEndIndex = ((bitStartIndex + bitsPerBlock) - 1) >> 6;
        int localStartBitIndex = bitStartIndex & 63;
        if (arrStartIndex == arrEndIndex) {
            return (int) ((this.blockData[arrStartIndex] >>> localStartBitIndex) & this.singleValMask);
        } else {
            return (int) (((this.blockData[arrStartIndex] >>> localStartBitIndex) | (this.blockData[arrEndIndex] << (64 - localStartBitIndex))) & this.singleValMask);
        }
    }

    public int index(int x, int y, int z) {
        if (x < 0 || z < 0 || x >= 16 || z >= 16) {
            throw new IndexOutOfBoundsException(
                    "Coords (x=" + x + ",z=" + z + ") out of section bounds");
        }
        return (y & 0xf) << 8 | z << 4 | x;
    }

    @Override
    public String toString() {
        return "ChunkSection{" +
                "bitsPerBlock=" + bitsPerBlock +
                ", palette=" + Arrays.toString(palette) +
                ", blockData.length=" + blockData.length +
                ", blockLight.length=" + blockLight.length +
                ", skyLight.length=" + (skyLight == null ? null : skyLight.length) +
                '}';
    }
}
