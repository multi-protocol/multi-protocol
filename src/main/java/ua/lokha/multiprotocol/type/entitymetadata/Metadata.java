package ua.lokha.multiprotocol.type.entitymetadata;

import ua.lokha.multiprotocol.Type;
import ua.lokha.multiprotocol.Version;

/**
 * Метадата entity
 */
public interface Metadata extends Type {

    /**
     * Получить тип данных, используемый метадатой для версии
     */
    Class<? extends MetadataType> getTypeClass(Version version);
}
