package ua.lokha.multiprotocol.type.entitymetadata;

import lombok.ToString;
import ua.lokha.multiprotocol.Id2TypeVersionMapper;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.*;
import ua.lokha.multiprotocol.type.entitymetadata.metadata.*;

import java.util.EnumMap;
import java.util.Map;

@ToString(of = "displayName")
public class MetadataProtocol extends Id2TypeVersionMapper<Metadata> {

    public static Id2TypeVersionMapper<MetadataType> TYPE_PROTOCOL = new Id2TypeVersionMapper<>();
    static {
        TYPE_PROTOCOL.register(ByteType.class, ByteType::new,
                map(Version.MINECRAFT_1_8, 0));
        TYPE_PROTOCOL.register(ShortType.class, ShortType::new,
                map(Version.MINECRAFT_1_8, 1),
                map(Version.MINECRAFT_1_9, -1));
        TYPE_PROTOCOL.register(VarIntType.class, VarIntType::new,
                map(Version.MINECRAFT_1_9, 1));
        TYPE_PROTOCOL.register(IntType.class, IntType::new,
                map(Version.MINECRAFT_1_8, 2),
                map(Version.MINECRAFT_1_9, -1));
        TYPE_PROTOCOL.register(FloatType.class, FloatType::new,
                map(Version.MINECRAFT_1_8, 3),
                map(Version.MINECRAFT_1_9, 2));
        TYPE_PROTOCOL.register(StringType.class, StringType::new,
                map(Version.MINECRAFT_1_8, 4),
                map(Version.MINECRAFT_1_9, 3));
        TYPE_PROTOCOL.register(Chat.class, Chat::new,
                map(Version.MINECRAFT_1_9, 4));
        TYPE_PROTOCOL.register(OptChat.class, OptChat::new,
                map(Version.MINECRAFT_1_13, 5));
        TYPE_PROTOCOL.register(Slot.class, Slot::new,
                map(Version.MINECRAFT_1_8, 5),
                map(Version.MINECRAFT_1_13, 6));
        TYPE_PROTOCOL.register(BooleanType.class, BooleanType::new,
                map(Version.MINECRAFT_1_9, 6),
                map(Version.MINECRAFT_1_13, 7));
        TYPE_PROTOCOL.register(Rotation.class, Rotation::new,
                map(Version.MINECRAFT_1_8, 7),
                map(Version.MINECRAFT_1_13, 8));
        TYPE_PROTOCOL.register(Position.class, Position::new,
                map(Version.MINECRAFT_1_8, 6),
                map(Version.MINECRAFT_1_9, 8),
                map(Version.MINECRAFT_1_13, 9));
        TYPE_PROTOCOL.register(OptPosition.class, OptPosition::new,
                map(Version.MINECRAFT_1_9, 9),
                map(Version.MINECRAFT_1_13, 10));
        TYPE_PROTOCOL.register(Direction.class, Direction::new,
                map(Version.MINECRAFT_1_9, 10),
                map(Version.MINECRAFT_1_13, 11));
        TYPE_PROTOCOL.register(OptUuid.class, OptUuid::new,
                map(Version.MINECRAFT_1_9, 11),
                map(Version.MINECRAFT_1_13, 12));
        TYPE_PROTOCOL.register(OptBlockId.class, OptBlockId::new,
                map(Version.MINECRAFT_1_9, 12),
                map(Version.MINECRAFT_1_13, 13));
        TYPE_PROTOCOL.register(Nbt.class, Nbt::new,
                map(Version.MINECRAFT_1_9, 13),
                map(Version.MINECRAFT_1_13, 14));
    }

    //TODO: сделать метадату EntityPose
    public static MetadataProtocol ENTITY = new MetadataProtocol("ENTITY", null) {{
        this.register(EntityBaseFlags.class, EntityBaseFlags::new, map(Version.MINECRAFT_1_8, 0));
        this.register(EntityAir.class, EntityAir::new, map(Version.MINECRAFT_1_8, 1));
        this.register(EntityCustomName.class, EntityCustomName::new, map(Version.MINECRAFT_1_9, 2));
        this.register(EntityCustomNameVisible.class, EntityCustomNameVisible::new, map(Version.MINECRAFT_1_9, 3));
        this.register(EntitySilent.class, EntitySilent::new, map(Version.MINECRAFT_1_9, 4));
        this.register(EntityNoGravity.class, EntityNoGravity::new, map(Version.MINECRAFT_1_10, 5));
    }};
    public static MetadataProtocol PROJECTILE = ENTITY;
    public static MetadataProtocol FIREBALL = ENTITY;
    public static MetadataProtocol HANGING = ENTITY;
    //TODO: сделать метадату ArrowShooter, ArrowPiercingLevel
    public static MetadataProtocol ARROW = new MetadataProtocol("ARROW", ENTITY) {{
        this.register(ArrowBaseFlags.class, ArrowBaseFlags::new,
                map(Version.MINECRAFT_1_8, 15),
                map(Version.MINECRAFT_1_9, 5),
                map(Version.MINECRAFT_1_10, 6),
                map(Version.MINECRAFT_1_14, 7));
    }};
    public static MetadataProtocol LIVING = new MetadataProtocol("LIVING", ENTITY) {{
        this.register(LivingHandState.class, LivingHandState::new,
                map(Version.MINECRAFT_1_9, 5),
                map(Version.MINECRAFT_1_10, 6),
                map(Version.MINECRAFT_1_14, 7));
        this.register(LivingHealth.class, LivingHealth::new,
                map(Version.MINECRAFT_1_8, 6),
                map(Version.MINECRAFT_1_10, 7),
                map(Version.MINECRAFT_1_14, 8));
        /*this.register(LivingPotionColor.class, LivingPotionColor::new,
                map(Version.MINECRAFT_1_8, 7),
                map(Version.MINECRAFT_1_10, 8),
                map(Version.MINECRAFT_1_14, 9));*/
        this.register(LivingPotionAmbient.class, LivingPotionAmbient::new,
                map(Version.MINECRAFT_1_8, 8),
                map(Version.MINECRAFT_1_10, 9),
                map(Version.MINECRAFT_1_14, 10));
        /*this.register(LivingNumberArrows.class, LivingNumberArrows::new,
                map(Version.MINECRAFT_1_8, 9),
                map(Version.MINECRAFT_1_10, 10),
                map(Version.MINECRAFT_1_14, 11));*/
    }};
    public static MetadataProtocol INSENTIENT = new MetadataProtocol("INSENTIENT", LIVING) {{
        this.register(InsentientBaseFlags.class, InsentientBaseFlags::new,
                map(Version.MINECRAFT_1_8, 15),
                map(Version.MINECRAFT_1_9, 10),
                map(Version.MINECRAFT_1_10, 11),
                map(Version.MINECRAFT_1_14, 13),
                map(Version.MINECRAFT_1_15, 14));
    }};
    public static MetadataProtocol FLYING = INSENTIENT;
    public static MetadataProtocol CREATURE = INSENTIENT;
    public static MetadataProtocol GOLEM = CREATURE;
    public static MetadataProtocol AMBIENT = INSENTIENT;
    public static MetadataProtocol WATER_MOB = INSENTIENT;
    public static MetadataProtocol AGEABLE = new MetadataProtocol("AGEABLE", CREATURE) {{
        this.register(AgeableBaby.class, AgeableBaby::new,
                map(Version.MINECRAFT_1_8, 12),
                map(Version.MINECRAFT_1_9, 11),
                map(Version.MINECRAFT_1_10, 12),
                map(Version.MINECRAFT_1_14, 14),
                map(Version.MINECRAFT_1_15, 15));
    }};
    public static MetadataProtocol MONSTER = CREATURE;
    public static MetadataProtocol ZOMBIE = new MetadataProtocol(EntityType.ZOMBIE.name(), MONSTER) {{
        this.register(ZombieBaby.class, ZombieBaby::new,
                map(Version.MINECRAFT_1_8, 12),
                map(Version.MINECRAFT_1_9, 11),
                map(Version.MINECRAFT_1_10, 12),
                map(Version.MINECRAFT_1_14, 14),
                map(Version.MINECRAFT_1_15, 15));
        // todo сделать remap
            /*this.register(ZombieUnused.class, ZombieUnused::new,
                    map(Version.MINECRAFT_1_8, 13));*/
        this.register(ZombieHandsHeldUp.class, ZombieHandsHeldUp::new,
                map(Version.MINECRAFT_1_9, 14),
                map(Version.MINECRAFT_1_10, 15),
                map(Version.MINECRAFT_1_11, 14),
                map(Version.MINECRAFT_1_14, -1));
    }};
    public static MetadataProtocol ANIMAL = AGEABLE;
    public static MetadataProtocol TAMEABLE_ANIMAL = new MetadataProtocol("TAMEABLE_ANIMAL", ANIMAL) {{
        this.register(TameableAnimalBaseFlags.class, TameableAnimalBaseFlags::new,
                map(Version.MINECRAFT_1_8, 16),
                map(Version.MINECRAFT_1_9, 12),
                map(Version.MINECRAFT_1_10, 13),
                map(Version.MINECRAFT_1_14, 15),
                map(Version.MINECRAFT_1_15, 16));
        // todo сделать remap
        /*this.register(TameableAnimalOwner.class, TameableAnimalOwner::new,
                map(Version.MINECRAFT_1_8, 16),
                map(Version.MINECRAFT_1_9, 12),
                map(Version.MINECRAFT_1_10, 13),
                map(Version.MINECRAFT_1_14, 15));*/
    }};
    public static MetadataProtocol ABSTRACT_HORSE = new MetadataProtocol("ABSTRACT_HORSE", ANIMAL) {{
        this.register(AbstractHorseBaseFlags.class, AbstractHorseBaseFlags::new,
                map(Version.MINECRAFT_1_8, 16),
                map(Version.MINECRAFT_1_9, 12),
                map(Version.MINECRAFT_1_10, 13),
                map(Version.MINECRAFT_1_14, 15),
                map(Version.MINECRAFT_1_15, 16));
        this.register(AbstractHorseOwner.class, AbstractHorseOwner::new,
                map(Version.MINECRAFT_1_14, 16), // todo тут не до конца remap
                map(Version.MINECRAFT_1_15, 17));
    }};
    public static MetadataProtocol CHESTED_HORSE = new MetadataProtocol("CHESTED_HORSE", ABSTRACT_HORSE) {{
        this.register(ChestedHorseHasChest.class, ChestedHorseHasChest::new,
                map(Version.MINECRAFT_1_11, 15),
                map(Version.MINECRAFT_1_14, 17),
                map(Version.MINECRAFT_1_15, 18));
    }};
    public static MetadataProtocol PLAYER = new MetadataProtocol("PLAYER", LIVING) {{
        this.register(PlayerAdditionalHearts.class, PlayerAdditionalHearts::new,
                map(Version.MINECRAFT_1_8, 17),
                map(Version.MINECRAFT_1_9, 10),
                map(Version.MINECRAFT_1_10, 11),
                map(Version.MINECRAFT_1_14, 13),
                map(Version.MINECRAFT_1_15, 14));
        this.register(PlayerScore.class, PlayerScore::new,
                map(Version.MINECRAFT_1_8, 18),
                map(Version.MINECRAFT_1_9, 11),
                map(Version.MINECRAFT_1_10, 12),
                map(Version.MINECRAFT_1_14, 14),
                map(Version.MINECRAFT_1_15, 15));
        this.register(PlayerSkinParts.class, PlayerSkinParts::new,
                map(Version.MINECRAFT_1_8, 10),
                map(Version.MINECRAFT_1_9, 12),
                map(Version.MINECRAFT_1_10, 13),
                map(Version.MINECRAFT_1_14, 15),
                map(Version.MINECRAFT_1_15, 16));
        this.register(PlayerMainHand.class, PlayerMainHand::new,
                map(Version.MINECRAFT_1_9, 13),
                map(Version.MINECRAFT_1_10, 14),
                map(Version.MINECRAFT_1_14, 16),
                map(Version.MINECRAFT_1_15, 17));
        this.register(PlayerLeftShoulder.class, PlayerLeftShoulder::new,
                map(Version.MINECRAFT_1_12, 15),
                map(Version.MINECRAFT_1_14, 17),
                map(Version.MINECRAFT_1_15, 18));
        this.register(PlayerRightShoulder.class, PlayerRightShoulder::new,
                map(Version.MINECRAFT_1_12, 16),
                map(Version.MINECRAFT_1_14, 18),
                map(Version.MINECRAFT_1_15, 19));
    }};
    private static MetadataProtocol GUARDIAN = new MetadataProtocol(EntityType.GUARDIAN.name(), MONSTER){{
        this.register(GuardianRetractingSpikes.class, GuardianRetractingSpikes::new,
                map(Version.MINECRAFT_1_8, 16),
                map(Version.MINECRAFT_1_9, 11),
                map(Version.MINECRAFT_1_10, 12),
                map(Version.MINECRAFT_1_14, 14),
                map(Version.MINECRAFT_1_15, 15));
        this.register(GuardianTarget.class, GuardianTarget::new,
                map(Version.MINECRAFT_1_8, 17),
                map(Version.MINECRAFT_1_9, 12),
                map(Version.MINECRAFT_1_10, 13),
                map(Version.MINECRAFT_1_14, 15),
                map(Version.MINECRAFT_1_15, 16));
    }};
    private static MetadataProtocol ABSTRACT_ILLAGER = new MetadataProtocol("ABSTRACT_ILLAGER", MONSTER){{
        // todo сделать remap
        // this.register(AbstractIllagerBaseFlags.class, AbstractIllagerBaseFlags::new);
    }};
    private static MetadataProtocol ABSTRACT_SKELETON = new MetadataProtocol("ABSTRACT_SKELETON", MONSTER){{
        this.register(AbstractSkeletonSwingingArms.class, AbstractSkeletonSwingingArms::new,
                map(Version.MINECRAFT_1_9, 12),
                map(Version.MINECRAFT_1_10, 13),
                map(Version.MINECRAFT_1_11, 12));
                map(Version.MINECRAFT_1_14, -1);
    }};
    private static MetadataProtocol MINECART = new MetadataProtocol("MINECART", ENTITY){{
        this.register(MinecartShakingPower.class, MinecartShakingPower::new,
                map(Version.MINECRAFT_1_8, 17),
                map(Version.MINECRAFT_1_9, 5),
                map(Version.MINECRAFT_1_10, 6),
                map(Version.MINECRAFT_1_14, 7));
        this.register(MinecartShakingDirection.class, MinecartShakingDirection::new,
                map(Version.MINECRAFT_1_8, 18),
                map(Version.MINECRAFT_1_9, 6),
                map(Version.MINECRAFT_1_10, 7),
                map(Version.MINECRAFT_1_14, 8));
        this.register(MinecartShakingMultiplier.class, MinecartShakingMultiplier::new,
                map(Version.MINECRAFT_1_8, 19),
                map(Version.MINECRAFT_1_9, 7),
                map(Version.MINECRAFT_1_10, 8),
                map(Version.MINECRAFT_1_14, 9));
        this.register(MinecartCustomBlockType.class, MinecartCustomBlockType::new,
                map(Version.MINECRAFT_1_8, 20),
                map(Version.MINECRAFT_1_9, 8),
                map(Version.MINECRAFT_1_10, 9),
                map(Version.MINECRAFT_1_14, 10));
        this.register(MinecartCustomBlockY.class, MinecartCustomBlockY::new,
                map(Version.MINECRAFT_1_8, 21),
                map(Version.MINECRAFT_1_9, 9),
                map(Version.MINECRAFT_1_10, 10),
                map(Version.MINECRAFT_1_14, 11));
        this.register(MinecartShowCustomBlock.class, MinecartShowCustomBlock::new,
                map(Version.MINECRAFT_1_8, 22),
                map(Version.MINECRAFT_1_9, 10),
                map(Version.MINECRAFT_1_10, 11),
                map(Version.MINECRAFT_1_14, 12));
    }};
    private static MetadataProtocol MINECART_CONTAINER = MINECART;

    public static Map<EntityType, MetadataProtocol> BY_ENTITY = new EnumMap<>(EntityType.class);
    static {
        BY_ENTITY.put(EntityType.PIG, new MetadataProtocol(EntityType.PIG.name(), ANIMAL) {{
            this.register(PigSaddle.class, PigSaddle::new,
                    map(Version.MINECRAFT_1_8, 16),
                    map(Version.MINECRAFT_1_9, 12),
                    map(Version.MINECRAFT_1_10, 13),
                    map(Version.MINECRAFT_1_14, 15),
                    map(Version.MINECRAFT_1_15, 16));
            this.register(PigTimeBoost.class, PigTimeBoost::new,
                    map(Version.MINECRAFT_1_11_1, 14),
                    map(Version.MINECRAFT_1_14, 16),
                    map(Version.MINECRAFT_1_15, 17));
        }});
        BY_ENTITY.put(EntityType.CREEPER, new MetadataProtocol(EntityType.CREEPER.name(), MONSTER) {{
            this.register(CreeperState.class, CreeperState::new,
                    map(Version.MINECRAFT_1_8, 16),
                    map(Version.MINECRAFT_1_9, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, 14),
                    map(Version.MINECRAFT_1_15, 15));
            this.register(CreeperCharged.class, CreeperCharged::new,
                    map(Version.MINECRAFT_1_8, 17),
                    map(Version.MINECRAFT_1_9, 12),
                    map(Version.MINECRAFT_1_10, 13),
                    map(Version.MINECRAFT_1_14, 15),
                    map(Version.MINECRAFT_1_15, 16));
            this.register(CreeperIgnited.class, CreeperIgnited::new,
                    map(Version.MINECRAFT_1_8, 18),
                    map(Version.MINECRAFT_1_9, 13),
                    map(Version.MINECRAFT_1_10, 14),
                    map(Version.MINECRAFT_1_14, 16),
                    map(Version.MINECRAFT_1_15, 17));
        }});
        BY_ENTITY.put(EntityType.ZOMBIE, ZOMBIE);
        BY_ENTITY.put(EntityType.SPIDER, new MetadataProtocol(EntityType.SPIDER.name(), MONSTER) {{
            this.register(SpiderBaseFlags.class, SpiderBaseFlags::new,
                    map(Version.MINECRAFT_1_8, 16),
                    map(Version.MINECRAFT_1_9, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, 14),
                    map(Version.MINECRAFT_1_15, 15));
        }});
        BY_ENTITY.put(EntityType.BAT, new MetadataProtocol(EntityType.BAT.name(), AMBIENT) {{
            this.register(BatBaseFlags.class, BatBaseFlags::new,
                    map(Version.MINECRAFT_1_8, 16),
                    map(Version.MINECRAFT_1_9, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, 14),
                    map(Version.MINECRAFT_1_15, 15));
        }});
        BY_ENTITY.put(EntityType.ARMOR_STAND, new MetadataProtocol(EntityType.ARMOR_STAND.name(), LIVING) {{
            this.register(ArmorStandBaseFlags.class, ArmorStandBaseFlags::new,
                    map(Version.MINECRAFT_1_8, 10),
                    map(Version.MINECRAFT_1_10, 11),
                    map(Version.MINECRAFT_1_14, 13),
                    map(Version.MINECRAFT_1_15, 14));
            this.register(ArmorStandHeadRotation.class, ArmorStandHeadRotation::new,
                    map(Version.MINECRAFT_1_8, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, 14),
                    map(Version.MINECRAFT_1_15, 15));
            this.register(ArmorStandBodyRotation.class, ArmorStandBodyRotation::new,
                    map(Version.MINECRAFT_1_8, 12),
                    map(Version.MINECRAFT_1_10, 13),
                    map(Version.MINECRAFT_1_14, 15),
                    map(Version.MINECRAFT_1_15, 16));
            this.register(ArmorStandLeftArmRotation.class, ArmorStandLeftArmRotation::new,
                    map(Version.MINECRAFT_1_8, 13),
                    map(Version.MINECRAFT_1_10, 14),
                    map(Version.MINECRAFT_1_14, 16),
                    map(Version.MINECRAFT_1_15, 17));
            this.register(ArmorStandRightArmRotation.class, ArmorStandRightArmRotation::new,
                    map(Version.MINECRAFT_1_8, 14),
                    map(Version.MINECRAFT_1_10, 15),
                    map(Version.MINECRAFT_1_14, 17),
                    map(Version.MINECRAFT_1_15, 18));
            this.register(ArmorStandLeftLegRotation.class, ArmorStandLeftLegRotation::new,
                    map(Version.MINECRAFT_1_8, 15),
                    map(Version.MINECRAFT_1_10, 16),
                    map(Version.MINECRAFT_1_14, 18),
                    map(Version.MINECRAFT_1_15, 19));
            this.register(ArmorStandRightLegRotation.class, ArmorStandRightLegRotation::new,
                    map(Version.MINECRAFT_1_8, 16),
                    map(Version.MINECRAFT_1_10, 17),
                    map(Version.MINECRAFT_1_14, 19),
                    map(Version.MINECRAFT_1_15, 20));
        }});
        BY_ENTITY.put(EntityType.SNOWBALL, PROJECTILE);
        BY_ENTITY.put(EntityType.THROWN_EGG, PROJECTILE);
        BY_ENTITY.put(EntityType.THROWN_POTION, new MetadataProtocol(EntityType.THROWN_POTION.name(), PROJECTILE){{
            this.register(PotionSlot.class, PotionSlot::new,
                    map(Version.MINECRAFT_1_9, 5),
                    map(Version.MINECRAFT_1_10, 6),
                    map(Version.MINECRAFT_1_14, 7));
        }});
        // todo сделать remap https://wiki.vg/index.php?title=Entity_metadata&oldid=14048#FallingBlock
        BY_ENTITY.put(EntityType.FALLING_BLOCK, ENTITY);
        BY_ENTITY.put(EntityType.ARROW, ARROW);
        //TODO: добавить метадату SplashTimer
        BY_ENTITY.put(EntityType.BOAT, new MetadataProtocol(EntityType.BOAT.name(), ENTITY){{
            this.register(BoatTimeLastHit.class, BoatTimeLastHit::new,
                    map(Version.MINECRAFT_1_8, 17),
                    map(Version.MINECRAFT_1_9, 5),
                    map(Version.MINECRAFT_1_10, 6),
                    map(Version.MINECRAFT_1_14, 7));
            this.register(BoatForwardDirection.class, BoatForwardDirection::new,
                    map(Version.MINECRAFT_1_8, 18),
                    map(Version.MINECRAFT_1_9, 6),
                    map(Version.MINECRAFT_1_10, 7),
                    map(Version.MINECRAFT_1_14, 8));
            this.register(BoatDamageTaken.class, BoatDamageTaken::new,
                    map(Version.MINECRAFT_1_8, 19),
                    map(Version.MINECRAFT_1_9, 7),
                    map(Version.MINECRAFT_1_10, 8),
                    map(Version.MINECRAFT_1_14, 9));
            this.register(BoatType.class, BoatType::new,
                    map(Version.MINECRAFT_1_10, 9),
                    map(Version.MINECRAFT_1_14, 10));
            this.register(BoatLeftPaddleTurning.class, BoatLeftPaddleTurning::new,
                    map(Version.MINECRAFT_1_10, 10),
                    map(Version.MINECRAFT_1_14, 11));
            this.register(BoatRightPaddleTurning.class, BoatRightPaddleTurning::new,
                    map(Version.MINECRAFT_1_10, 11),
                    map(Version.MINECRAFT_1_14, 12));
        }});
        BY_ENTITY.put(EntityType.ENDER_CRYSTAL, new MetadataProtocol(EntityType.ENDER_CRYSTAL.name(), ENTITY){{
            this.register(EnderCrystalBeamTarget.class, EnderCrystalBeamTarget::new,
                    map(Version.MINECRAFT_1_9, 5),
                    map(Version.MINECRAFT_1_10, 6),
                    map(Version.MINECRAFT_1_14, 7));
            this.register(EnderCrystalShowBottom.class, EnderCrystalShowBottom::new,
                    map(Version.MINECRAFT_1_9, 6),
                    map(Version.MINECRAFT_1_10, 7),
                    map(Version.MINECRAFT_1_14, 8));
        }});
        BY_ENTITY.put(EntityType.FIREBALL, FIREBALL);
        BY_ENTITY.put(EntityType.WITHER_SKULL, new MetadataProtocol(EntityType.WITHER_SKULL.name(), FIREBALL) {{
            this.register(WitherSkullInvulnerable.class, WitherSkullInvulnerable::new,
                    map(Version.MINECRAFT_1_8, 10),
                    map(Version.MINECRAFT_1_9, 5),
                    map(Version.MINECRAFT_1_10, 6),
                    map(Version.MINECRAFT_1_14, 7));
        }});
        BY_ENTITY.put(EntityType.FIREWORK_ROCKET, new MetadataProtocol(EntityType.FIREWORK_ROCKET.name(), ENTITY){{
            this.register(FireworksSlot.class, FireworksSlot::new,
                    map(Version.MINECRAFT_1_8, 8),
                    map(Version.MINECRAFT_1_9, 5),
                    map(Version.MINECRAFT_1_10, 6),
                    map(Version.MINECRAFT_1_14, 7));
            this.register(FireworksShooter.class, FireworksShooter::new,
                    map(Version.MINECRAFT_1_11_1, 7),
                    map(Version.MINECRAFT_1_14, 8));
        }});
        BY_ENTITY.put(EntityType.ITEM_FRAME, new MetadataProtocol(EntityType.ITEM_FRAME.name(), HANGING){{
            this.register(ItemFrameSlot.class, ItemFrameSlot::new,
                    map(Version.MINECRAFT_1_8, 8),
                    map(Version.MINECRAFT_1_9, 5),
                    map(Version.MINECRAFT_1_10, 6),
                    map(Version.MINECRAFT_1_14, 7));
            this.register(ItemFrameRotation.class, ItemFrameRotation::new,
                    map(Version.MINECRAFT_1_8, 9),
                    map(Version.MINECRAFT_1_9, 6),
                    map(Version.MINECRAFT_1_10, 7),
                    map(Version.MINECRAFT_1_14, 8));
        }});
        BY_ENTITY.put(EntityType.ITEM, new MetadataProtocol(EntityType.ITEM.name(), ENTITY){{
            this.register(ItemSlot.class, ItemSlot::new,
                    map(Version.MINECRAFT_1_8, 10),
                    map(Version.MINECRAFT_1_9, 5),
                    map(Version.MINECRAFT_1_10, 6),
                    map(Version.MINECRAFT_1_14, 7));
        }});
        BY_ENTITY.put(EntityType.SQUID, WATER_MOB);
        BY_ENTITY.put(EntityType.HORSE, new MetadataProtocol(EntityType.HORSE.name(), ABSTRACT_HORSE){{
            this.register(HorseType.class, HorseType::new,
                    map(Version.MINECRAFT_1_8, 20),
                    map(Version.MINECRAFT_1_9, 14),
                    map(Version.MINECRAFT_1_10, 15),
                    map(Version.MINECRAFT_1_14, 17),
                    map(Version.MINECRAFT_1_15, 18));
            this.register(HorseArmor.class, HorseArmor::new,
                    map(Version.MINECRAFT_1_8, 22),
                    map(Version.MINECRAFT_1_9, 16),
                    map(Version.MINECRAFT_1_10, 17),
                    map(Version.MINECRAFT_1_11, 16),
                    map(Version.MINECRAFT_1_14, 18),
                    map(Version.MINECRAFT_1_15, 19));
        }});
        BY_ENTITY.put(EntityType.ZOMBIE_HORSE, ABSTRACT_HORSE);
        BY_ENTITY.put(EntityType.SKELETON_HORSE, ABSTRACT_HORSE);
        BY_ENTITY.put(EntityType.DONKEY, CHESTED_HORSE);
        BY_ENTITY.put(EntityType.MULE, CHESTED_HORSE);
        BY_ENTITY.put(EntityType.LLAMA, new MetadataProtocol(EntityType.LLAMA.name(), CHESTED_HORSE){{
            this.register(LlamaStrength.class, LlamaStrength::new,
                    map(Version.MINECRAFT_1_11, 16),
                    map(Version.MINECRAFT_1_14, 18),
                    map(Version.MINECRAFT_1_15, 19));
            this.register(LlamaCarpetColor.class, LlamaCarpetColor::new,
                    map(Version.MINECRAFT_1_11, 17),
                    map(Version.MINECRAFT_1_14, 19),
                    map(Version.MINECRAFT_1_15, 20));
            this.register(LlamaType.class, LlamaType::new,
                    map(Version.MINECRAFT_1_11, 18),
                    map(Version.MINECRAFT_1_14, 20),
                    map(Version.MINECRAFT_1_15, 21));
        }});
        BY_ENTITY.put(EntityType.RABBIT, new MetadataProtocol(EntityType.RABBIT.name(), ANIMAL){{
            this.register(RabbitType.class, RabbitType::new,
                    map(Version.MINECRAFT_1_8, 18),
                    map(Version.MINECRAFT_1_9, 12),
                    map(Version.MINECRAFT_1_10, 13),
                    map(Version.MINECRAFT_1_14, 15),
                    map(Version.MINECRAFT_1_15, 16));
        }});
        BY_ENTITY.put(EntityType.PLAYER, PLAYER);
        BY_ENTITY.put(EntityType.SLIME, new MetadataProtocol(EntityType.SLIME.name(), INSENTIENT){{
            /*this.register(SlimeSize.class, SlimeSize::new,
                    map(Version.MINECRAFT_1_8, 16),
                    map(Version.MINECRAFT_1_9, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, 14),
                    map(Version.MINECRAFT_1_15, 15));*/
        }});
        BY_ENTITY.put(EntityType.POLAR_BEAR, new MetadataProtocol(EntityType.POLAR_BEAR.name(), ANIMAL){{
            this.register(PolarBearStandingUp.class, PolarBearStandingUp::new,
                    map(Version.MINECRAFT_1_10, 13),
                    map(Version.MINECRAFT_1_14, 15),
                    map(Version.MINECRAFT_1_15, 16));
        }});
        BY_ENTITY.put(EntityType.SHEEP, new MetadataProtocol(EntityType.SHEEP.name(), ANIMAL){{
            this.register(SheepBaseFlags.class, SheepBaseFlags::new,
                    map(Version.MINECRAFT_1_8, 16),
                    map(Version.MINECRAFT_1_9, 12),
                    map(Version.MINECRAFT_1_10, 13),
                    map(Version.MINECRAFT_1_14, 15),
                    map(Version.MINECRAFT_1_15, 16));
        }});
        BY_ENTITY.put(EntityType.OCELOT, new MetadataProtocol(EntityType.OCELOT.name(), TAMEABLE_ANIMAL){{
            //todo сделать remap
            // this.register(OcelotType.class, OcelotType::new);
        }});
        BY_ENTITY.put(EntityType.WOLF, new MetadataProtocol(EntityType.WOLF.name(), TAMEABLE_ANIMAL){{
            // todo сделать remap
            // this.register(WolfDamageTaken.class, WolfDamageTaken::new);
            this.register(WolfBegging.class, WolfBegging::new,
                    map(Version.MINECRAFT_1_8, 19),
                    map(Version.MINECRAFT_1_9, 15),
                    map(Version.MINECRAFT_1_10, 16),
                    map(Version.MINECRAFT_1_14, 18));
            this.register(WolfCollarColor.class, WolfCollarColor::new,
                    map(Version.MINECRAFT_1_8, 20),
                    map(Version.MINECRAFT_1_9, 16),
                    map(Version.MINECRAFT_1_10, 17),
                    map(Version.MINECRAFT_1_14, 19));
        }});
        BY_ENTITY.put(EntityType.PARROT, new MetadataProtocol(EntityType.PARROT.name(), TAMEABLE_ANIMAL){{
            this.register(ParrotType.class, ParrotType::new,
                    map(Version.MINECRAFT_1_12, 15),
                    map(Version.MINECRAFT_1_14, 17),
                    map(Version.MINECRAFT_1_15, 18));
        }});
        BY_ENTITY.put(EntityType.VILLAGER, new MetadataProtocol(EntityType.VILLAGER.name(), AGEABLE){{
            this.register(VillagerProfession.class, VillagerProfession::new,
                    map(Version.MINECRAFT_1_8, 16),
                    map(Version.MINECRAFT_1_9, 12),
                    map(Version.MINECRAFT_1_10, 13),
                    map(Version.MINECRAFT_1_14, 15),
                    map(Version.MINECRAFT_1_14_1, 16),
                    map(Version.MINECRAFT_1_15, 17));
        }});
        BY_ENTITY.put(EntityType.IRON_GOLEM, new MetadataProtocol(EntityType.IRON_GOLEM.name(), GOLEM){{
            this.register(IronGolemBaseFlags.class, IronGolemBaseFlags::new,
                    map(Version.MINECRAFT_1_8, 16),
                    map(Version.MINECRAFT_1_9, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, 14),
                    map(Version.MINECRAFT_1_15, 15));
        }});
        BY_ENTITY.put(EntityType.SNOWMAN, new MetadataProtocol(EntityType.SNOWMAN.name(), GOLEM){{
            this.register(SnowmanBaseFlags.class, SnowmanBaseFlags::new,
                    map(Version.MINECRAFT_1_9, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, 14),
                    map(Version.MINECRAFT_1_15, 15));
        }});
        BY_ENTITY.put(EntityType.SHULKER, new MetadataProtocol(EntityType.SHULKER.name(), GOLEM){{
            this.register(ShulkerFacingDirection.class, ShulkerFacingDirection::new,
                    map(Version.MINECRAFT_1_9, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, 14),
                    map(Version.MINECRAFT_1_15, 15));
            this.register(ShulkerAttachmentPosition.class, ShulkerAttachmentPosition::new,
                    map(Version.MINECRAFT_1_9, 12),
                    map(Version.MINECRAFT_1_10, 13),
                    map(Version.MINECRAFT_1_14, 15),
                    map(Version.MINECRAFT_1_15, 16));
            this.register(ShulkerShieldHeight.class, ShulkerShieldHeight::new,
                    map(Version.MINECRAFT_1_9, 13),
                    map(Version.MINECRAFT_1_10, 14),
                    map(Version.MINECRAFT_1_14, 16),
                    map(Version.MINECRAFT_1_15, 17));
            this.register(ShulkerColor.class, ShulkerColor::new,
                    map(Version.MINECRAFT_1_11, 15),
                    map(Version.MINECRAFT_1_14, 17),
                    map(Version.MINECRAFT_1_15, 18));
        }});
        BY_ENTITY.put(EntityType.BLAZE, new MetadataProtocol(EntityType.BLAZE.name(), MONSTER){{
            this.register(BlazeBaseFlags.class, BlazeBaseFlags::new,
                    map(Version.MINECRAFT_1_8, 16),
                    map(Version.MINECRAFT_1_9, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, 14),
                    map(Version.MINECRAFT_1_15, 15));
        }});
        BY_ENTITY.put(EntityType.ENDERMITE, MONSTER);
        BY_ENTITY.put(EntityType.GIANT, MONSTER);
        BY_ENTITY.put(EntityType.GUARDIAN, GUARDIAN);
        BY_ENTITY.put(EntityType.ELDER_GUARDIAN, GUARDIAN);
        BY_ENTITY.put(EntityType.SILVERFISH, MONSTER);
        BY_ENTITY.put(EntityType.VINDICATION_ILLAGER, ABSTRACT_ILLAGER);
        BY_ENTITY.put(EntityType.EVOCATION_ILLAGER, ABSTRACT_ILLAGER);
        BY_ENTITY.put(EntityType.ILLUSION_ILLAGER, ABSTRACT_ILLAGER);
        // todo вроде еще есть Spellcaster Illager https://wiki.vg/index.php?title=Entity_metadata&oldid=14048#Spellcaster_Illager
        BY_ENTITY.put(EntityType.VEX, new MetadataProtocol(EntityType.VEX.name(), MONSTER){{
            this.register(VexBaseFlags.class, VexBaseFlags::new,
                    map(Version.MINECRAFT_1_11, 12),
                    map(Version.MINECRAFT_1_14, 14),
                    map(Version.MINECRAFT_1_15, 15));
        }});
        BY_ENTITY.put(EntityType.EVOCATION_FANGS, ENTITY);
        BY_ENTITY.put(EntityType.SKELETON, ABSTRACT_SKELETON);
        BY_ENTITY.put(EntityType.WITHER_SKELETON, ABSTRACT_SKELETON);
        BY_ENTITY.put(EntityType.STRAY, ABSTRACT_SKELETON);
        BY_ENTITY.put(EntityType.WITCH, new MetadataProtocol(EntityType.WITCH.name(), MONSTER){{
            this.register(WitchDrinkingPotion.class, WitchDrinkingPotion::new,
                    map(Version.MINECRAFT_1_8, 16),
                    map(Version.MINECRAFT_1_9, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, 14),
                    map(Version.MINECRAFT_1_15, 15));
        }});
        BY_ENTITY.put(EntityType.WITHER, new MetadataProtocol(EntityType.WITHER.name(), MONSTER){{
            this.register(WitherCenterHeadTarget.class, WitherCenterHeadTarget::new,
                    map(Version.MINECRAFT_1_8, 17),
                    map(Version.MINECRAFT_1_9, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, 14),
                    map(Version.MINECRAFT_1_15, 15));
            this.register(WitherLeftHeadTarget.class, WitherLeftHeadTarget::new,
                    map(Version.MINECRAFT_1_8, 18),
                    map(Version.MINECRAFT_1_9, 12),
                    map(Version.MINECRAFT_1_10, 13),
                    map(Version.MINECRAFT_1_14, 15),
                    map(Version.MINECRAFT_1_15, 16));
            this.register(WitherRightHeadTarget.class, WitherRightHeadTarget::new,
                    map(Version.MINECRAFT_1_8, 19),
                    map(Version.MINECRAFT_1_9, 13),
                    map(Version.MINECRAFT_1_10, 14),
                    map(Version.MINECRAFT_1_14, 16),
                    map(Version.MINECRAFT_1_15, 17));
            this.register(WitherInvulnerableTime.class, WitherInvulnerableTime::new,
                    map(Version.MINECRAFT_1_8, 20),
                    map(Version.MINECRAFT_1_9, 14),
                    map(Version.MINECRAFT_1_10, 15),
                    map(Version.MINECRAFT_1_14, 17),
                    map(Version.MINECRAFT_1_15, 18));
        }});
        BY_ENTITY.put(EntityType.ZOMBIE_VILLAGER, new MetadataProtocol(EntityType.ZOMBIE_VILLAGER.name(), ZOMBIE){{
            this.register(ZombieVillagerConverting.class, ZombieVillagerConverting::new,
                    map(Version.MINECRAFT_1_8, 14),
                    map(Version.MINECRAFT_1_9, 13),
                    map(Version.MINECRAFT_1_10, 14),
                    map(Version.MINECRAFT_1_11, 15),
                    map(Version.MINECRAFT_1_14, 17),
                    map(Version.MINECRAFT_1_15, 18));
            this.register(ZombieVillagerProfession.class, ZombieVillagerProfession::new,
                    map(Version.MINECRAFT_1_11, 16),
                    map(Version.MINECRAFT_1_14, 18),
                    map(Version.MINECRAFT_1_15, 19));
        }});
        BY_ENTITY.put(EntityType.HUSK, ZOMBIE);

        //TODO: добавить метадату StaredAt
        BY_ENTITY.put(EntityType.ENDERMAN, new MetadataProtocol(EntityType.ENDERMAN.name(), MONSTER){{
            this.register(EndermanCarriedBlock.class, EndermanCarriedBlock::new,
                    map(Version.MINECRAFT_1_8, 16),
                    map(Version.MINECRAFT_1_9, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, 14),
                    map(Version.MINECRAFT_1_15, 15));
            this.register(EndermanScreaming.class, EndermanScreaming::new,
                    map(Version.MINECRAFT_1_8, 18),
                    map(Version.MINECRAFT_1_9, 12),
                    map(Version.MINECRAFT_1_10, 13),
                    map(Version.MINECRAFT_1_14, 15),
                    map(Version.MINECRAFT_1_15, 16));
        }});
        BY_ENTITY.put(EntityType.ENDER_DRAGON, new MetadataProtocol(EntityType.ENDER_DRAGON.name(), INSENTIENT){{
            this.register(EnderDragonDragonPhase.class, EnderDragonDragonPhase::new,
                    map(Version.MINECRAFT_1_9, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, 14),
                    map(Version.MINECRAFT_1_15, 15));
        }});
        BY_ENTITY.put(EntityType.GHAST, new MetadataProtocol(EntityType.GHAST.name(), FLYING){{
            this.register(GhastAttacking.class, GhastAttacking::new,
                    map(Version.MINECRAFT_1_8, 16),
                    map(Version.MINECRAFT_1_9, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, 14),
                    map(Version.MINECRAFT_1_15, 15));
        }});
        BY_ENTITY.put(EntityType.LLAMA_SPIT, ENTITY);
        BY_ENTITY.put(EntityType.MINECART_RIDEABLE, MINECART);
        BY_ENTITY.put(EntityType.MINECART_HOPPER, MINECART_CONTAINER);
        BY_ENTITY.put(EntityType.MINECART_CHEST, MINECART_CONTAINER);
        BY_ENTITY.put(EntityType.MINECART_FURNACE, new MetadataProtocol(EntityType.MINECART_FURNACE.name(), MINECART){{
            this.register(MinecartFurnacePowered.class, MinecartFurnacePowered::new,
                    map(Version.MINECRAFT_1_8, 16),
                    map(Version.MINECRAFT_1_9, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, -1));
        }});
        BY_ENTITY.put(EntityType.MINECART_TNT, MINECART);
        BY_ENTITY.put(EntityType.MINECART_SPAWNER, MINECART);
        BY_ENTITY.put(EntityType.MINECART_COMMAND_BLOCK, new MetadataProtocol(EntityType.MINECART_COMMAND_BLOCK.name(), MINECART){{
            this.register(MinecartCommandBlockCommand.class, MinecartCommandBlockCommand::new,
                    map(Version.MINECRAFT_1_8, 23),
                    map(Version.MINECRAFT_1_9, 11),
                    map(Version.MINECRAFT_1_10, 12),
                    map(Version.MINECRAFT_1_14, 13));
            this.register(MinecartCommandBlockLastOutput.class, MinecartCommandBlockLastOutput::new,
                    map(Version.MINECRAFT_1_8, 24),
                    map(Version.MINECRAFT_1_9, 12),
                    map(Version.MINECRAFT_1_10, 13),
                    map(Version.MINECRAFT_1_14, 14));
        }});
        BY_ENTITY.put(EntityType.PRIMED_TNT, new MetadataProtocol(EntityType.PRIMED_TNT.name(), ENTITY){{
            this.register(PrimedTntFuseTime.class, PrimedTntFuseTime::new,
                    map(Version.MINECRAFT_1_9, 5),
                    map(Version.MINECRAFT_1_10, 6),
                    map(Version.MINECRAFT_1_14, 7));
        }});
    }

    public static MetadataProtocol getByEntity(EntityType mobType) {
        MetadataProtocol metadataProtocol = BY_ENTITY.get(mobType);
        if (metadataProtocol == null) {
            throw new IllegalStateException("protocol by mob " + mobType + " not found");
        }
        return metadataProtocol;
    }

    private String displayName;
    
    MetadataProtocol(String displayName, MetadataProtocol include) {
        this.displayName = displayName;
        for (Version version : Version.getVersions()) {
            this.getVersionMapper().put(version, new VersionMapper<>(version));
        }
        if (include != null) {
            include.getVersionMapper().forEach((version, includeMapper) -> {
                VersionMapper<Metadata> versionMapper = this.getVersionMapper().get(version);
                includeMapper.getTypeById().forEach((index, metadataInfo) -> {
                    versionMapper.register(metadataInfo.getTypeClass(), metadataInfo.getCreator(), index);
                });
            });
        }
    }
}
