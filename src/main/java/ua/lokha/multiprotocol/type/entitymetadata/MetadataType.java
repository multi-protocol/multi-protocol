package ua.lokha.multiprotocol.type.entitymetadata;

import ua.lokha.multiprotocol.Type;

/**
 * Тип метадаты entity
 */
public interface MetadataType extends Type {
}

