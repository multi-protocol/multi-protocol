package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.ByteType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class BlazeBaseFlags implements Metadata {
    private byte value;

    @Override
    public void read(ByteBuf buf, Version version) {
        value = buf.readByte();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeByte(value);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        return ByteType.class;
    }
}
