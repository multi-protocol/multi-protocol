package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.type.BooleanType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class BoatLeftPaddleTurning implements Metadata {
    private boolean paddleTurning;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_10)) {
            throw new UnsupportedVersionException(version);
        }
        paddleTurning = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_10)) {
            throw new UnsupportedVersionException(version);
        }
        buf.writeBoolean(paddleTurning);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if (version.isBefore(Version.MINECRAFT_1_10)) {
            throw new UnsupportedVersionException(version);
        }
        return BooleanType.class;
    }
}
