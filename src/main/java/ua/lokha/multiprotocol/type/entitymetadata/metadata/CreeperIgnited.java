package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.BooleanType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class CreeperIgnited implements Metadata {
    private boolean ignited;

    @Override
    public void read(ByteBuf buf, Version version) {
        ignited = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeBoolean(ignited);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        return BooleanType.class;
    }
}
