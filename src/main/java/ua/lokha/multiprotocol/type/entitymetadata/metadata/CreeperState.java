package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.ByteType;
import ua.lokha.multiprotocol.type.VarIntType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class CreeperState implements Metadata {
    private int value;

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            value = buf.readByte();
        else
            value = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            buf.writeByte(value);
        else
            PacketUtils.writeVarInt(value, buf);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            return ByteType.class;

        return VarIntType.class;
    }
}
