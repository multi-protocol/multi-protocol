package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.OptBlockId;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class EndermanCarriedBlock implements Metadata {
    private int blockId;

    @Override
    public void read(ByteBuf buf, Version version) {
        blockId = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeVarInt(blockId, buf);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        return OptBlockId.class;
    }
}
