package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.type.OptChat;
import ua.lokha.multiprotocol.type.StringType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class EntityCustomName implements Metadata {
    private boolean present;
    private String name;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version);
        }
        if (version.isAfterOrEq(Version.MINECRAFT_1_13)) {
            present = buf.readBoolean();
            if (present) {
                name = PacketUtils.readString(buf);
            }
        } else {
            present = true;
            name = PacketUtils.readString(buf);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version);
        }
        if (version.isAfterOrEq(Version.MINECRAFT_1_13)) {
            buf.writeBoolean(present && StringUtils.isNotBlank(name));
            if (present && StringUtils.isNotBlank(name)) {
                PacketUtils.writeString(name, buf);
            }
        } else {
            PacketUtils.writeString(name, buf);
        }
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if (version.isBefore(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version);
        }
        if (version.isAfterOrEq(Version.MINECRAFT_1_13)) {
            return OptChat.class;
        } else {
            return StringType.class;
        }
    }
}
