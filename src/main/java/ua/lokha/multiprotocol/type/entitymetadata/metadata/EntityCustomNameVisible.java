package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.type.BooleanType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class EntityCustomNameVisible implements Metadata {
    private boolean visible;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version);
        }
        visible = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version);
        }
        buf.writeBoolean(visible);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if (version.isBefore(Version.MINECRAFT_1_9)) {
            throw new UnsupportedVersionException(version);
        }
        return BooleanType.class;
    }
}
