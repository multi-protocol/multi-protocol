package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Slot;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString(callSuper = true)
public class ItemFrameSlot extends Slot implements Metadata {
    @Override
    public void read(ByteBuf buf, Version version) {
        super.read(buf, version);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        super.write(buf, version);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        return Slot.class;
    }
}
