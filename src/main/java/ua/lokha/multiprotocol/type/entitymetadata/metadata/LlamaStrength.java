package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.type.VarIntType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class LlamaStrength implements Metadata {
    private int strength;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_11)) {
            throw new UnsupportedVersionException(version);
        }
        strength = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_11)) {
            throw new UnsupportedVersionException(version);
        }
        PacketUtils.writeVarInt(strength, buf);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if (version.isBefore(Version.MINECRAFT_1_11)) {
            throw new UnsupportedVersionException(version);
        }
        return VarIntType.class;
    }
}
