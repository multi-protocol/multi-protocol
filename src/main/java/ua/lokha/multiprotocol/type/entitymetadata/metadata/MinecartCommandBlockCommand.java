package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.StringType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class MinecartCommandBlockCommand implements Metadata {
    private String command;

    @Override
    public void read(ByteBuf buf, Version version) {
        command = PacketUtils.readString(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeString(command, buf);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        return StringType.class;
    }
}
