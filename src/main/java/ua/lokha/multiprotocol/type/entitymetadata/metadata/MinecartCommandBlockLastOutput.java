package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.Chat;
import ua.lokha.multiprotocol.type.StringType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class MinecartCommandBlockLastOutput implements Metadata {
    private String lastOutput;

    @Override
    public void read(ByteBuf buf, Version version) {
        lastOutput = PacketUtils.readString(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        PacketUtils.writeString(lastOutput, buf);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            return StringType.class;

        return Chat.class;
    }
}
