package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.FloatType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class MinecartShakingMultiplier implements Metadata {
    private float shakingMultiplier;

    @Override
    public void read(ByteBuf buf, Version version) {
        shakingMultiplier = buf.readFloat();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeFloat(shakingMultiplier);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        return FloatType.class;
    }
}
