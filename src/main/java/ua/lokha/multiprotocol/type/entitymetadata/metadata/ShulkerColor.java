package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.exception.UnsupportedVersionException;
import ua.lokha.multiprotocol.type.ByteType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class ShulkerColor implements Metadata {
    private byte color;

    @Override
    public void read(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_11)) {
            throw new UnsupportedVersionException(version);
        }
        color = buf.readByte();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if (version.isBefore(Version.MINECRAFT_1_11)) {
            throw new UnsupportedVersionException(version);
        }
        buf.writeByte(color);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if (version.isBefore(Version.MINECRAFT_1_11)) {
            throw new UnsupportedVersionException(version);
        }
        return ByteType.class;
    }
}
