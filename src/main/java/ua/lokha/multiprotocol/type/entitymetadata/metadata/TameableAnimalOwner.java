package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.OptUuid;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

import java.util.UUID;

@Data
public class TameableAnimalOwner implements Metadata {
    private boolean present;
    private UUID uuid;

    @Override
    public void read(ByteBuf buf, Version version) {
        present = buf.readBoolean();
        if (present) {
            uuid = PacketUtils.readUUID(buf);
        }
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeBoolean(present);
        if (present) {
            PacketUtils.writeUUID(uuid, buf);
        }
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        return OptUuid.class;
    }
}
