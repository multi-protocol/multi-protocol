package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.IntType;
import ua.lokha.multiprotocol.type.VarIntType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;
import ua.lokha.multiprotocol.util.PacketUtils;

@Data
public class WitherLeftHeadTarget implements Metadata {
    private int headTargetId;

    @Override
    public void read(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            headTargetId = buf.readInt();
        else
            headTargetId = PacketUtils.readVarInt(buf);
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            buf.writeInt(headTargetId);
        else
            PacketUtils.writeVarInt(headTargetId, buf);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            return IntType.class;

        return VarIntType.class;
    }
}
