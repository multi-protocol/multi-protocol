package ua.lokha.multiprotocol.type.entitymetadata.metadata;

import io.netty.buffer.ByteBuf;
import lombok.Data;
import ua.lokha.multiprotocol.Version;
import ua.lokha.multiprotocol.type.BooleanType;
import ua.lokha.multiprotocol.type.ByteType;
import ua.lokha.multiprotocol.type.entitymetadata.Metadata;
import ua.lokha.multiprotocol.type.entitymetadata.MetadataType;

@Data
public class ZombieBaby implements Metadata {
    private boolean baby;

    @Override
    public void read(ByteBuf buf, Version version) {
        baby = buf.readBoolean();
    }

    @Override
    public void write(ByteBuf buf, Version version) {
        buf.writeBoolean(baby);
    }

    @Override
    public Class<? extends MetadataType> getTypeClass(Version version) {
        if(version.isBefore(Version.MINECRAFT_1_9))
            return ByteType.class;

        return BooleanType.class;
    }
}
