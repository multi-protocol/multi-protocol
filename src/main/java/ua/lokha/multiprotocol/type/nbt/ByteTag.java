package ua.lokha.multiprotocol.type.nbt;

/**
 * Copyright Mojang AB.
 * 
 * Don't do evil.
 */

import io.netty.buffer.ByteBuf;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ByteTag extends Tag {
    public byte data;

    public ByteTag(String name) {
        super(name);
    }

    public ByteTag(String name, byte data) {
        super(name);
        this.data = data;
    }

    @Override
    public void read(ByteBuf dis) {
        data = dis.readByte();
    }

    @Override
    public void write(ByteBuf dos) {
        dos.writeByte(data);
    }

    @Override
    public byte getId() {
        return TAG_Byte;
    }

    @Override
    public Tag copy() {
        return new ByteTag(getName(), data);
    }
}
