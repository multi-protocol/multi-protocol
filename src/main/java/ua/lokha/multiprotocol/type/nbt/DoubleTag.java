package ua.lokha.multiprotocol.type.nbt;

/**
 * Copyright Mojang AB.
 * 
 * Don't do evil.
 */

import io.netty.buffer.ByteBuf;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DoubleTag extends Tag {
    public double data;

    public DoubleTag(String name) {
        super(name);
    }

    public DoubleTag(String name, double data) {
        super(name);
        this.data = data;
    }

    @Override
    public void read(ByteBuf dis) {
        data = dis.readDouble();
    }

    @Override
    public void write(ByteBuf dos) {
        dos.writeDouble(data);
    }

    @Override
    public byte getId() {
        return TAG_Double;
    }

    @Override
    public Tag copy() {
        return new DoubleTag(getName(), data);
    }
}
