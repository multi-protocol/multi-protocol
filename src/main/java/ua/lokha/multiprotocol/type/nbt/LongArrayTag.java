package ua.lokha.multiprotocol.type.nbt;

import io.netty.buffer.ByteBuf;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class LongArrayTag extends Tag {
    public long[] data;

    public LongArrayTag(String name) {
        super(name);
    }

    public LongArrayTag(String name, long[] data) {
        super(name);
        this.data = data;
    }

    @Override
    public void read(ByteBuf dis) {
        this.data = new long[dis.readInt()];
        for(int index = 0; index < this.data.length; ++index) {
            this.data[index] = dis.readLong();
        }
    }

    @Override
    public void write(ByteBuf out) {
        out.writeInt(this.data.length);
        for(int index = 0; index < this.data.length; ++index) {
            out.writeLong(this.data[index]);
        }
    }

    @Override
    public byte getId() {
        return TAG_Long_Array;
    }

    @Override
    public Tag copy() {
        long[] cp = new long[data.length];
        System.arraycopy(data, 0, cp, 0, data.length);
        return new LongArrayTag(getName(), cp);
    }
}
