package ua.lokha.multiprotocol.type.nbt;

/**
 * Copyright Mojang AB.
 * 
 * Don't do evil.
 */

import io.netty.buffer.ByteBuf;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class LongTag extends Tag {
    public long data;

    public LongTag(String name) {
        super(name);
    }

    public LongTag(String name, long data) {
        super(name);
        this.data = data;
    }

    @Override
    public void read(ByteBuf dis) {
        data = dis.readLong();
    }

    @Override
    public void write(ByteBuf dos) {
        dos.writeLong(data);
    }

    @Override
    public byte getId() {
        return TAG_Long;
    }

    @Override
    public Tag copy() {
        return new LongTag(getName(), data);
    }
}
