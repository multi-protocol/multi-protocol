package ua.lokha.multiprotocol.type.nbt;

/**
 * Copyright Mojang AB.
 * 
 * Don't do evil.
 */

import io.netty.buffer.ByteBuf;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import static ua.lokha.multiprotocol.util.PacketUtils.readUTF;
import static ua.lokha.multiprotocol.util.PacketUtils.writeUTF;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class StringTag extends Tag {
    public String data;

    public StringTag(String name) {
        super(name);
    }

    public StringTag(String name, String data) {
        super(name);
        this.data = data;
        if (data == null) throw new IllegalArgumentException("Empty string not allowed");
    }

    @Override
    public void read(ByteBuf dis) {
        data = readUTF(dis);
    }

    @Override
    public void write(ByteBuf dos) {
        writeUTF(data, dos);
    }

    @Override
    public byte getId() {
        return TAG_String;
    }

    @Override
    public Tag copy() {
        return new StringTag(getName(), data);
    }
}
