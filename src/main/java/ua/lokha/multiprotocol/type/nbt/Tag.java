package ua.lokha.multiprotocol.type.nbt;

/**
 * Copyright Mojang AB.
 *
 * Don't do evil.
 */

import io.netty.buffer.ByteBuf;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import static ua.lokha.multiprotocol.util.PacketUtils.readUTF;
import static ua.lokha.multiprotocol.util.PacketUtils.writeUTF;

@EqualsAndHashCode
@ToString
public abstract class Tag {

    public static final byte TAG_End = 0;
    public static final byte TAG_Byte = 1;
    public static final byte TAG_Short = 2;
    public static final byte TAG_Int = 3;
    public static final byte TAG_Long = 4;
    public static final byte TAG_Float = 5;
    public static final byte TAG_Double = 6;
    public static final byte TAG_Byte_Array = 7;
    public static final byte TAG_String = 8;
    public static final byte TAG_List = 9;
    public static final byte TAG_Compound = 10;
    public static final byte TAG_Int_Array = 11;
    public static final byte TAG_Long_Array = 12;

    private String name;

    protected Tag(String name) {
        if (name == null) {
            this.name = "";
        } else {
            this.name = name;
        }
    }

    public abstract void read(ByteBuf dis);

    public abstract void write(ByteBuf dos);

    public abstract byte getId();

    public abstract Tag copy();

    public Tag setName(String name) {
        if (name == null) {
            this.name = "";
        } else {
            this.name = name;
        }
        return this;
    }

    public String getName() {
        if (name == null) {
            return "";
        }
        return name;
    }

    public static Tag readNamedTag(ByteBuf buf) {
        byte type = buf.readByte();
        if (type == Tag.TAG_End) {
            return new EndTag();
        }
        String name = readUTF(buf);
        Tag tag = createTag(type, name);

        tag.read(buf);
        return tag;
    }

    public static void writeNamedTag(Tag tag, ByteBuf buf) {
        buf.writeByte(tag.getId());
        if (tag.getId() == Tag.TAG_End) {
            return;
        }

        writeUTF(tag.getName(), buf);

        tag.write(buf);
    }

    public static Tag createTag(byte type, String name) {
        switch (type) {
            case TAG_End:
                return new EndTag();
            case TAG_Byte:
                return new ByteTag(name);
            case TAG_Short:
                return new ShortTag(name);
            case TAG_Int:
                return new IntTag(name);
            case TAG_Long:
                return new LongTag(name);
            case TAG_Float:
                return new FloatTag(name);
            case TAG_Double:
                return new DoubleTag(name);
            case TAG_Byte_Array:
                return new ByteArrayTag(name);
            case TAG_Int_Array:
                return new IntArrayTag(name);
            case TAG_String:
                return new StringTag(name);
            case TAG_List:
                return new ListTag<>(name);
            case TAG_Compound:
                return new CompoundTag(name);
            case TAG_Long_Array:
                return new LongArrayTag(name);
            default:
                throw new IllegalArgumentException("type " + type + " not found");
        }
    }

    public static String getTagName(byte type) {
        switch (type) {
            case TAG_End:
                return "TAG_End";
            case TAG_Byte:
                return "TAG_Byte";
            case TAG_Short:
                return "TAG_Short";
            case TAG_Int:
                return "TAG_Int";
            case TAG_Long:
                return "TAG_Long";
            case TAG_Float:
                return "TAG_Float";
            case TAG_Double:
                return "TAG_Double";
            case TAG_Byte_Array:
                return "TAG_Byte_Array";
            case TAG_Int_Array:
                return "TAG_Int_Array";
            case TAG_String:
                return "TAG_String";
            case TAG_List:
                return "TAG_List";
            case TAG_Compound:
                return "TAG_Compound";
            case TAG_Long_Array:
                return "TAG_Long_Array";
            default:
                throw new IllegalArgumentException("type " + type + " not found");
        }
    }
}
