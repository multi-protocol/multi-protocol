package ua.lokha.multiprotocol.type.sound;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import ua.lokha.multiprotocol.util.ResourceUtils;

import java.util.Objects;

public class MinecraftSoundData {

    protected static final String[] idToName = new String[1024];

    protected static void register(int id, String name) {
        idToName[id] = name;
    }

    static {
        JsonArray sounds = ResourceUtils.GSON.fromJson(Objects.requireNonNull(ResourceUtils.getAsBufferedReader("sounds.json")), JsonArray.class);
        for (JsonElement element : sounds) {
            JsonObject soundData = element.getAsJsonObject();
            register(soundData.get("id").getAsInt(), soundData.get("name").getAsString());
        }
    }

    public static String getNameById(int id) {
        if ((id >= 0) && (id < idToName.length)) {
            return idToName[id];
        } else {
            return null;
        }
    }

}
