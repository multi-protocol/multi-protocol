package ua.lokha.multiprotocol.util;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.Objects;

public class NativeUtils {
    private static boolean nativeLibraryLoaded = false;

    static {
        if ("Linux".equals(System.getProperty("os.name")) && "amd64".equals(System.getProperty("os.arch"))) {
            try {
                System.loadLibrary("multi_protocol");
                nativeLibraryLoaded = true;
            } catch (Throwable ignored) {
            }
            if (!nativeLibraryLoaded) {
                try (InputStream inputStream = NativeUtils.class.getClassLoader().getResourceAsStream("multi_protocol.so")) {
                    File temp = File.createTempFile("multi_protocol", ".so");
                    temp.deleteOnExit();
                    try (OutputStream outputStream = new FileOutputStream(temp)) {
                        IOUtils.copy(Objects.requireNonNull(inputStream), outputStream);
                    }
                    System.load(temp.getPath());
                    nativeLibraryLoaded = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private byte[] remap1_9to1_8(long[] blockData, int[] palette, short bitsPerBlock) {
        byte[] data = new byte[4096 * 2];
        if (nativeLibraryLoaded) {
            NativeUtils.remapChunk1_9to1_8$native(blockData, palette, bitsPerBlock, data);
        } else {
            int singleValMask = (1 << bitsPerBlock) - 1;
            int pos = 0;
            for (int block = 0; block < 4096; block++) {
                int bitStartIndex = block * bitsPerBlock;
                int arrStartIndex = bitStartIndex >> 6;
                int arrEndIndex = ((bitStartIndex + bitsPerBlock) - 1) >> 6;
                int localStartBitIndex = bitStartIndex & 63;
                int paletteIndex;
                if (arrStartIndex == arrEndIndex) {
                    paletteIndex = (int) ((blockData[arrStartIndex] >>> localStartBitIndex) & singleValMask);
                } else {
                    paletteIndex = (int) (((blockData[arrStartIndex] >>> localStartBitIndex) | (blockData[arrEndIndex] << (64 - localStartBitIndex))) & singleValMask);
                }
                int blockState = palette[paletteIndex];
                data[pos++] = (byte) blockState;
                data[pos++] = (byte) (blockState >> 8);
            }
        }
        return data;
    }

    public static void main(String[] args) {
        int singleValMask = (1 << 4) - 1;
        System.out.println(singleValMask);
    }

    private static native void remapChunk1_9to1_8$native(long[] blockData, int[] palette, short bitsPerBlock, byte[] desc);
}
