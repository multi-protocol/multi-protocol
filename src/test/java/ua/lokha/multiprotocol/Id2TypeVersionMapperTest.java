package ua.lokha.multiprotocol;

import org.junit.Test;

import static org.junit.Assert.*;
import static ua.lokha.multiprotocol.Id2TypeVersionMapper.map;

public class Id2TypeVersionMapperTest {

    @Test
    public void test() {
        Id2TypeVersionMapper<Type> mapper = new Id2TypeVersionMapper<>();

        mapper.register(OneType.class, OneType::new,
                map(Version.MINECRAFT_1_9, 10),
                map(Version.MINECRAFT_1_13, 20),
                map(Version.MINECRAFT_1_14, -1));

        mapper.register(TwoType.class, TwoType::new,
                map(Version.MINECRAFT_1_10, 5),
                map(Version.MINECRAFT_1_13, 10),
                map(Version.MINECRAFT_1_13_2, 40));

        for (Version version : Version.getVersions()) {
            assertEquals(-1, mapper.getId(ThreeType.class, version));
            assertNull(mapper.getType(38274 /* any no used id */, version));
            assertNull(mapper.create(24244 /* any no used id */, version));
        }

        assertEquals(-1, mapper.getId(OneType.class, Version.MINECRAFT_1_8));
        assertEquals(10, mapper.getId(OneType.class, Version.MINECRAFT_1_9_1));
        assertEquals(10, mapper.getId(OneType.class, Version.MINECRAFT_1_9_2));
        assertEquals(10, mapper.getId(OneType.class, Version.MINECRAFT_1_9_4));
        assertEquals(10, mapper.getId(OneType.class, Version.MINECRAFT_1_10));
        assertEquals(10, mapper.getId(OneType.class, Version.MINECRAFT_1_11));
        assertEquals(10, mapper.getId(OneType.class, Version.MINECRAFT_1_11_1));
        assertEquals(10, mapper.getId(OneType.class, Version.MINECRAFT_1_12));
        assertEquals(10, mapper.getId(OneType.class, Version.MINECRAFT_1_12_1));
        assertEquals(10, mapper.getId(OneType.class, Version.MINECRAFT_1_12_2));
        assertEquals(20, mapper.getId(OneType.class, Version.MINECRAFT_1_13));
        assertEquals(20, mapper.getId(OneType.class, Version.MINECRAFT_1_13_1));
        assertEquals(20, mapper.getId(OneType.class, Version.MINECRAFT_1_13_2));
        assertEquals(-1, mapper.getId(OneType.class, Version.MINECRAFT_1_14));
        assertEquals(-1, mapper.getId(OneType.class, Version.MINECRAFT_1_14_1));
        assertEquals(-1, mapper.getId(OneType.class, Version.MINECRAFT_1_14_2));
        assertEquals(-1, mapper.getId(OneType.class, Version.MINECRAFT_1_14_3));
        assertEquals(-1, mapper.getId(OneType.class, Version.MINECRAFT_1_14_4));

        assertEquals(-1, mapper.getId(TwoType.class, Version.MINECRAFT_1_8));
        assertEquals(-1, mapper.getId(TwoType.class, Version.MINECRAFT_1_9_1));
        assertEquals(-1, mapper.getId(TwoType.class, Version.MINECRAFT_1_9_2));
        assertEquals(-1, mapper.getId(TwoType.class, Version.MINECRAFT_1_9_4));
        assertEquals(5, mapper.getId(TwoType.class, Version.MINECRAFT_1_10));
        assertEquals(5, mapper.getId(TwoType.class, Version.MINECRAFT_1_11));
        assertEquals(5, mapper.getId(TwoType.class, Version.MINECRAFT_1_11_1));
        assertEquals(5, mapper.getId(TwoType.class, Version.MINECRAFT_1_12));
        assertEquals(5, mapper.getId(TwoType.class, Version.MINECRAFT_1_12_1));
        assertEquals(5, mapper.getId(TwoType.class, Version.MINECRAFT_1_12_2));
        assertEquals(10, mapper.getId(TwoType.class, Version.MINECRAFT_1_13));
        assertEquals(10, mapper.getId(TwoType.class, Version.MINECRAFT_1_13_1));
        assertEquals(40, mapper.getId(TwoType.class, Version.MINECRAFT_1_13_2));
        assertEquals(40, mapper.getId(TwoType.class, Version.MINECRAFT_1_14));
        assertEquals(40, mapper.getId(TwoType.class, Version.MINECRAFT_1_14_1));
        assertEquals(40, mapper.getId(TwoType.class, Version.MINECRAFT_1_14_2));
        assertEquals(40, mapper.getId(TwoType.class, Version.MINECRAFT_1_14_3));
        assertEquals(40, mapper.getId(TwoType.class, Version.MINECRAFT_1_14_4));

        assertNull(mapper.getType(5, Version.MINECRAFT_1_8));
        assertNull(mapper.getType(5, Version.MINECRAFT_1_9));
        assertNull(mapper.getType(5, Version.MINECRAFT_1_9_1));
        assertNull(mapper.getType(5, Version.MINECRAFT_1_9_2));
        assertNull(mapper.getType(5, Version.MINECRAFT_1_9_4));
        assertEquals(TwoType.class, mapper.getType(5, Version.MINECRAFT_1_10));
        assertEquals(TwoType.class, mapper.getType(5, Version.MINECRAFT_1_11));
        assertEquals(TwoType.class, mapper.getType(5, Version.MINECRAFT_1_11_1));
        assertEquals(TwoType.class, mapper.getType(5, Version.MINECRAFT_1_12));
        assertEquals(TwoType.class, mapper.getType(5, Version.MINECRAFT_1_12_1));
        assertEquals(TwoType.class, mapper.getType(5, Version.MINECRAFT_1_12_2));
        assertNull(mapper.getType(5, Version.MINECRAFT_1_13));
        assertNull(mapper.getType(5, Version.MINECRAFT_1_13_1));
        assertNull(mapper.getType(5, Version.MINECRAFT_1_13_2));
        assertNull(mapper.getType(5, Version.MINECRAFT_1_14));
        assertNull(mapper.getType(5, Version.MINECRAFT_1_14_1));
        assertNull(mapper.getType(5, Version.MINECRAFT_1_14_2));
        assertNull(mapper.getType(5, Version.MINECRAFT_1_14_3));
        assertNull(mapper.getType(5, Version.MINECRAFT_1_14_4));

        assertEquals(OneType.class, mapper.getType(10, Version.MINECRAFT_1_9));
        assertEquals(OneType.class, mapper.getType(10, Version.MINECRAFT_1_9_1));
        assertEquals(OneType.class, mapper.getType(10, Version.MINECRAFT_1_9_2));
        assertEquals(OneType.class, mapper.getType(10, Version.MINECRAFT_1_9_4));
        assertEquals(OneType.class, mapper.getType(10, Version.MINECRAFT_1_10));
        assertEquals(OneType.class, mapper.getType(10, Version.MINECRAFT_1_11));
        assertEquals(OneType.class, mapper.getType(10, Version.MINECRAFT_1_11_1));
        assertEquals(OneType.class, mapper.getType(10, Version.MINECRAFT_1_12));
        assertEquals(OneType.class, mapper.getType(10, Version.MINECRAFT_1_12_1));
        assertEquals(OneType.class, mapper.getType(10, Version.MINECRAFT_1_12_2));
        assertEquals(TwoType.class, mapper.getType(10, Version.MINECRAFT_1_13));
        assertEquals(TwoType.class, mapper.getType(10, Version.MINECRAFT_1_13_1));

        assertEquals(OneType.class, mapper.getType(20, Version.MINECRAFT_1_13));
        assertEquals(OneType.class, mapper.getType(20, Version.MINECRAFT_1_13_1));
        assertEquals(OneType.class, mapper.getType(20, Version.MINECRAFT_1_13_2));

        assertEquals(TwoType.class, mapper.getType(40, Version.MINECRAFT_1_14));
        assertEquals(TwoType.class, mapper.getType(40, Version.MINECRAFT_1_14_1));
        assertEquals(TwoType.class, mapper.getType(40, Version.MINECRAFT_1_14_2));
        assertEquals(TwoType.class, mapper.getType(40, Version.MINECRAFT_1_14_3));
        assertEquals(TwoType.class, mapper.getType(40, Version.MINECRAFT_1_14_4));

        assertTrue(mapper.create(10, Version.MINECRAFT_1_9) instanceof OneType);
        assertTrue(mapper.create(10, Version.MINECRAFT_1_10) instanceof OneType);
        assertTrue(mapper.create(20, Version.MINECRAFT_1_13) instanceof OneType);
        assertTrue(mapper.create(10, Version.MINECRAFT_1_13) instanceof TwoType);
        assertTrue(mapper.create(10, Version.MINECRAFT_1_13_1) instanceof TwoType);
        assertNull(mapper.create(40, Version.MINECRAFT_1_8));
    }

    public interface Type {
    }

    public static class OneType implements Type {
    }

    public static class TwoType implements Type {
    }

    public static class ThreeType implements Type {
    }
}