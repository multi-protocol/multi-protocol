package ua.lokha.multiprotocol.util;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class MyObjectTest {

    private static final String STRING_VALUE = "someString";
    private static final int FINAL_INT_VALUE = 42;

    private ObjectForTest obj;

    @Before
    public void setup() {
        obj = new ObjectForTest();
        obj.stringField = STRING_VALUE;
    }

    @Test
    public void getField_notNull() {
        String stringField = MyObject.wrap(obj).getField("stringField").getObject(String.class);

        assertEquals(STRING_VALUE, stringField);
    }

    @Test
    public void getField_null() {
        Object nullField = MyObject.wrap(obj).getField("nullField").getObject();

        assertNull(nullField);
    }

    @Test
    public void getField_final() {
        int value = MyObject.wrap(obj).getField("MAGIC_NUMBER").getObject();

        assertEquals(FINAL_INT_VALUE, value);
    }

    @Test(expected = NoSuchFieldException.class)
    public void getField_nonExists() {
        MyObject.wrap(obj).getField("nonExistsField").getObject();
    }

    @Test
    public void setField_normalValue() {
        final String NEW_VALUE = "newValue";

        MyObject.wrap(obj).setField("stringField", NEW_VALUE);

        assertEquals(obj.stringField, NEW_VALUE);
    }

    @Test
    public void setField_MyObjectValue() {
        final String NEW_VALUE = "newValue";
        final MyObject NEW_VALUE_MYOBJECT = MyObject.wrap("newValue");

        MyObject.wrap(obj).setField("stringField", NEW_VALUE_MYOBJECT);

        assertEquals(obj.stringField, NEW_VALUE);
    }

    //TODO Не изменяется финализированное поле. Так и должно быть?
    @Test
    public void setField_final() {
        final int NEW_VALUE = 21;

        MyObject.wrap(obj).setField("MAGIC_NUMBER", NEW_VALUE);

        assertEquals(obj.MAGIC_NUMBER, NEW_VALUE);
    }

    @Test
    public void invokeMethod_void() {
        ObjectForTest spyObj = spy(obj);

        MyObject.wrap(spyObj).invokeMethod("voidMethod");

        verify(spyObj).voidMethod();
    }

    //TODO Этот тест падает. Ошибка в MyObject.invokeMethod?
    @Test
    public void invokeMethod_void_args() {
        ObjectForTest spyObj = spy(obj);

        MyObject.wrap(spyObj).invokeMethod("voidMethodArgs", 1, new Object());

        verify(spyObj).voidMethodArgs(1, new Object());
    }

    @Test
    public void invokeMethod_nonNull() {
        ObjectForTest spyObj = spy(obj);

        Object object = MyObject.wrap(spyObj).invokeMethod("objectMethod").getObject();

        verify(spyObj).objectMethod();
        assertNotNull(object);
    }

    //TODO Этот тест падает. Ошибка в MyObject.invokeMethod?
    // да ошибка в invokeMethod, в java int.class.isInstance(Integer object) возвращает false
    // либа должна быть к такому готова
 /*   @Test
    public void invokeMethod_nonNull_args() {
        ObjectForTest spyObj = spy(obj);

        Object object = MyObject.wrap(spyObj).invokeMethod("objectMethodArgs", 1, new Object()).getObject();

        verify(spyObj).objectMethodArgs(1, new Object());
        assertNotNull(object);
    }*/

    @Test(expected = NoSuchMethodException.class)
    public void invokeMethod_nonExists() {
        MyObject.wrap(obj).invokeMethod("nonExistsMethod");
    }

    //TODO Возникает прямое исключение, но оно не оборачивается в RuntimeException
    @Test
    public void unchecked_SupplierThrows() {
        try {
            MyObject.unchecked(new MyObject.SupplierThrows<String>() {

                @Override
                public String get() throws Exception {
                    throw new Exception();
                }
            });
        } catch (Exception e) {
            fail();
        }
    }

    //TODO Возникает прямое исключение, но оно не оборачивается в RuntimeException
    @Test
    public void unchecked_RunnableThrows() {
        try {
            MyObject.unchecked(new MyObject.RunnableThrows() {

                @Override
                public void run() throws Exception {
                    throw new Exception();
                }
            });
        } catch (Exception e) {
            fail();
        }
    }

    //TODO Возникает прямое исключение, но оно не оборачивается в RuntimeException
    @Test
    public void unchecked_PredicateThrows() {
        try {
            MyObject.unchecked(new MyObject.PredicateThrows<String>() {
                @Override
                public boolean test(String val) throws Exception {
                    throw new Exception();
                }
            }).test("string");
        } catch (Exception e) {
            fail();
        }
    }

    private static class ObjectForTest {

        private final int MAGIC_NUMBER = FINAL_INT_VALUE;

        private String stringField;

        private Object nullField;

        public void voidMethod() {
            // nothing
        }

        public Object objectMethod() {
            return new Object();
        }

        public void voidMethodArgs(int primitiveArg, Object objectArg) {
            // nothing
        }

        public Object objectMethodArgs(int primitiveArg, Object objectArg) {
            return new Object();
        }
    }
}