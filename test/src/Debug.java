

public class Debug {

    public static void main(String[] args) {
        int x = 619;
        int y = 3;
        int z = -1340;

        {
            System.out.println((((long)x & 0x3FFFFFF) << 38) | (((long)y & 0xFFF) << 26) | (z & 0x3FFFFFF));
        }

        {
            final int NUM_X_BITS = 1 + MathHelper.log2(MathHelper.smallestEncompassingPowerOfTwo(30000000));
            final int NUM_Z_BITS = NUM_X_BITS;
            final int NUM_Y_BITS = 64 - NUM_X_BITS - NUM_Z_BITS;
            final int Y_SHIFT = 0 + NUM_Z_BITS;
            final int X_SHIFT = Y_SHIFT + NUM_Y_BITS;
            final long X_MASK = (1L << NUM_X_BITS) - 1L;
            final long Y_MASK = (1L << NUM_Y_BITS) - 1L;
            final long Z_MASK = (1L << NUM_Z_BITS) - 1L;
            
            System.out.println(((long)x & X_MASK) << X_SHIFT | ((long)y & Y_MASK) << Y_SHIFT | ((long)z & Z_MASK) << 0);
        }

        {
            final int NUM_X_BITS = 1 + MathHelper.log2(MathHelper.smallestEncompassingPowerOfTwo(30000000));
            final int NUM_Z_BITS = NUM_X_BITS;
            final int NUM_Y_BITS = 64 - NUM_X_BITS - NUM_Z_BITS;
            final long X_MASK = (1L << NUM_X_BITS) - 1L;
            final long Y_MASK = (1L << NUM_Y_BITS) - 1L;
            final long Z_MASK = (1L << NUM_Z_BITS) - 1L;
            final int field_218292_j = NUM_Y_BITS;
            final int field_218293_k = NUM_Y_BITS + NUM_Z_BITS;
            
            long i = 0L;
            i = i | ((long)x & X_MASK) << field_218293_k;
            i = i | ((long)y & Y_MASK) << 0;
            i = i | ((long)z & Z_MASK) << field_218292_j;
            System.out.println(i);
        }

        {
            System.out.println((((long)x & 0x3ffffff) << 38) | ((long)y & 0xfff) | (((long)z & 0x3ffffff) << 12));
        }

    }


}
