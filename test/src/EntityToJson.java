import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EntityToJson {
    public static void main(String[] args) throws Exception {
        String txt = FileUtils.readFileToString(new File("entity-enum.txt")).replace("\r", "");

        List list = new ArrayList<>();

        String[] current = txt.split("\n");
        for (int i = 0; i < current.length; i++) {
            String line = current[i];

            String mat = StringUtils.substringBefore(line, "(").trim();
            Integer mobId = Integer.parseInt(StringUtils.substringBetween(line, "(", ",").trim());
            Integer objectId = Integer.parseInt(StringUtils.substringBetween(line, ",", ")").trim());

            if (objectId == -1) {
                continue;
            }

            Map block = new HashMap();
            List map = new ArrayList();

            {
                Map ver = new HashMap();
                ver.put("version", "MINECRAFT_1_8");
                ver.put("id", objectId);
                map.add(ver);
            }

            {
                Map ver = new HashMap();
                ver.put("version", "MINECRAFT_1_13");
                ver.put("id", -1);
                map.add(ver);
            }

            block.put("enum", mat);
            block.put("map", map);

            list.add(block);
        }

        Gson gson = new Gson();
        String json = gson.toJson(list);

        FileUtils.writeStringToFile(new File("out.txt"), json);
    }
}
