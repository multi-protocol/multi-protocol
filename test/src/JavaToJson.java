import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JavaToJson {
    public static void main(String[] args) throws Exception {
        String txt = FileUtils.readFileToString(new File("java-block-protocol.txt")).replace("\r", "");

        List list = new ArrayList<>();

        String[] current = txt.split("\n");
        for (int i = 0; i < current.length; i++) {
            String line = current[i];

            String mat = StringUtils.substringBetween(line, "Material.", ",");
            String id1_8 = StringUtils.substringBetween(line, "MINECRAFT_1_8, ", ")");
            String id1_13 = StringUtils.substringBetween(line, "MINECRAFT_1_13, ", ")");

            Map block = new HashMap();
            List map = new ArrayList();

            {
                Map ver = new HashMap();
                ver.put("version", "MINECRAFT_1_8");
                ver.put("id", String.valueOf(Integer.parseInt(id1_8) << 4));
                map.add(ver);
            }

            {
                if (StringUtils.isNotBlank(id1_13)) {
                    Map ver = new HashMap();
                    ver.put("version", "MINECRAFT_1_13");
                    ver.put("id", id1_13);
                    map.add(ver);
                }
            }

            block.put("material", mat);
            block.put("map", map);

            list.add(block);
        }

        Gson gson = new Gson();
        String json = gson.toJson(list);

        FileUtils.writeStringToFile(new File("out.txt"), json);
    }
}
