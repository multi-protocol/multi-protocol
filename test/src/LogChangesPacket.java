import com.google.common.collect.Lists;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class LogChangesPacket {
    public static void main(String[] args) throws Exception {
        File file = new File("changes-protocol.txt");
        List<String> lines = new ArrayList<>(FileUtils.readLines(file, StandardCharsets.UTF_8));
        Lists.reverse(lines);
        boolean version = false;
        for (String line : lines) {
            if (line.contains("Update Sign") || line.matches("[0-9., ]+")) {
                System.out.println(line);
            }
        }
    }
}
