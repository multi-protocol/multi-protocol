package by.capitoska.model;

import com.google.gson.*;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;


public class PopulateBlockstateProtocol {


    /**
     * Simple readFile from  file, where return all text in String
     *
     * @param pathname
     * @return all text from file in String
     * @throws IOException
     */
    public static String readFile(String pathname) throws IOException {
        FileReader fileReader = new FileReader(new File(pathname));
        char[] text = new char[256];
        StringBuilder stringBuilder = new StringBuilder();
        int amount;
        while ((amount = fileReader.read(text)) > 0) {
            if (amount < text.length) {
                text = Arrays.copyOf(text, amount);
            }
            stringBuilder.append(text);
        }
        fileReader.close();
        return stringBuilder.toString();
    }

    public static void writeFile(String text) throws IOException {
        FileWriter fileWriter = new FileWriter(new File("test.txt"));
        fileWriter.write(text);
        fileWriter.close();
    }


    /**
     * @hashMap<Key,Value> - полученный словарь, где
     * @Key - название предмета
     * @Value - id предмета
     */
    public static Map convertSetToMap(Set set) {
        Map<String, Integer> hashMap = new HashMap<>();
        Iterator iterator = set.iterator();

        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            Integer value = Integer.parseInt((String) entry.getKey());
            String key = ((JsonElement) entry.getValue()).getAsString();
            key = key.substring(10);
            hashMap.put(key, value);
        }
        return hashMap;
    }

    /**
     * @param name
     * @param id      the id in new version
     * @param version the Minecraft version from
     * @return
     */
    public static JsonElement addNewObject(String name, String id, String version) {
        JsonObject jsonObject1 = new JsonObject();
        jsonObject1.addProperty("id", id);
        jsonObject1.addProperty("version", version);

        JsonArray jsonArray1 = new JsonArray();
        jsonArray1.add(jsonObject1);

        JsonObject jsonObject2 = new JsonObject();
        jsonObject2.addProperty("value", name);
        jsonObject2.add("map", jsonArray1);
        return jsonObject2;
    }

    private static JsonObject addStandardJsonObject(String id, String version) {
        JsonObject newJsonObject = new JsonObject();
        newJsonObject.addProperty("id", id);
        newJsonObject.addProperty("version", version);
        return newJsonObject;
    }

    public static void updateOldBlocks(HashMap<String, Integer> blockData, String version, JsonElement jsonCurrentElement) {
        for (JsonElement currentItem : jsonCurrentElement.getAsJsonArray()) {
            JsonObject currentJSONObject = currentItem.getAsJsonObject();
            String nameObjectInCurrentJsonFile = currentJSONObject.get("value").getAsString();
            JsonArray jsonArray = (JsonArray) currentJSONObject.get("map");
            JsonObject jsonObjectFromMap;
            Iterator iterator = blockData.entrySet().iterator();
            if (jsonArray.size() > 0) {
                jsonObjectFromMap = (JsonObject) jsonArray.get(jsonArray.size() - 1);
                Integer idLastVersionInCurrentJsonFile = Integer.parseInt(jsonObjectFromMap.get("id").getAsString());
                while (iterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    if (entry.getKey().equals(nameObjectInCurrentJsonFile)) {
                        if (!entry.getValue().equals(idLastVersionInCurrentJsonFile)) {
//                            JsonObject newJsonObject = new JsonObject();
//                            newJsonObject.addProperty("id", entry.getValue().toString());
//                            newJsonObject.addProperty("version", version);

                            jsonArray.add(addStandardJsonObject(entry.getValue().toString(), version));
                        }
                        iterator.remove();
                    }
                }
            } else {
                while (iterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    if (entry.getKey().equals(nameObjectInCurrentJsonFile)) {
                        jsonArray.add(addStandardJsonObject(entry.getValue().toString(), version));
                        iterator.remove();
                    }
                }
            }
        }
    }


    /**
     * @param version version Minecraft, that we want remove.
     * @author Arseni Kamadei(Capitoska)
     * @version 1.0
     */
    public static void deleteVersionFromJsonObjects(String version, String inputPathname, String outputPathname) throws IOException {
        String rootcurrent = readFile(inputPathname);
        Gson gson = new Gson();
        JsonElement jsonCurrentElement = gson.fromJson(rootcurrent, JsonElement.class);
        Iterator jsonCurrentElementIterator  = jsonCurrentElement.getAsJsonArray().iterator();
        while (jsonCurrentElementIterator.hasNext()){
            JsonElement mapJsonElement = ((JsonObject) jsonCurrentElementIterator.next()).get("map");
            Iterator jsonElementIterator = mapJsonElement.getAsJsonArray().iterator();
            while (jsonElementIterator.hasNext()) {
                JsonElement jsonElement1 = (JsonElement) jsonElementIterator.next();
                if (jsonElement1.getAsJsonObject().get("version").getAsString().equals(version)) {
                    jsonElementIterator.remove();
                }
            }
            if (mapJsonElement.getAsJsonArray().size() == 0){
                jsonCurrentElementIterator.remove();
            }
        }
//        for (JsonElement jsonElement : jsonCurrentElement.getAsJsonArray()
//        ) {
//            JsonElement mapJsonElement = ((JsonObject) jsonElement).get("map");
//            Iterator jsonElementIterator = mapJsonElement.getAsJsonArray().iterator();
//            while (jsonElementIterator.hasNext()) {
//                JsonElement jsonElement1 = (JsonElement) jsonElementIterator.next();
//                if (jsonElement1.getAsJsonObject().get("version").getAsString().equals(version)) {
//                    jsonElementIterator.remove();
//                }
//            }
//        }
        Gson gson1 = new GsonBuilder().disableHtmlEscaping().create();
        String json = gson1.toJson(jsonCurrentElement);
        FileUtils.writeStringToFile(new File(outputPathname), json);
    }

    /**
     * This method work only for add new version in block-protocol.json.
     *
     * @param blockData This is Map blocks, where Key is block name and Value is ID this block.
     * @param version   This is Minecraft version. All space and point replace on underscore. For example if
     *                  your version is 1.9.4, input "MINECRAFT_1_9_4"
     * @author Arseni Kamadei (Capitoska)
     */
    public static void addNewVersionInProtocol(HashMap<String, Integer> blockData,
                                               String version, String inputPathname,
                                               String outputPathname) throws IOException {
        String rootcurrents = readFile(inputPathname);
        Gson gson = new Gson();
        JsonElement jsonCurrentElement = gson.fromJson(rootcurrents, JsonElement.class);
        updateOldBlocks(blockData, version, jsonCurrentElement);
        JsonArray jsonArray = jsonCurrentElement.getAsJsonArray();
        for (Map.Entry entry : blockData.entrySet()) {
            jsonArray.add(addNewObject(entry.getKey().toString(), entry.getValue().toString(), version));
        }
        Gson gson1 = new GsonBuilder().disableHtmlEscaping().create();
        String json = gson1.toJson(jsonCurrentElement);
        FileUtils.writeStringToFile(new File(outputPathname), json);
    }

    /**
     *  For test and use thib library without import.
     *
     * @param args
     * @throws Exception
     */
//    public static void main(String[] args) throws Exception {
//        String rootBlocks = readFile("mapping-1.15.json");
//        Gson gson = new Gson();
//        JsonElement jsonBlocksElement = gson.fromJson(rootBlocks, JsonElement.class);
//        JsonObject jsonObject = jsonBlocksElement.getAsJsonObject().get("items").getAsJsonObject();
//        Set set = jsonObject.entrySet();
//        HashMap<String, Integer> stringIntegerHashMap =
//                (HashMap<String, Integer>) convertSetToMap(set);
//
//        addNewVersionInProtocol(stringIntegerHashMap, "MINECRAFT_1_15",
//                "item-protocol.json", "item-protocol.json");
//
//    }
}
