import net.minecraft.server.v1_12_R1.Block;
import net.minecraft.server.v1_12_R1.IBlockData;
import net.minecraft.server.v1_12_R1.RegistryBlockID;
import util.MyObject;

/**
 * Created by дартЪ on 30.01.2020
 */
public class PrintBlockIds {

    public static void main(String[] args) {
        RegistryBlockID<IBlockData> blocks = MyObject.wrap(Block.class).getField("REGISTRY_ID").getObject();
        blocks.forEach(data -> System.out.println(String.format("Block Data %s and it's id %d", data.toString(), blocks.getId(data))));
    }
}
